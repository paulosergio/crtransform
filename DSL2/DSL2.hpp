#pragma once
#include "BasicTrans.hpp"
#include <functional>
#include <vector>
#include <cassert>
#include <complex>
#include "NumTh.hpp"
#include "Permutations.hpp"

/*
  Trans represents either the Identity matrix
  - used as a flag to signal that a list of transformations has ended
  - or a linear transform of the type Il @ f @ Ir
  The pointer *prev points to the tranform that follows in the chain
*/
enum TransType {
  T_ID,
  T_TENSORABLE
};

struct Trans {
  TransType m_type;

  union Trans_u {
    
    struct Id_s {
      uint64_t m;
    } m_id;

    struct TransF_s {
      uint64_t l, r;
      BasicTrans f;
      Trans* prev;
    } m_transf;
    
  } m_trans;

  ~Trans();
};

/* Dot and Kronecker implement the following equalities:
   Im @ In = Imn
   In @ (A . B) = (In @ A) . (In @ B)
   (A . B) = (A @ In) . (B @ In)
   (An @ Bm) = (An @ Im) . (In @ Bm)
*/
Trans *Dot(const Trans *t1, const Trans *t2);
Trans *Kronecker(Trans *t1, Trans *t2);

/* For m = p1^l1 * ... * pn^ln,
   KroneckerProd computes base(p1, l1) @ ... @ base (pn, ln) */
Trans *KroneckerProd(std::function<Trans*(uint64_t, uint64_t)> base,
		     uint64_t m);

std::ostream& operator<<(std::ostream &os, const Trans &t);

Trans* get_copy(const Trans *t);

uint64_t get_dim(const Trans *t);

Trans* make_transf(uint64_t l,
		   uint64_t r,
		   BasicTrans f,
		   Trans *pt);

Trans* make_transf(uint64_t l,
		   uint64_t r,
		   BasicTrans f);

Trans* make_id(uint64_t m);

TransType get_type(const Trans *t);

/* Converts elements of the form
   Il @ Am @ Ir to
   to L^(l*m*r)lm (I(l*r) @ Am) L^(l*m*r)r */
Trans* parallelise(const Trans *t);

/* Permutes a vector with stride n */
template<typename T>
std::vector<T> perm(std::vector<T> x, uint64_t n);

/* Evaluates the transform t, for the input x, modulo q,
   assuming the transform is DFT/IDFT/CRT/ICRT with index index*/
template<typename ZZq>
std::vector<typename ZZq::value_type>
eval_zzq (const Trans *t,
	  std::vector<typename ZZq::value_type> x,
	  typename ZZq::value_type q,
	  uint64_t index);

std::vector<double>
static eval_rr (const Trans *t,
		std::vector<double> x,
		uint64_t index);

template<typename ZZ>
std::vector<typename ZZ::value_type>
eval_zz (const Trans *t,
	 std::vector<typename ZZ::value_type> x,
	 uint64_t index);

void dsl2_set_debug(bool debug);
bool dsl2_get_debug();

/* Implementation of templated functions */
template<typename T>
std::vector<T> perm(std::vector<T> x, uint64_t n)
{
  if (n == 1 || n == x.size())
    return x;
  
  uint64_t m = x.size()/n;
  std::vector<T> y(x.size());

  for (uint64_t h = 0; h < x.size(); h++) {
    uint64_t i = h/n;
    uint64_t j = h - i*n;
    y[j * m + i] = x[h];
  }

  return y;
}

template<typename ZZ>
std::vector<typename ZZ::value_type>
apply_zz (const BasicTrans &t,
	  std::vector<typename ZZ::value_type> x)
{
  std::vector<typename ZZ::value_type> y(x);
  
  switch (get_type(t)) {
  case BT_PERM:
    return perm<typename ZZ::value_type>(x, get_permutation_n(t));
  case BT_L:
    {
      uint64_t n = x.size();
      y[0] = x[0];

      for (uint64_t i = 1; i < n; i++) {
	y[i] = x[i] + y[i-1];
      }
    }
    break;
  case BT_L_INV:
    {
      uint64_t n = x.size();
      y[0] = x[0];
      
      for (uint64_t i = 1; i < n; i++) {
	y[i] = x[i] - x[i-1];
      }
    }
    break;
  default:
    abort();
  }

  return y;
}


template<typename ZZ>
std::vector<typename ZZ::value_type>
eval_zz (const Trans *t,
	 std::vector<typename ZZ::value_type> x,
	 uint64_t index)
{
  std::vector<typename ZZ::value_type> y(x);
  uint64_t n              = x.size();
  if (dsl2_get_debug())
    std::cout << "###### EVAL_ZZ #####\n";

  for (const Trans *it = t;
       get_type(it) != T_ID;
       it = it->m_trans.m_transf.prev) {
    assert(get_dim(it) == n);

    if (dsl2_get_debug())
      std::cout << *it << "\n";
    
    uint64_t l = it->m_trans.m_transf.l;
    uint64_t r = it->m_trans.m_transf.r;
    uint64_t m = n / (l*r);

    y = perm(y, r);
    for (uint64_t j = 0; j < l*r; j++) {
      std::vector<typename ZZ::value_type>
	y_slice (std::begin(y) + j * m,
		 std::begin(y) + (j+1) * m);

      y_slice = apply_zz<ZZ>(it->m_trans.m_transf.f,
			     y_slice);
      
      std::copy(std::begin(y_slice),
		std::end(y_slice),
		std::begin(y) + j * m);
    }
	
    y = perm(y, l*m);
  }

  return y;
}

std::vector<double>
static apply_rr
(const BasicTrans &t,
 std::vector<double> x,
 std::complex<double> w_m)
{
  std::vector<double> y(x);
  double sqrt_2 = std::sqrt(2.0);
  
  switch (get_type(t)) {
  case BT_PERM:
    return perm<double>(x, get_permutation_n(t));
  case BT_CRT_B:
    {
      std::complex<double> w1(1.0, 0.0), w2;
      uint64_t n = x.size();
      if (n > 1)
	std::fill(y.begin(), y.end(), 0.0);

      for (uint64_t i = 0; i < n; i++) {
	w2 = w1;
	for (uint64_t j = 0; j < n/2; j++) {
	  y[i] += sqrt_2 * w2.real() * x[j];
	  y[i] += sqrt_2 * w2.imag() * x[n - (j+1)];
	  w2 *= w1;
	}
	w1 *= w_m;
      }
    }
    break;
  default:
    abort();
  }

  return y;
}

std::vector<double>
eval_rr (const Trans *t,
	 std::vector<double> x,
	 uint64_t index)
{
  std::vector<double> y(x);
  uint64_t n              = x.size();
  std::complex<double> w0 = findElemOfOrderCC(index);
  if (dsl2_get_debug())
    std::cout << "###### EVAL_RR #####\n";

  for (const Trans *it = t;
       get_type(it) != T_ID;
       it = it->m_trans.m_transf.prev) {
    assert(get_dim(it) == n);

    if (dsl2_get_debug())
      std::cout << *it << "\n";
    
    uint64_t l = it->m_trans.m_transf.l;
    uint64_t r = it->m_trans.m_transf.r;
    uint64_t m = n / (l*r);

    y = perm(y, r);
    for (uint64_t j = 0; j < l*r; j++) {
      std::vector<double> y_slice(std::begin(y) + j * m,
				  std::begin(y) + (j+1) * m);

      y_slice = apply_rr(it->m_trans.m_transf.f,
			 y_slice,
			 std::pow(w0, index/get_m(it->m_trans.m_transf.f)));
      
      std::copy(std::begin(y_slice),
		std::end(y_slice),
		std::begin(y) + j * m);
    }
	
    y = perm(y, l*m);
  }

  return y;
}

template<typename ZZq>
std::vector<typename ZZq::value_type>
apply_zzq (const BasicTrans &t,
	   std::vector<typename ZZq::value_type> x,
	   typename ZZq::value_type w_m,
	   typename ZZq::value_type q)
{
  std::vector<typename ZZq::value_type> y(x);
  
  switch (get_type(t)) {
  case BT_PERM:
    return perm<typename ZZq::value_type>(x, get_permutation_n(t));
  case BT_TWIDDLE_INV:
    w_m = modinv<ZZq>(w_m, q);
    /* fallthrough */
  case BT_TWIDDLE:
    {
      auto fs = factor(get_m(t));
      assert(fs.size() == 1);
      typename ZZq::value_type m1 = pown(fs[0].first, fs[0].second-1);
      
      for (uint64_t k = 0; k < x.size(); k++) {
	uint64_t i0, i1;
	i1 = k % m1;
	i0 = k / m1;
	typename ZZq::value_type wij = pown<ZZq>(w_m, i1*i0, q);
	y[k] = ((typename ZZq::greater_value_type)y[k] * wij) % q;
      }
      return y;
    }
  case BT_TWIDDLE_CRT_INV:
    w_m = modinv<ZZq>(w_m, q);
    /* fallthrough */
  case BT_TWIDDLE_CRT:
    {
      auto fs = factor(get_m(t));
      assert(fs.size() == 1);
      typename ZZq::value_type m1 = pown(fs[0].first, fs[0].second-1);

      for (uint64_t i = 1; i < fs[0].first; i++) {
	for (uint64_t j = 0; j < m1; j++) {
	  typename ZZq::value_type wij = pown<ZZq>(w_m, i*j, q);
	  y[(i-1)*m1 + j] = ((typename ZZq::greater_value_type)y[(i-1)*m1 + j] * wij) % q;
	}
      }
      return y;
    }
  case BT_DFT:
    {
      for (uint64_t i = 0; i < x.size(); i++) {
	y[i] = 0;
	for (uint64_t j = 0; j < x.size(); j++) {
	  typename ZZq::value_type wij = pown<ZZq>(w_m, i*j, q);
	  y[i] = ((typename ZZq::greater_value_type)x[j] * wij + y[i]) % q;
	}
      }
      return y;
    }
  case BT_DFT_INV:
    {
      w_m = modinv<ZZq>(w_m, q);
      typename ZZq::value_type minv  = modinv<ZZq>(get_m(t), q);
      for (uint64_t i = 0; i < x.size(); i++) {
	y[i] = 0;
	for (uint64_t j = 0; j < x.size(); j++) {
	  typename ZZq::value_type wij = pown<ZZq>(w_m, i*j, q);
	  y[i] = ((typename ZZq::greater_value_type)x[j] * wij + y[i]) % q;
	}
      }
      for (uint64_t i = 0; i < x.size(); i++) {
	y[i] = ((typename ZZq::greater_value_type)y[i] * minv) % q;
      }
      return y;
    }
  case BT_CRT:
    {
      assert(x.size() == get_m(t)-1);
      uint64_t phi_m = get_m(t) - 1;
      
      for (uint64_t i = 1; i < get_m(t); i++) {
	y[i-1] = 0;
	for (uint64_t j = 0; j < phi_m; j++) {
	  typename ZZq::value_type wij = pown<ZZq>(w_m, i*j, q);
	  y[i-1] = ((typename ZZq::greater_value_type)x[j] * wij + y[i-1]) % q;
	}
      }
      return y;
    }
  case BT_CRT_INV:
    {
      assert(x.size() == get_m(t)-1);
      uint64_t phi_m = get_m(t) - 1;
      w_m   = modinv<ZZq>(w_m, q);
      typename ZZq::value_type minv  = modinv<ZZq>(get_m(t), q);

      for (uint64_t i = 0; i < phi_m; i++) {
	y[i] = 0;
	for (uint64_t j = 1; j < get_m(t); j++) {
	  typename ZZq::value_type wij = pown<ZZq>(w_m, i*j, q);
	  y[i] = ((typename ZZq::greater_value_type)x[j-1] * wij + y[i]) % q;
	}
	y[i] = ((typename ZZq::greater_value_type)y[i] * minv) % q;
      }

      typename ZZq::value_type sum = 0;
      for (uint64_t i = 0; i < phi_m; i++)
	sum = ((typename ZZq::greater_value_type)sum + y[i]) % q;
      for (uint64_t i = 0; i < phi_m; i++)
	y[i] = ((typename ZZq::greater_value_type)sum + y[i]) % q;
      return y;
    }
  default:
    abort();
  }

  return y;
}

template<typename ZZq>
std::vector<typename ZZq::value_type>
eval_zzq (const Trans *t,
	  std::vector<typename ZZq::value_type> x,
	  typename ZZq::value_type q,
	  uint64_t index)
{
  std::vector<typename ZZq::value_type> y(x);
  uint64_t n     = x.size();
  typename ZZq::value_type w0 =
    findElemOfOrder<ZZq>(index, q, factor(index));

  if (dsl2_get_debug()) {
    std::cout << "###### EVAL_ZZq #####\n";
    print_perm(x); std::cout << "\n";
  }

  for (const Trans *it = t;
       get_type(it) != T_ID;
       it = it->m_trans.m_transf.prev) {
    if (dsl2_get_debug()) {
      std::cout << "transform_dim = " << get_dim(it) << std::endl;
      std::cout << "n = " << n << std::endl;
    }
    assert(get_dim(it) == n);

    if (dsl2_get_debug())
      std::cout << *it << "\n";
    
    uint64_t l = it->m_trans.m_transf.l;
    uint64_t r = it->m_trans.m_transf.r;
    uint64_t m = n / (l*r);

    y = perm(y, r);
    if (dsl2_get_debug()) {
      print_perm(y); std::cout << "\n";
    }
    for (uint64_t j = 0; j < l*r; j++) {
      std::vector<typename ZZq::value_type>
	y_slice (std::begin(y) + j * m,
		 std::begin(y) + (j+1) * m);

      y_slice = apply_zzq<ZZq>
	(it->m_trans.m_transf.f,
	 y_slice,
	 pown<ZZq>(w0, index/get_m(it->m_trans.m_transf.f), q),
	 q);
      
      std::copy(std::begin(y_slice),
		std::end(y_slice),
		std::begin(y) + j * m);
    }

    y = perm(y, l*m);
    if (dsl2_get_debug()) {
      print_perm(y); std::cout << "\n";
    }
  }

  return y;
}

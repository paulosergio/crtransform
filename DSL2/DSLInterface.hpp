#pragma once
#include <vector>
#include <cstdint>

template<typename ZZq, typename ZZ>
struct DSLInterface
{
  virtual std::vector<typename ZZq::value_type>
  eval_crt (const std::vector<typename ZZq::value_type> &x) = 0;
  
  virtual std::vector<typename ZZq::value_type>
  eval_icrt (const std::vector<typename ZZq::value_type> &x) = 0;
  
  virtual std::vector<typename ZZ::value_type>
  eval_l (const std::vector<typename ZZ::value_type> &x) = 0;
  
  virtual std::vector<typename ZZ::value_type>
  eval_il (const std::vector<typename ZZ::value_type> &x) = 0;
  
  virtual std::vector<double>
  eval_crt_b (const std::vector<double> &x) = 0;

  virtual ~DSLInterface() {};
};

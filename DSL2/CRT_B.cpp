#include "CRT_B.hpp"
#include <memory>
#include "NumTh.hpp"

Trans *get_crt_b_base(uint64_t p, uint64_t k)
{
  using TransP = std::unique_ptr<Trans>;

  uint64_t mlp   = pown(p, k-1);
  TransP   crt_b (make_transf(1, 1, make_crt_b(p)));
  TransP   id    (make_id(mlp));

  return Kronecker(crt_b.get(), id.get());
}

Trans *get_crt_b(uint64_t m)
{
  return KroneckerProd(get_crt_b_base, m);
}

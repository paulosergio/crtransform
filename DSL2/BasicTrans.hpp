#pragma once
#include <cstdint>
#undef str
#include <iostream>
#undef str
#include <sstream>

/* Basic operations used by the DSL */
enum BasicTransType {
  BT_PERM,
  BT_DFT,
  BT_TWIDDLE,
  BT_CRT,
  BT_TWIDDLE_CRT,
  BT_DFT_INV,
  BT_TWIDDLE_INV,
  BT_CRT_INV,
  BT_TWIDDLE_CRT_INV,
  BT_CRT_B,
  BT_L,
  BT_L_INV
};

struct BasicTrans {
  BasicTransType m_type;

  union Trans_u {
    
    struct Perm_s {
      uint64_t m, n;
    } m_perm;

    struct DFT_s {
      uint64_t m;
    } m_dft;

    struct Twiddle_s {
      uint64_t m;
    } m_twiddle;

    struct CRT_s {
      uint64_t m;
    } m_crt;

    struct TwiddleCRT_s {
      uint64_t m;
    } m_twiddle_crt;

    struct DFTInv_s {
      uint64_t m;
    } m_dft_inv;

    struct TwiddleInv_s {
      uint64_t m;
    } m_twiddle_inv;

    struct CRTInv_s {
      uint64_t m;
    } m_crt_inv;

    struct TwiddleCRTInv_s {
      uint64_t m;
    } m_twiddle_crt_inv;

    struct CRT_B_s {
      uint64_t m;
    } m_crt_b;

    struct L_s {
      uint64_t m;
    } m_l;

    struct LInv_s {
      uint64_t m;
    } m_l_inv;

  } m_trans;
};

#define STRINGIFY(x) #x
#define CONCAT(x, y) x ## y

#define CONSTRUCTOR1(TYPE, MEMBER, NAME)	\
  BasicTrans CONCAT(make_,NAME)(uint64_t m);

#define CONSTRUCTOR2(TYPE, MEMBER, NAME)	 \
  BasicTrans CONCAT(make_,NAME)(uint64_t m,	 \
				uint64_t n);

CONSTRUCTOR2(BT_PERM, m_perm, permutation)
CONSTRUCTOR1(BT_DFT, m_dft, dft)
CONSTRUCTOR1(BT_TWIDDLE, m_twiddle, twiddle)
CONSTRUCTOR1(BT_CRT, m_crt, crt)
CONSTRUCTOR1(BT_TWIDDLE_CRT, m_twiddle_crt, twiddle_crt)
CONSTRUCTOR1(BT_DFT_INV, m_dft_inv, dft_inv)
CONSTRUCTOR1(BT_TWIDDLE_INV, m_twiddle_inv, twiddle_inv)
CONSTRUCTOR1(BT_CRT_INV, m_crt_inv, crt_inv)
CONSTRUCTOR1(BT_TWIDDLE_CRT_INV, m_twiddle_crt_inv, twiddle_crt_inv)
CONSTRUCTOR1(BT_CRT_B, m_crt_b, crt_b)
CONSTRUCTOR1(BT_L, m_l, l)
CONSTRUCTOR1(BT_L_INV, m_l_inv, l_inv)

#undef CONSTRUCTOR1
#undef CONSTRUCTOR2

#define PRINTER1(TYPE, MEMBER, NAME)			\
  std::string CONCAT(print_,NAME)(const BasicTrans &t);

#define PRINTER2(TYPE, MEMBER, NAME)			\
  std::string CONCAT(print_,NAME)(const BasicTrans &t);

PRINTER2(BT_PERM, m_perm, permutation)
PRINTER1(BT_DFT, m_dft, dft)
PRINTER1(BT_TWIDDLE, m_twiddle, twiddle)
PRINTER1(BT_CRT, m_crt, crt)
PRINTER1(BT_TWIDDLE_CRT, m_twiddle_crt, twiddle_crt)
PRINTER1(BT_DFT_INV, m_dft_inv, dft_inv)
PRINTER1(BT_TWIDDLE_INV, m_twiddle_inv, twiddle_inv)
PRINTER1(BT_CRT_INV, m_crt_inv, crt_inv)
PRINTER1(BT_TWIDDLE_CRT_INV, m_twiddle_crt_inv, twiddle_crt_inv)
PRINTER1(BT_CRT_B, m_crt_b, crt_b)
PRINTER1(BT_L, m_l, l)
PRINTER1(BT_L_INV, m_l_inv, l_inv)

#undef PRINTER1
#undef PRINTER2

std::ostream& operator<<(std::ostream &os, const BasicTrans &t);
uint64_t get_dim(const BasicTrans &t);
uint64_t get_m(const BasicTrans &t);
BasicTransType get_type(const BasicTrans &t);
uint64_t get_permutation_n(const BasicTrans &t);
void set_permutation_n(BasicTrans &t, uint64_t n);

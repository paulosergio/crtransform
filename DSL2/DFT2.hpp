#pragma once
#include "DSL2.hpp"
#include <vector>
#include <tuple>
#include <cstdint>

Trans *get_dft(uint64_t m);
Trans *get_idft(uint64_t m);

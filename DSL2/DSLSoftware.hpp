#pragma once
#include "DSLInterface.hpp"
#include "DSL2.hpp"
#include <memory>
#include "CRT2.hpp"
#include "L.hpp"
#include "CRT_B.hpp"

template<typename ZZq, typename ZZ>
struct DSLSoftware : public DSLInterface<ZZq,ZZ>
{
  using TransP = std::unique_ptr<Trans>;

  uint64_t m_m, m_q;
  TransP crt, icrt, l, il, crt_b;
  
  DSLSoftware(uint64_t m, uint64_t q);

  virtual std::vector<typename ZZq::value_type>
  eval_crt (const std::vector<typename ZZq::value_type> &x);
  
  virtual std::vector<typename ZZq::value_type>
  eval_icrt (const std::vector<typename ZZq::value_type> &x);
  
  virtual std::vector<typename ZZ::value_type>
  eval_l (const std::vector<typename ZZ::value_type> &x);
  
  virtual std::vector<typename ZZ::value_type>
  eval_il (const std::vector<typename ZZ::value_type> &x);
  
  virtual std::vector<double>
  eval_crt_b (const std::vector<double> &x);

  virtual ~DSLSoftware();
};

template<typename ZZq, typename ZZ>
DSLSoftware<ZZq,ZZ>::DSLSoftware(uint64_t m, uint64_t q) :
  m_m(m),
  m_q(q),
  crt(get_crt(m)),
  icrt(get_icrt(m)),
  l(get_l(m)),
  il(get_il(m)),
  crt_b(get_crt_b(m))
{
}

template<typename ZZq, typename ZZ>
std::vector<typename ZZq::value_type>
DSLSoftware<ZZq,ZZ>::eval_crt
(const std::vector<typename ZZq::value_type> &x)
{
  return eval_zzq<ZZq>(crt.get(), x, m_q, m_m);
}

template<typename ZZq, typename ZZ>
std::vector<typename ZZq::value_type>
DSLSoftware<ZZq,ZZ>::eval_icrt
(const std::vector<typename ZZq::value_type> &x)
{
  return eval_zzq<ZZq>(icrt.get(), x, m_q, m_m);
}

template<typename ZZq, typename ZZ>
std::vector<typename ZZ::value_type>
DSLSoftware<ZZq,ZZ>::eval_l
(const std::vector<typename ZZ::value_type> &x)
{
  return eval_zz<ZZ>(l.get(), x, m_m);
}

template<typename ZZq, typename ZZ>
std::vector<typename ZZ::value_type>
DSLSoftware<ZZq,ZZ>::eval_il
(const std::vector<typename ZZ::value_type> &x)
{
  return eval_zz<ZZ>(il.get(), x, m_m);
}

template<typename ZZq, typename ZZ>
std::vector<double>
DSLSoftware<ZZq,ZZ>::eval_crt_b
(const std::vector<double> &x)
{
  return eval_rr(crt_b.get(), x, m_m);
}

template<typename ZZq, typename ZZ>
DSLSoftware<ZZq,ZZ>::~DSLSoftware()
{
}

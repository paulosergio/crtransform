#pragma once
#include "DSL2.hpp"
#include <vector>
#include <tuple>
#include <cstdint>

Trans *get_crt(uint64_t m);
Trans *get_icrt(uint64_t m);

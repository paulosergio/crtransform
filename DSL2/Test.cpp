#define CATCH_CONFIG_MAIN
#include "catch.hpp"
#include "NumTh.hpp"
#include "CRT2.hpp"
#include "DFT2.hpp"
#include "DSL2.hpp"
#include <vector>
#include <cstdint>
#include <functional>
#include <algorithm>
#include <memory>
#include <random>
#include <iostream>
#include "PrettyPrinter.hpp"

/* Compares the DFT, IDFT, CRT and ICRT as computed by the
   DSL with their equivalent using ordinary matrix arithmetic */

using vector_t = std::vector<uint64_t>;
using matrix_t = std::vector<vector_t>;

static matrix_t get_dft_prime_power(uint64_t m, uint64_t w, uint64_t q)
{
  matrix_t dft(m, vector_t(m));

  for (uint64_t i = 0; i < m; i++) {
    for (uint64_t j = 0; j < m; j++) {
      dft[i][j] = pown<ZZq64_t>(w, i*j, q);
    }
  }

  return dft;
}

static matrix_t get_crt_prime_power(uint64_t m, uint64_t w, uint64_t q)
{
  uint64_t phi_m = totient(m);
  matrix_t crt(phi_m, vector_t(phi_m));

  uint64_t ii = 0;

  for (uint64_t i = 1; i <= m; i++) {
    if (gcd(i, m) == 1) {
      for (uint64_t j = 0; j < phi_m; j++) {
	crt[ii][j] = pown<ZZq64_t>(w, i*j, q);
      }
      ii++;
    }
  }

  return crt;
}

static matrix_t KroneckerM(matrix_t a, matrix_t b, uint64_t q)
{
  uint64_t ha = a.size();
  uint64_t hb = b.size();
  assert(a.size() > 0 && b.size() > 0);
  uint64_t wa = a[0].size();
  uint64_t wb = b[0].size();
  matrix_t c(ha*hb,
	     vector_t(wa*wb));

  for (uint64_t i = 0; i < ha*hb; i++) {
    uint64_t ia = i / hb;
    uint64_t ib = i - ia * hb;
    for (uint64_t j = 0; j < wa*wb; j++) {
      uint64_t ja = j / wb;
      uint64_t jb = j - ja * wb;

      c[i][j] = ((uint128_t)a[ia][ja]*b[ib][jb]) % q;
    }
  }
  
  return c;
}

static matrix_t KroneckerProdM
(std::function<matrix_t(uint64_t,uint64_t,uint64_t)> base,
 uint64_t m, uint64_t q)
{
  auto fs    = factor(m);
  uint64_t w = findElemOfOrder<ZZq64_t>(m, q, fs);
  uint64_t p, k;
  std::tie(p, k) = fs[0];
  uint64_t pk = pown(p, k);
  auto prod = base(pk,
		   pown<ZZq64_t>(w, m/pk, q),
		   q);
  
  for (auto it = std::begin(fs) + 1; it != std::end(fs); it++) {
    std::tie(p, k) = *it;
    uint64_t pk = pown(p, k);
    auto tmp = base(pk,
		    pown<ZZq64_t>(w, m/pk, q),
		    q);
    prod     = KroneckerM(prod, tmp, q);
  }

  return prod;
}

static vector_t product(matrix_t a, vector_t v, uint64_t q)
{
  vector_t v1(a.size(), 0);
  assert(a.size() > 0 && (v.size() == a[0].size()));

  for (uint64_t i = 0; i < a.size(); i++) {
    for (uint64_t j = 0; j < v.size(); j++) {
      v1[i] = ((uint128_t)a[i][j] * v[j] + v1[i]) % q;
    }
  }

  return v1;
}

TEST_CASE("Testing DFT", "[dft]")
{
  uint64_t q = 4051;
  // q = 1 mod 3*3*3*5*5
  uint64_t numtests = 100;
  std::default_random_engine gen;
  std::uniform_int_distribution<uint64_t> dist(0, q-1);
  auto gen_input = [&gen, &dist] (std::vector<uint64_t> &x) {
    std::generate(std::begin(x),
		  std::end(x),
		  [&gen, &dist] () {
		    return dist(gen);
		  });
  };

  SECTION("m = 3") {
    uint64_t m = 3;
    std::unique_ptr<Trans> dft(get_dft(m));
    matrix_t dft1 = KroneckerProdM(get_dft_prime_power,
				   m,
				   q);

    for (uint64_t i = 0; i < numtests; i++) {
      vector_t x(m, 0);
      gen_input(x);
      auto y  = eval_zzq<ZZq64_t>(dft.get(), x, q, m);
      auto y1 = product(dft1, x, q);

      REQUIRE(std::equal(std::begin(y),
			 std::end(y),
			 std::begin(y1)));
    }
  }

  SECTION("m = 3*3*3") {
    uint64_t m = 3*3*3;
    std::unique_ptr<Trans> dft(get_dft(m));
    matrix_t dft1 = KroneckerProdM(get_dft_prime_power,
				   m,
				   q);

    for (uint64_t i = 0; i < numtests; i++) {
      vector_t x(m, 0);
      gen_input(x);
      auto y  = eval_zzq<ZZq64_t>(dft.get(), x, q, m);
      auto y1 = product(dft1, x, q);

      REQUIRE(std::equal(std::begin(y),
			 std::end(y),
			 std::begin(y1)));
    }
  }

  SECTION("m = 5*5") {
    uint64_t m = 5*5;
    std::unique_ptr<Trans> dft(get_dft(m));
    matrix_t dft1 = KroneckerProdM(get_dft_prime_power,
				   m,
				   q);

    for (uint64_t i = 0; i < numtests; i++) {
      vector_t x(m, 0);
      gen_input(x);
      auto y  = eval_zzq<ZZq64_t>(dft.get(), x, q, m);
      auto y1 = product(dft1, x, q);

      REQUIRE(std::equal(std::begin(y),
			 std::end(y),
			 std::begin(y1)));
    }
  }

  SECTION("m = 3*3*3*5*5") {
    uint64_t m = 3*3*3*5*5;
    std::unique_ptr<Trans> dft(get_dft(m));
    matrix_t dft1 = KroneckerProdM(get_dft_prime_power,
				   m,
				   q);

    for (uint64_t i = 0; i < numtests; i++) {
      vector_t x(m, 0);
      gen_input(x);
      auto y  = eval_zzq<ZZq64_t>(dft.get(), x, q, m);
      auto y1 = product(dft1, x, q);

      REQUIRE(std::equal(std::begin(y),
			 std::end(y),
			 std::begin(y1)));
    }
  }
}

TEST_CASE("Testing IDFT", "[idft]")
{
  uint64_t q = 4051;
  // q = 1 mod 3*3*3*5*5
  uint64_t numtests = 100;
  std::default_random_engine gen;
  std::uniform_int_distribution<uint64_t> dist(0, q-1);
  auto gen_input = [&gen, &dist] (std::vector<uint64_t> &x) {
    std::generate(std::begin(x),
		  std::end(x),
		  [&gen, &dist] () {
		    return dist(gen);
		  });
  };

  SECTION("m = 3") {
    uint64_t m = 3;
    std::unique_ptr<Trans> idft(get_idft(m));
    matrix_t dft1 = KroneckerProdM(get_dft_prime_power,
				   m,
				   q);

    for (uint64_t i = 0; i < numtests; i++) {
      vector_t x(m, 0);
      gen_input(x);
      auto y  = product(dft1, x, q);
      auto x1 = eval_zzq<ZZq64_t>(idft.get(), y, q, m);

      REQUIRE(std::equal(std::begin(x),
			 std::end(x),
			 std::begin(x1)));
    }
  }

  SECTION("m = 3*3*3") {
    uint64_t m = 3*3*3;
    std::unique_ptr<Trans> idft(get_idft(m));
    matrix_t dft1 = KroneckerProdM(get_dft_prime_power,
				   m,
				   q);

    for (uint64_t i = 0; i < numtests; i++) {
      vector_t x(m, 0);
      gen_input(x);
      auto y  = product(dft1, x, q);
      auto x1 = eval_zzq<ZZq64_t>(idft.get(), y, q, m);

      REQUIRE(std::equal(std::begin(x),
			 std::end(x),
			 std::begin(x1)));
    }
  }

  SECTION("m = 5*5") {
    uint64_t m = 5*5;
    std::unique_ptr<Trans> idft(get_idft(m));
    matrix_t dft1 = KroneckerProdM(get_dft_prime_power,
				   m,
				   q);

    for (uint64_t i = 0; i < numtests; i++) {
      vector_t x(m, 0);
      gen_input(x);
      auto y  = product(dft1, x, q);
      auto x1 = eval_zzq<ZZq64_t>(idft.get(), y, q, m);

      REQUIRE(std::equal(std::begin(x),
			 std::end(x),
			 std::begin(x1)));
    }
  }

  SECTION("m = 3*3*3*5*5") {
    uint64_t m = 3*3*3*5*5;
    std::unique_ptr<Trans> idft(get_idft(m));
    matrix_t dft1 = KroneckerProdM(get_dft_prime_power,
				   m,
				   q);

    for (uint64_t i = 0; i < numtests; i++) {
      vector_t x(m, 0);
      gen_input(x);
      auto y  = product(dft1, x, q);
      auto x1 = eval_zzq<ZZq64_t>(idft.get(), y, q, m);

      REQUIRE(std::equal(std::begin(x),
			 std::end(x),
			 std::begin(x1)));
    }
  }
}

TEST_CASE("Testing CRT", "[crt]")
{
  uint64_t q = 4051;
  // q = 1 mod 3*3*3*5*5
  uint64_t numtests = 100;
  std::default_random_engine gen;
  std::uniform_int_distribution<uint64_t> dist(0, q-1);
  auto gen_input = [&gen, &dist] (std::vector<uint64_t> &x) {
    std::generate(std::begin(x),
		  std::end(x),
		  [&gen, &dist] () {
		    return dist(gen);
		  });
  };

  SECTION("m = 3") {
    uint64_t m = 3;
    std::unique_ptr<Trans> crt(get_crt(m));
    matrix_t crt1 = KroneckerProdM(get_crt_prime_power,
				   m,
				   q);

    for (uint64_t i = 0; i < numtests; i++) {
      vector_t x(totient(m), 0);
      gen_input(x);
      auto y  = eval_zzq<ZZq64_t>(crt.get(), x, q, m);
      auto y1 = product(crt1, x, q);

      REQUIRE(std::equal(std::begin(y),
			 std::end(y),
			 std::begin(y1)));
    }
  }

  SECTION("m = 3*3*3") {
    uint64_t m = 3*3*3;
    std::unique_ptr<Trans> crt(get_crt(m));
    matrix_t crt1 = KroneckerProdM(get_crt_prime_power,
				   m,
				   q);

    for (uint64_t i = 0; i < numtests; i++) {
      vector_t x(totient(m), 0);
      gen_input(x);
      auto y  = eval_zzq<ZZq64_t>(crt.get(), x, q, m);
      auto y1 = product(crt1, x, q);

      REQUIRE(std::equal(std::begin(y),
			 std::end(y),
			 std::begin(y1)));
    }
  }

  SECTION("m = 5*5") {
    uint64_t m = 5*5;
    std::unique_ptr<Trans> crt(get_crt(m));
    matrix_t crt1 = KroneckerProdM(get_crt_prime_power,
				   m,
				   q);

    for (uint64_t i = 0; i < numtests; i++) {
      vector_t x(totient(m), 0);
      gen_input(x);
      auto y  = eval_zzq<ZZq64_t>(crt.get(), x, q, m);
      auto y1 = product(crt1, x, q);

      REQUIRE(std::equal(std::begin(y),
			 std::end(y),
			 std::begin(y1)));
    }
  }

  SECTION("m = 3*3*3*5*5") {
    uint64_t m = 3*3*3*5*5;
    std::unique_ptr<Trans> crt(get_crt(m));
    matrix_t crt1 = KroneckerProdM(get_crt_prime_power,
				   m,
				   q);

    for (uint64_t i = 0; i < numtests; i++) {
      vector_t x(totient(m), 0);
      gen_input(x);
      auto y  = eval_zzq<ZZq64_t>(crt.get(), x, q, m);
      auto y1 = product(crt1, x, q);

      REQUIRE(std::equal(std::begin(y),
			 std::end(y),
			 std::begin(y1)));
    }
  }
}

TEST_CASE("Testing ICRT", "[icrt]")
{
  uint64_t q = 4051;
  // q = 1 mod 3*3*3*5*5
  uint64_t numtests = 100;
  std::default_random_engine gen;
  std::uniform_int_distribution<uint64_t> dist(0, q-1);
  auto gen_input = [&gen, &dist] (std::vector<uint64_t> &x) {
    std::generate(std::begin(x),
		  std::end(x),
		  [&gen, &dist] () {
		    return dist(gen);
		  });
  };

  SECTION("m = 3") {
    uint64_t m = 3;
    std::unique_ptr<Trans> icrt(get_icrt(m));
    matrix_t crt1 = KroneckerProdM(get_crt_prime_power,
				   m,
				   q);

    for (uint64_t i = 0; i < numtests; i++) {
      vector_t x(totient(m), 0);
      gen_input(x);
      auto y  = product(crt1, x, q);
      auto x1 = eval_zzq<ZZq64_t>(icrt.get(), y, q, m);

      REQUIRE(std::equal(std::begin(x),
			 std::end(x),
			 std::begin(x1)));
    }
  }

  SECTION("m = 3*3*3") {
    uint64_t m = 3*3*3;
    std::unique_ptr<Trans> icrt(get_icrt(m));
    matrix_t crt1 = KroneckerProdM(get_crt_prime_power,
				   m,
				   q);

    for (uint64_t i = 0; i < numtests; i++) {
      vector_t x(totient(m), 0);
      gen_input(x);
      auto y  = product(crt1, x, q);
      auto x1 = eval_zzq<ZZq64_t>(icrt.get(), y, q, m);

      REQUIRE(std::equal(std::begin(x),
			 std::end(x),
			 std::begin(x1)));
    }
  }

  SECTION("m = 5*5") {
    uint64_t m = 5*5;
    std::unique_ptr<Trans> icrt(get_icrt(m));
    matrix_t crt1 = KroneckerProdM(get_crt_prime_power,
				   m,
				   q);

    for (uint64_t i = 0; i < numtests; i++) {
      vector_t x(totient(m), 0);
      gen_input(x);
      auto y  = product(crt1, x, q);
      auto x1 = eval_zzq<ZZq64_t>(icrt.get(), y, q, m);

      REQUIRE(std::equal(std::begin(x),
			 std::end(x),
			 std::begin(x1)));
    }
  }

  SECTION("m = 3*3*3*5*5") {
    uint64_t m = 3*3*3*5*5;
    std::unique_ptr<Trans> icrt(get_icrt(m));
    matrix_t crt1 = KroneckerProdM(get_crt_prime_power,
				   m,
				   q);

    for (uint64_t i = 0; i < numtests; i++) {
      vector_t x(totient(m), 0);
      gen_input(x);
      auto y  = product(crt1, x, q);
      auto x1 = eval_zzq<ZZq64_t>(icrt.get(), y, q, m);

      REQUIRE(std::equal(std::begin(x),
			 std::end(x),
			 std::begin(x1)));
    }
  }
}

#include "L.hpp"
#include <memory>
#include "NumTh.hpp"

Trans *get_l_base(uint64_t p, uint64_t k)
{
  using TransP = std::unique_ptr<Trans>;

  uint64_t mlp   = pown(p, k-1);
  TransP   l     (make_transf(1, 1, make_l(p)));
  TransP   id    (make_id(mlp));

  return Kronecker(l.get(), id.get());
}

Trans *get_l(uint64_t m)
{
  return KroneckerProd(get_l_base, m);
}

Trans *get_il_base(uint64_t p, uint64_t k)
{
  using TransP = std::unique_ptr<Trans>;

  uint64_t mlp   = pown(p, k-1);
  TransP   l_inv (make_transf(1, 1, make_l_inv(p)));
  TransP   id    (make_id(mlp));

  return Kronecker(l_inv.get(), id.get());
}

Trans *get_il(uint64_t m)
{
  return KroneckerProd(get_il_base, m);
}


#include "DFT2.hpp"
#include "NumTh.hpp"
#include <memory>

Trans *get_dft_base(uint64_t p, uint64_t k)
{
  using TransP = std::unique_ptr<Trans>;
  
  if (k > 1) {
    uint64_t m1    = pown(p, k-1);
    uint64_t m     = m1 * p;
      
    TransP perm    (make_transf(1, 1, make_permutation(m, m1)));
    TransP idp     (make_id(p));
    TransP dft_m1  (get_dft_base(p, k-1));
    TransP tmp1    (Kronecker(idp.get(), dft_m1.get()));
    TransP tmp2    (Dot(perm.get(), tmp1.get()));
    TransP twiddle (make_transf(1, 1, make_twiddle(m)));
    TransP tmp3    (Dot(tmp2.get(), twiddle.get()));
    TransP dft_p   (get_dft_base(p, 1));
    TransP idm1    (make_id(m1));
    TransP tmp4    (Kronecker(dft_p.get(), idm1.get()));
      
    return Dot(tmp3.get(), tmp4.get());
  } else {
    return make_transf(1, 1, make_dft(p));
  }
}

Trans *get_dft(uint64_t m)
{
  return KroneckerProd(get_dft_base, m);
}

Trans *get_idft_base(uint64_t p, uint64_t k)
{
  using TransP = std::unique_ptr<Trans>;
  
  if (k > 1) {
    uint64_t m1    = pown(p, k-1);
    uint64_t m     = m1 * p;
      
    TransP perm    (make_transf(1, 1, make_permutation(m, m/m1)));
    TransP idp     (make_id(p));
    TransP dft_m1  (get_idft_base(p, k-1));
    TransP tmp1    (Kronecker(idp.get(), dft_m1.get()));
    TransP tmp2    (Dot(tmp1.get(), perm.get()));
    TransP twiddle (make_transf(1, 1, make_twiddle_inv(m)));
    TransP tmp3    (Dot(twiddle.get(), tmp2.get()));
    TransP dft_p   (get_idft_base(p, 1));
    TransP idm1    (make_id(m1));
    TransP tmp4    (Kronecker(dft_p.get(), idm1.get()));
      
    return Dot(tmp4.get(), tmp3.get());
  } else {
    return make_transf(1, 1, make_dft_inv(p));
  }
}

Trans *get_idft(uint64_t m)
{
  return KroneckerProd(get_idft_base, m);
}


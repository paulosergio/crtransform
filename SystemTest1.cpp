#include "System1.hpp"
#include <random>
#include <iomanip>
#include "CRT2.hpp"
#include <array>
#include "NumTh.hpp"
#include <NTL/ZZ.h>

struct SystemTestbench : sc_module
{
  static constexpr uint64_t numtests = 10;
  std::array<uint64_t, numtests> ws, ms, ns, qs;
  
  std::array<Trans*, numtests> ts;
  std::array<Glue*, numtests> params;
  std::vector<std::string> uut_names;
  std::array<System*, numtests> uut;
  
  sc_in<bool> clk;
  std::array<std::vector<sc_signal<uint64_t>>, numtests> uut_x_in;
  std::array<std::vector<sc_signal<uint64_t>>, numtests> uut_y_out;
  std::array<sc_signal<bool>, numtests> uut_start;

  std::default_random_engine gen;
  std::vector<std::uniform_int_distribution<uint64_t>> dists;
  std::uniform_int_distribution<uint64_t> distM;
  std::uniform_int_distribution<uint64_t> distW;

  Trans *distT(uint64_t m)
  {
    std::uniform_int_distribution<uint64_t> _distT(0,1);

    switch(_distT(gen)) {
    case 0: std::cout << "CRT\n"; return get_crt(m);
    case 1: std::cout << "ICRT\n"; return get_icrt(m);
    default: std::cout << "ERROR\n"; return nullptr;
    }
  }

  void process()
  {
    wait();

    for (uint64_t i = 0; i < numtests; i++) {
      std::cout << "######## TEST " << i << " #######\n";
      
      std::vector<uint64_t> x(uut[i]->m_params.m_n);
      std::generate(std::begin(x),
		    std::end(x),
		    [this,i] () {
		      return this->dists[i](this->gen);
		    });

      uut_start[i].write(true);

      for (uint64_t j = 0; j < uut[i]->perm->m_bsize; j++) {
	for (uint64_t h = 0; h < ws[i]; h++) {
	  if (h * uut[i]->perm->m_bsize + j < x.size()) {
	    std::cout << sc_time_stamp() << " writing: ";
	    std::cout << x[h * uut[i]->perm->m_bsize + j]
	    	      << " into " << h << "\n";
	    uut_x_in[i][h].write(x[h * uut[i]->perm->m_bsize + j]);
	  } else {
	    uut_x_in[i][h].write(0);
	  }
	}
	wait();
	uut_start[i].write(false);
      }

      for (uint64_t j = 0; j < uut[i]->total_exec_time - uut[i]->perm->m_bsize; j++)
	wait();

      std::vector<uint64_t> y(x.size());
      auto weights = uut[i]->m_params.m_data_allocation.back();
      for (uint64_t j = 0; j < uut[i]->perm->m_bsize; j++) {
	wait(5.0, SC_NS);
	for (uint64_t h = 0; h < ws[i]; h++) {
	  if (j < weights[h]) {
	    uint64_t k = from_additive(weights,
				       std::make_pair(h, j));
	    y[k] = uut_y_out[i][h].read();
	    std::cout << sc_time_stamp() << " reading: ";
	    std::cout << y[k] << " into " << k << "\n";
	  }
	}
	wait();
      }

      auto yexp = eval(ts[i], x, qs[i], ms[i]);
      print_perm(yexp); std::cout << "\n";
      auto y1 = eval(uut[i]->m_params.m_ops, x, qs[i], ms[i]);
      print_perm(y1); std::cout << "\n";
      assert(std::equal(std::begin(y),
			std::end(y),
			std::begin(yexp)));
    }

    std::cout << "SUCCESS\n";

    sc_stop();
  }

  SystemTestbench(sc_module_name name, uint64_t seed)
    : sc_module(name)
    , distM(20,30)
    , distW(4,10)
    , gen(seed)
  {
    for (uint64_t i = 0; i < numtests; i++) {
      std::cout << "####### SETUP " << i << " ######\n";
      ms[i] = distM(gen);
      qs[i] = ms[i] + 1;
      while (!NTL::ProbPrime(qs[i]))
	qs[i] += ms[i];
      dists.emplace_back(0, qs[i]-1);
      ws[i] = distW(gen);
      ns[i] = totient(ms[i]);
      std::cout << "q = " << qs[i] << "\n";
      std::cout << "m = " << ms[i] << "\n";
      std::cout << "w = " << ws[i] << "\n";
      std::cout << "n = " << ns[i] << "\n";
      ts[i] = distT(ms[i]);
      params[i] = new Glue(ws[i], ts[i], ms[i], qs[i]);
      uut_names.emplace_back(std::string("uut_") +
			     std::to_string(i));
      uut[i] = new System(uut_names[i].c_str(), *params[i]);
      uut[i]->clk(clk);
      uut[i]->start(uut_start[i]);
      uut_x_in[i]  = std::vector<sc_signal<uint64_t>>(ws[i]);
      uut_y_out[i] = std::vector<sc_signal<uint64_t>>(ws[i]);;

      for (uint64_t h = 0; h < ws[i]; h++) {
	uut[i]->x_in[h](uut_x_in[i][h]);
	uut[i]->y_out[h](uut_y_out[i][h]);
      }
    }

    SC_THREAD(process);
    sensitive << clk.pos();
  }

  virtual ~SystemTestbench()
  {
    for (auto t : ts)
      delete t;
    for (auto s : uut)
      delete s;
    for (auto g : params)
      delete g;
  }

  SC_HAS_PROCESS(SystemTestbench);
};

int sc_main(int argc, char *argv[])
{
  sc_clock TestClk("TestClock", 10, SC_NS, 0.5);

  uint64_t seed = 0;
  if (argc > 1) {
    seed = std::stoi(argv[1]);
  }
  
  SystemTestbench t("SystemTestbench", seed);
  t.clk(TestClk);

  sc_trace_file *Tf;
  Tf = sc_create_vcd_trace_file("traces");
  Tf->set_time_unit(1, sc_time_unit::SC_NS);

  const uint64_t debugSystem = 6;

  std::vector<std::string> perm_in_names(t.ws[debugSystem]);
  for (uint64_t i = 0; i < t.ws[debugSystem]; i++) {
    perm_in_names[i] = std::string("perm_in_") + std::to_string(i);
    sc_trace(Tf, t.uut[debugSystem]->perm_in[i], perm_in_names[i].c_str());
  }
  sc_trace(Tf, t.uut[debugSystem]->perm_start, "perm_start");
  sc_trace(Tf, t.uut[debugSystem]->proc_start, "proc_start");
  sc_trace(Tf, t.uut[debugSystem]->sync_start, "sync_start");
  std::vector<std::string> perm_out_names(t.ws[debugSystem]);
  for (uint64_t i = 0; i < t.ws[debugSystem]; i++) {
    perm_out_names[i] = std::string("perm_out_") + std::to_string(i);
    sc_trace(Tf, t.uut[debugSystem]->perm_out[i], perm_out_names[i].c_str());
  }
  std::vector<std::string> proc_out_names(t.ws[debugSystem]);
  for (uint64_t i = 0; i < t.ws[debugSystem]; i++) {
    proc_out_names[i] = std::string("proc_out_") + std::to_string(i);
    sc_trace(Tf, t.uut[debugSystem]->proc_out[i], proc_out_names[i].c_str());
  }
  
  sc_start();
  return 0;
}

#pragma once
#include "Permutations.hpp"
#include "Buffer.hpp"
#include "systemc.h"
#include <string>
#include <list>
#include <tuple>

using namespace sc_core;

using token_perm_t = std::tuple<
  std::vector<uint64_t>,     //index
  std::vector<permutation_t> //permutations
  >;

/* tokenise_perms will remove the repeated permutations in perms
   and output the striped array along with an index that associates
   the entries in perms with the entries in the outputted array */
token_perm_t tokenise_perms(std::vector<permutation_t> perms);

/* Permuter implements the architecture described in 
   
   Peter A. Milder, James C. Hoe and Markus Püschel (Proc. Design, Automation and Test in Europe (DATE), pp. 1118-1123, 2009)
   Automatic Generation of Streaming Datapaths for Arbitrary Fixed Permutations
   
   with a few modifications.
   
   A permutation of an array of size n is decomposed into 
   n/w permutations of size w. Herein, the n-size array is loaded
   into w buffers of size n/w sequentially. On the following n/w
   iterations, the values will be loaded from these buffers
   (using m_read_addresses for the addresses), permuted using the
   small permutations of size w (m_interconnect),
   and written to another bank of w buffers (using m_write_address).
   Finally, the values are read sequentially from the w buffers */

struct Permuter : sc_module
{  
  uint64_t m_n, m_w, m_bsize;
  permutation_t m_p;
  
  std::vector<std::vector<std::vector<uint64_t>>> m_read_addresses;
  std::vector<std::vector<std::vector<uint64_t>>> m_write_addresses;
  std::vector<std::vector<std::vector<uint64_t>>> m_interconnect;
  
  std::vector<std::vector<uint64_t>> curr_read_addr;
  std::vector<std::vector<uint64_t>> curr_write_addr;
  std::vector<std::vector<uint64_t>> curr_interconnect;
  
  std::vector<Buffer*> in_buckets;
  std::vector<Buffer*> out_buckets;
  std::vector<std::string> in_buckets_labels, out_buckets_labels;
  sc_in<bool> start, clk;
  /* Unlike in Puschel's paper we assume that x_in[i] will load the value in the position i * (n/w) + j on cycle j*/
  std::vector<sc_in<uint64_t>> x_in;
  /* We follow a similar rationale for the output as for the input */
  std::vector<sc_out<uint64_t>> y_out;
  sc_in<uint64_t> permutation;

  std::vector<sc_signal<uint64_t>> y_out1, y_reg;
  std::vector<sc_signal<bool>> y_zero;

  sc_signal<bool> write_in, write_out;
  std::vector<sc_signal<uint64_t>> addr_in, addr_out;
  std::vector<sc_signal<uint64_t>> bucket_xs, bucket_ys;
  sc_signal<uint64_t> counter_i, counter_j;

  std::string label;

  std::tuple<
    std::vector<std::vector<uint64_t>>, //read_addr
    std::vector<std::vector<uint64_t>>, //write_addr
    std::vector<std::vector<uint64_t>>  //interconnect
    >
  schedule(permutation_t p);
  
  void control();
  void process();

  void named_in_buckets();
  void named_out_buckets();
  
  Permuter(sc_module_name name,
	   uint64_t n,
	   uint64_t w,
	   std::vector<permutation_t> perms);

  virtual ~Permuter();

  SC_HAS_PROCESS(Permuter);
};

#pragma once
#include "systemc.h"
#include <vector>
#include <cstdint>
#include <string>

struct FIFO : public sc_module
{
  /* tap selects at which stage we want the output
     to come from. With tap = 0, the output is equal to the input;
     with other taps, the output will be equal to the input after tap
     cycles */
  sc_in<uint64_t>  tap_in;
  sc_in<uint64_t>  x_in;
  sc_out<uint64_t> y_out;
  sc_in<bool>      clk, start;

  std::vector<sc_signal<uint64_t>> regs;
  sc_signal<uint64_t> tap;

  std::string label;
  
  void control();
  void process();

  FIFO(sc_module_name name, uint64_t depth);
  
  SC_HAS_PROCESS(FIFO);
};

/* Several FIFOs (one for each x_in) are configured according to
   m_schedule. They have an amount of registers equal to the maximum
   delay that will be necessary to impose on the inputs. At each cycle,
   the tap of FIFO i is set to m_schedule[cycle][i] */
struct Delayer : public sc_module
{
  std::vector<std::vector<uint64_t>> m_schedule;
  std::vector<uint64_t>              m_curr_delays;
  uint64_t                           m_w;
  
  std::vector<sc_in<uint64_t>>  x_in;
  std::vector<sc_out<uint64_t>> y_out;
  sc_in<uint64_t>               cycle;
  sc_in<bool>                   clk, start;

  std::vector<std::string>         fifos_name;
  std::vector<FIFO *>              fifos;
  std::vector<sc_signal<uint64_t>> fifos_tap;
  sc_signal<bool>                  fifos_start;

  std::string label;

  void control();

  Delayer(sc_module_name name,
	  std::vector<std::vector<uint64_t>> schedule);
  virtual ~Delayer();

  SC_HAS_PROCESS(Delayer);
};

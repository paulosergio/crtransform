#include "systemc.h"
#include "Permuter.hpp"
#include <random>

using namespace sc_core;

struct PermuterTestbench : sc_module
{
  const uint64_t b             = 6;
  const uint64_t w             = 4;
  const uint64_t wb            = w*b;
  const uint64_t numtests      = 100;
  const uint64_t q             = 2323;

  std::default_random_engine gen;
  std::uniform_int_distribution<uint64_t> dist;
  std::vector<permutation_t> perms;

  Permuter uut;
  std::vector<sc_signal<uint64_t>> uut_x_in;
  std::vector<sc_signal<uint64_t>> uut_y_out;
  sc_signal<uint64_t> uut_permutation;
  sc_signal<bool> uut_start;
  sc_in<bool> clk;

  std::vector<uint64_t> apply_perm(std::vector<uint64_t> x,
				   permutation_t p)
  {
    std::vector<uint64_t> y(x.size());

    for (uint64_t i = 0; i < x.size(); i++) {
      y[p[i]] = x[i];
    }

    return y;
  }

  std::vector<permutation_t> random_perms()
  {
    std::vector<permutation_t> perms(numtests,
				     permutation_t(wb));
    for (uint64_t i = 0; i < numtests; i++) {
      for (uint64_t j = 0; j < wb; j++)
	perms[i][j] = j;
      std::shuffle(std::begin(perms[i]),
		   std::end(perms[i]),
		   gen);
    }
    return perms;
  }

  void process()
  {
    wait();

    for (uint64_t i = 0; i < numtests; i++) {
      uut_start.write(true);
      uut_permutation.write(i);

      std::vector<uint64_t> x(wb);
      std::generate(std::begin(x),
		    std::end(x),
		    [this] () {
		      return this->dist(this->gen);
		    });

      for (uint64_t j = 0; j < b; j++) {
	for (uint64_t h = 0; h < w; h++) {
	  uut_x_in[h].write(x[h*b + j]);
	  // std::cout << x[h*b + j] << " ";
	}
	// std::cout << "\n";
	wait();
	uut_start.write(false);
      }

      for (uint64_t j = 0; j < b; j++)
	wait();

      std::vector<uint64_t> y(wb);
      for (uint64_t j = 0; j < b; j++) {
	wait(5.0, SC_NS);
	for (uint64_t h = 0; h < w; h++) {
	  y[h*b + j] = uut_y_out[h].read();
	  // std::cout << y[h*b + j] << " ";
	}
	// std::cout << "\n";
	wait();
      }
      
      auto yexp = apply_perm(x, perms[i]);
      assert(std::equal(std::begin(y),
			std::end(y),
			std::begin(yexp)));
    }

    std::cout << "SUCCESS!\n";

    sc_stop();
  }

  PermuterTestbench(sc_module_name name)
    : sc_module(name)
    , dist(0, q-1)
    , perms(random_perms())
    , uut("uut", wb, w, perms)
    , uut_x_in(w)
    , uut_y_out(w)
  {
    for (uint64_t h = 0; h < w; h++) {
      uut.x_in[h](uut_x_in[h]);
      uut.y_out[h](uut_y_out[h]);
    }
    uut.permutation(uut_permutation);
    uut.start(uut_start);
    uut.clk(clk);

    SC_THREAD(process);
    sensitive << clk.pos();
  }

  SC_HAS_PROCESS(PermuterTestbench);
};

int sc_main(int argc, char *argv[])
{
  sc_clock TestClk("TestClock", 10, SC_NS, 0.5);
  PermuterTestbench t("PermuterTestbench");
  t.clk(TestClk);
  sc_start();

  return 0;
}

#include "systemc.h"
#include "Delayer.hpp"
#include <random>

using namespace sc_core;

struct DelayerTestbench : sc_module
{
  const uint64_t b         = 2;
  const uint64_t w         = 4;
  const uint64_t bw        = b*w;
  const uint64_t numtests  = 100;
  const uint64_t q         = 2323;
  const uint64_t max_delay = 15;
  
  std::default_random_engine gen;
  std::uniform_int_distribution<uint64_t> dist;
  std::uniform_int_distribution<uint64_t> dist_delay;
  
  std::vector<std::vector<uint64_t>> schedule;
  Delayer uut;
  std::vector<sc_signal<uint64_t>>  uut_x_in;
  std::vector<sc_signal<uint64_t>>  uut_y_out;
  sc_signal<uint64_t>               uut_cycle;
  sc_signal<bool>                   uut_start;

  sc_in<bool> clk;

  std::vector<std::vector<uint64_t>> random_schedule()
  {
    std::vector<std::vector<uint64_t>> s
      (numtests,
       std::vector<uint64_t>(w));

    for (auto &row : s) {
      for (auto &cell : row) {
	cell = dist_delay(gen);
      }
    }
    
    return s;
  }

  void control()
  {
    wait();

    std::vector<uint64_t> x(bw), y(bw);

    for (uint64_t i = 0; i < numtests; i++) {
      std::generate(std::begin(x),
		    std::end(x),
		    [this] () {
		      return this->dist(this->gen);
		    });
      
      uut_start.write(true);
      uut_cycle.write(i);

      uint64_t max_delay_i = *std::max_element(std::begin(schedule[i]),
					       std::end(schedule[i]));

      for (uint64_t j = 0; j < max_delay_i; j++) {
	for (uint64_t h = 0; h < w; h++) {
	  if (j >= max_delay_i - schedule[i][h] &&
	      j < max_delay_i - schedule[i][h] + b) {
	    uut_x_in[h].write(x[h*b + j - (max_delay_i - schedule[i][h])]);
	  }
	}

	wait();
	uut_start.write(false);
      }

      for (uint64_t j = max_delay_i;
	   j < max_delay_i + b; j++) {
	for (uint64_t h = 0; h < w; h++) {
	  if (j >= max_delay_i - schedule[i][h] &&
	      j < max_delay_i - schedule[i][h] + b) {
	    uut_x_in[h].write(x[h*b + j - (max_delay_i - schedule[i][h])]);
	  }
	}
	wait(5.0, SC_NS);
	for (uint64_t h = 0; h < w; h++) {
	  y[h*b + j - max_delay_i] = uut_y_out[h].read();
	}
	wait();
      }

      assert(std::equal(std::begin(x),
			std::end(x),
			std::begin(y)));
    }

    std::cout << "SUCCESS!\n";

    sc_stop();
  }

  DelayerTestbench(sc_module_name name)
    : sc_module(name)
    , dist(0, q-1)
    , dist_delay(0, max_delay-1)
    , schedule(random_schedule())
    , uut("uut", schedule)
    , uut_x_in(w)
    , uut_y_out(w)
  {
    for (uint64_t i = 0; i < w; i++) {
      uut.x_in[i](uut_x_in[i]);
      uut.y_out[i](uut_y_out[i]);
    }
    uut.cycle(uut_cycle);
    uut.clk(clk);
    uut.start(uut_start);

    SC_THREAD(control);
    sensitive << clk.pos();
  }

  SC_HAS_PROCESS(DelayerTestbench);
};

int sc_main(int argc, char *argv[])
{
  sc_clock TestClk("TestClock", 10, SC_NS, 0.5);
  DelayerTestbench t("DelayerTestbench");
  t.clk(TestClk);
  sc_start();

  return 0;
}

#include "PE.hpp"

ButterflyOp::ButterflyOp(ButterflyOpType type,
			 uint64_t m, uint64_t h,
			 uint64_t q, uint64_t _w0,
			 std::vector<uint64_t> factors)
  : m_type(type),
    m_m(m), m_h(h),
    m_q(q),
    w0(_w0),
    m_factors(factors)
{
  assert(m_type != BO_TWIDDLE ||
	 (factors.size() == m));
  assert(m_type == BO_TWIDDLE ||
	 (NTL::ProbPrime(m)));
}

bool ButterflyOp::operator==(const ButterflyOp &other) const
{
  if (m_type != other.m_type ||
      m_m != other.m_m ||
      m_h != other.m_h)
    return false;

  if (m_type == BO_TWIDDLE &&
      (!std::equal(std::begin(m_factors),
		   std::end(m_factors),
		   std::begin(other.m_factors))))
    return false;

  return true;
}

bool ButterflyOp::operator!=(const ButterflyOp &other) const
{
  return !(*this==(other));
}

uint64_t ButterflyOp::get_size() const
{
  if (m_type == BO_DFT || m_type == BO_DFT_INV ||
      m_type == BO_TWIDDLE)
    return m_m;
  return m_m-1; // m_type == BO_CRT || m_type == BO_CRT_INV
}

uint64_t ButterflyOp::get_h() const
{
  return m_h;
}

std::vector<uint64_t> ButterflyOp::get_ws() const
{
  if (m_type == BO_TWIDDLE) return m_factors;
    
  std::vector<std::pair<uint64_t, uint64_t>> m_m_factors{
    std::make_pair(m_m, (uint64_t)1) };
  // std::cout << "m_m = " << m_m << "\n";
  uint64_t minv = modinv(m_m, m_q);

  if (m_type == BO_DFT) {
    std::vector<uint64_t> ws(m_m * m_m);
    uint64_t w = w0;

    for (uint64_t i = 0; i < m_m; i++) {
      for (uint64_t j = 0; j < m_m; j++) {
	uint64_t ij = i*m_m + j;
	ws[ij] = pown(w, i*j, m_q);
      }
    }

    return ws;
  }

  if (m_type == BO_DFT_INV) {
    std::vector<uint64_t> ws(m_m * m_m);
    uint64_t w = modinv(w0, m_q);

    for (uint64_t i = 0; i < m_m; i++) {
      for (uint64_t j = 0; j < m_m; j++) {
	uint64_t ij = i*m_m + j;
	ws[ij] = ((uint128_t)pown(w, i*j, m_q) * minv) % m_q;
      }
    }

    return ws;
  }

  if (m_type == BO_CRT) {
    uint64_t phi_m = m_m - 1;
    std::vector<uint64_t> ws(phi_m * phi_m);
    uint64_t w = w0;

    for (uint64_t i = 1; i < m_m; i++) {
      for (uint64_t j = 0; j < phi_m; j++) {
	ws[(i-1)*phi_m + j] = pown(w, i*j, m_q);
      }
    }

    return ws;
  }

  if (m_type == BO_CRT_INV) {
    //CRT_INV = p^{-1) (1_{p-1} 1_{p-1}^t + I_{p-1}) X
    //X is the upper right (p-1)*(p-1) submatrix of DFT*
    uint64_t phi_m = m_m - 1;
    std::vector<uint64_t> ws_tmp(phi_m * phi_m);
    std::vector<uint64_t> ws(phi_m * phi_m);
    uint64_t w = modinv(w0, m_q);

    for (uint64_t i = 0; i < phi_m; i++) {
      for (uint64_t j = 1; j < m_m; j++) {
	ws_tmp[i*phi_m + j-1] = ((uint128_t)pown(w, i*j, m_q) * minv) % m_q;
      }
    }

    for (uint64_t i = 0; i < phi_m; i++) {
      for (uint64_t j = 0; j < phi_m; j++) {
	ws[i*phi_m + j] = 0;
	for (uint64_t k = 0; k < phi_m; k++) {
	  uint64_t a = (k == i ? 2 : 1);
	  ws[i*phi_m + j] = ((uint128_t)ws_tmp[k*phi_m + j]*a + ws[i*phi_m + j]) % m_q;
	}
      }
    }

    return ws;
  }

  return std::vector<uint64_t>{};
}

token_t tokenise(std::vector<ButterflyOp> ops)
{
  std::vector<uint64_t> index;
  std::vector<std::vector<uint64_t>> wss;
  std::vector<uint64_t> ms;
  std::vector<uint64_t> hs;
  std::vector<bool> linears;

  for (uint64_t i = 0; i < ops.size(); i++) {
    bool repeated = false;
    for (uint64_t j = 0; j < i; j++) {
      if (ops[i] == ops[j]) {
	repeated = true;
	index.push_back(index[j]);
	break;
      }
    }

    if (!repeated) {
      index.push_back(wss.size());
      wss.push_back(ops[i].get_ws());
      ms.push_back(ops[i].get_size());
      hs.push_back(ops[i].get_h());
      linears.push_back(ops[i].m_type == BO_TWIDDLE);
    }
  }

  return std::make_tuple(index, wss, ms, hs, linears);
}

void Butterfly::control()
{
  while (true) {
    wait();

    // 0 <= j < n
    // 0 <= i < h
    // 0 <= k <= n+1
    // k = 0 : writing x
    // 0 < k <= n : computing y_k = sum_j x_j w^{jk}
    // k = n+1 : writing y
    if (start.read() == true) {
      uint64_t i = operation.read();
      m_curr_m = m_ms[i];
      m_curr_h = m_hs[i];
      m_curr_ws = m_wss[i];
      m_curr_linear = m_linears[i];

      if (m_curr_m != 1) {
	counter_k.write(0);
	counter_i.write(0);
	counter_j.write(1);
      } else if (m_curr_m == 1 && m_curr_h != 1) {
	counter_k.write(0);
	counter_i.write(1);
	counter_j.write(0);
      } else {
	counter_k.write(1);
	counter_i.write(0);
	counter_j.write(0);
      }

    } else {
      if (counter_j.read() == m_curr_m-1) {
	counter_j.write(0);
	if (counter_i.read() == m_curr_h - 1) {
	  counter_i.write(0);
	  counter_k.write(counter_k.read()+1);
	} else {
	  counter_i.write(counter_i.read()+1);
	}
      } else {
	counter_j.write(counter_j.read()+1);
      }
    }

    yreg.write(ytmp);
  }
}

void Butterfly::process()
{    
  write_x.write(counter_k.read() == 0 ||
		start.read());
  addr_x.write((start.read() ? 0 :
		counter_i.read() * m_curr_m + counter_j.read()));

  uint64_t k = counter_k.read() - 1;
  uint64_t i = counter_i.read();
  uint64_t j = counter_j.read();
  uint64_t kj = (k*m_curr_m + j < m_curr_ws.size() ? k*m_curr_m + j : 0);

  uint64_t yk;
  if (j == 0 || m_curr_linear) {
    yk = ((uint128_t)out_x.read() * m_curr_ws[kj]) % m_q;
  } else {
    yk = (((uint128_t)out_x.read() * m_curr_ws[kj]) + yreg.read()) % m_q;
  }
  ytmp.write(yk);
    
  in_y.write(yk);

  if (!m_curr_linear) {
    write_y.write((counter_k.read() > 0) &&
		  (counter_k.read() <= m_curr_m) &&
		  (counter_j.read() == m_curr_m-1));
    addr_y.write(counter_k.read() <= m_curr_m ?
		 i * m_curr_m + k :
		 i * m_curr_m + j);
    // if ((counter_k.read() > 0) &&
    // 	(counter_k.read() <= m_curr_m) &&
    // 	(counter_j.read() == m_curr_m-1)) {
    //   std::cout << label << " - " << sc_time_stamp() << ": ";
    //   std::cout << "writing " << yk <<
    // 	" to " <<  i * m_curr_m + k << "\n";
    // }
  } else {
    write_y.write(counter_k.read() == 1);
    // std::cout << label << " - " << sc_time_stamp() << ": ";
    // std::cout << "writing " << yk <<
    // 	" to " << i * m_curr_m + j << "\n";
    addr_y.write(i * m_curr_m + j);
  }

  if (m_curr_m == 1 && m_curr_h == 1) {
    // std::cout << label << " - " << sc_time_stamp() << ": ";
    // std::cout << "reading " << yreg.read() <<
    // 	" from " << yreg.read() << "\n";
    y_out.write(yreg.read());
  } else {
    // if (counter_k.read() == m_curr_m+1) {
    //   std::cout << label << " - " << sc_time_stamp() << ": ";
    //   std::cout << "reading " << out_y.read() <<
    //  	" from " << i * m_curr_m + j << "\n";
    // }
    y_out.write(out_y.read());
  }
}

uint64_t Butterfly::max_prod
(std::vector<uint64_t> a,
 std::vector<uint64_t> b)
{
  uint64_t res = 0;

  for (uint64_t i = 0; i < a.size(); i++) {
    uint64_t prod = a[i] * b[i];

    if (prod > res)
      res = prod;
  }

  return res;
}

Butterfly::Butterfly(sc_module_name name,
		     uint64_t q,
		     std::vector<std::vector<uint64_t>> wss,
		     std::vector<uint64_t> ms,
		     std::vector<uint64_t> hs,
		     std::vector<bool> linears)
  : sc_module(name),
    m_q(q),
    m_max_mh(max_prod(ms, hs)),
    m_wss(wss),
    m_ms(ms),
    m_hs(hs),
    m_linears(linears),
    x_n("x_n", m_max_mh),
    y_n("y_n", m_max_mh)
{
  label = name;

  SC_THREAD(control);
  sensitive << clk.pos();
  SC_METHOD(process);
  sensitive << out_x
	    << out_y
	    << counter_i
	    << counter_j
	    << counter_k
	    << yreg
	    << start;

  x_n.clk(clk);
  x_n.write(write_x);
  x_n.addr(addr_x);
  x_n.data_in(x_in);
  x_n.data_out(out_x);

  y_n.clk(clk);
  y_n.write(write_y);
  y_n.addr(addr_y);
  y_n.data_in(in_y);
  y_n.data_out(out_y);

  m_curr_ws = m_wss[0];
  m_curr_m = m_ms[0];
  m_curr_h = m_hs[0];
}

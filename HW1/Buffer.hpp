#pragma once
#include "systemc.h"
#include <cstdint>
#include <vector>
#include <string>

using namespace sc_core;

struct Buffer : public sc_module
{
  sc_in<bool> write, clk;
  sc_in<uint64_t> addr, data_in;
  sc_out<uint64_t> data_out;
  std::vector<uint64_t> mem;
  std::string label;
  sc_signal<bool> mem_changed;

  void write_process();
  void read_process();

  Buffer(sc_module_name name, uint64_t n);

  SC_HAS_PROCESS(Buffer);
};

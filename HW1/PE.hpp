#pragma once
#include "systemc.h"
#include <vector>
#include <cstdint>
#include <string>
#include <algorithm>
#include <numeric>
#include <unordered_map>
#include "Buffer.hpp"
#include <NTL/ZZ.h>
#include "NumTh.hpp"

using namespace sc_core;

enum ButterflyOpType {
  BO_DFT,
  BO_CRT,
  BO_DFT_INV,
  BO_CRT_INV,
  BO_TWIDDLE
};

struct ButterflyOp
{
  ButterflyOpType m_type;
  uint64_t m_m, m_h, m_q;
  uint64_t w0;

  std::vector<uint64_t> m_factors;

  ButterflyOp(ButterflyOpType type,
	      uint64_t m, uint64_t h,
	      uint64_t q, uint64_t w0,
	      std::vector<uint64_t> factors = {});
  
  bool operator==(const ButterflyOp &other) const;
  bool operator!=(const ButterflyOp &other) const;

  uint64_t get_size() const;
  uint64_t get_h() const;
  std::vector<uint64_t> get_ws() const;
};

using token_t = std::tuple<
  std::vector<uint64_t>, //index
  std::vector<std::vector<uint64_t>>, //ws
  std::vector<uint64_t>, //m
  std::vector<uint64_t>, //h
  std::vector<bool> //linear
  >;

/* Eliminates duplicate entries in a list of ButterflyOps;
   and returns the data required to construct a Butterfly implementing
   those operations in std::get<1..4>. The values in
   std::get<0>(output) are useful when there are duplicate entries:
   there are as many entries there as there are in ops, and the ith
   entry in std::get<0>(output) points to the parameters in
   std::get<1..4> that implement the ith ButterflyOp in ops */
token_t tokenise(std::vector<ButterflyOp> ops);

struct Butterfly : public sc_module
{
  uint64_t m_q;
  uint64_t m_max_mh;
  std::vector<std::vector<uint64_t>> m_wss;
  std::vector<uint64_t> m_ms, m_hs;
  std::vector<bool> m_linears;

  /* The processor will partition the input x into h sub-arrays
     of size m, and will operate on each sub-array individually */
  uint64_t m_curr_m, m_curr_h;
  std::vector<uint64_t> m_curr_ws;
  bool m_curr_linear;

  Buffer x_n, y_n;  
  
  sc_signal<uint64_t> counter_i, counter_j, counter_k;

  /* operation determines which operation will be executed by
     the Butterfly. The ith operation corresponds to the one encoded
     by wss[i], ms[i], hs[i], linears[i]. x will be read during the
     first h*m cycles (including the cycle where start is set).
     If the operation is linear it will take a further h*m cycles to
     execute or h*m^2 if it is quadratic. Finally the result is
     outputted in the following h*m cycles */
  sc_in<bool> start, clk;
  sc_in<uint64_t> operation;
  sc_in<uint64_t> x_in;
  sc_out<uint64_t> y_out;

  sc_signal<bool> write_x, write_y;
  sc_signal<uint64_t> in_y, out_y;
  sc_signal<uint64_t> out_x;
  sc_signal<uint64_t> addr_y, addr_x;
  sc_signal<uint64_t> ytmp, yreg;

  std::string label;

  void control();
  void process();

  uint64_t max_prod
  (std::vector<uint64_t> a,
   std::vector<uint64_t> b);

  Butterfly(sc_module_name name,
	    uint64_t q,
	    std::vector<std::vector<uint64_t>> wss,
	    std::vector<uint64_t> ms,
	    std::vector<uint64_t> hs,
	    std::vector<bool> linears);

  SC_HAS_PROCESS(Butterfly);
};

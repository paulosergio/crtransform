#include "Permuter.hpp"

token_perm_t tokenise_perms(std::vector<permutation_t> perms)
{
  std::vector<uint64_t> index;
  std::vector<permutation_t> res;

  for (uint64_t i = 0; i < perms.size(); i++) {
    bool repeated = false;

    for (uint64_t j = 0; j < i; j++) {
      if (std::equal(std::begin(perms[i]),
		     std::end(perms[i]),
		     std::begin(perms[j]))) {
	repeated = true;
	index.push_back(index[j]);
	break;
      }
    }

    if (!repeated) {
      index.push_back(res.size());
      res.push_back(perms[i]);
    }
  }

  std::cout << "##### TOKENISE_PERMS ######\n";
  print_perm(index); std::cout << "\n";
  for (auto p : res) {
    print_perm(p); std::cout << "\n";
  }

  return std::make_tuple(index, res);
}

std::tuple<
  std::vector<std::vector<uint64_t>>, //read_addr
  std::vector<std::vector<uint64_t>>, //write_addr
  std::vector<std::vector<uint64_t>>  //interconnect
  >
Permuter::schedule(permutation_t p)
{
  std::vector<std::vector<uint64_t>> read_addresses(m_bsize, std::vector<uint64_t>(m_w));
  std::vector<std::vector<uint64_t>> write_addresses(m_bsize, std::vector<uint64_t>(m_w));
  std::vector<std::vector<uint64_t>> interconnect(m_bsize, std::vector<uint64_t>(m_w));
  /* c[k][j] counts how many values will need to be written
     to out_buckets[k] coming from in_buckets[j] */
  matrix_t c(m_w, std::vector<uint64_t>(m_w, 0));

  // std::cout << "##### schedule #####\n";
  // print_perm(p); std::cout << std::endl;
  for (uint64_t k = 0; k < m_w; k++) {
    for (uint64_t l = 0; l < m_w; l++) {
	
      for (uint64_t i = l*m_bsize; i < (l+1)*m_bsize; i++) {
	if (p[i] >= k*m_bsize &&
	    p[i] < (k+1)*m_bsize) {
	  c[k][l]++;
	}
      }

      // std::cout << c[k][l];
    }
    // std::cout << std::endl;
  }

  /* c is decomposed into a sum of permutation matrices.
     Permutations are represented as an array of values such that
     P = (e_(p[0]), e_(p[1]), ..., e_(p[w-1]))
     where e_i is the ith canonical basis vector */
  auto ps = decompose_semimagic(c);

  // for (const auto &kp : ps) {
  //   uint64_t k;
  //   permutation_t p;
  //   std::tie(k, p) = kp;
  //   std::cout << "k = " << k << std::endl;
  //   std::cout << "p = ";
  //   print_perm(p);
  //   std::cout << std::endl;
  // }   

  std::list<std::pair<uint64_t, uint64_t>> pl;
  for (uint64_t i = 0; i < m_n; i++)
    pl.push_back(std::make_pair(i, p[i]));

  uint64_t t = 0; //clock cycle
  /* For each of the above permutations of size w we associate values
     from the larger permutation of size n */
  for (const auto &kp : ps) {
    uint64_t k;
    permutation_t p;
    std::tie(k, p) = kp;

    for (uint64_t ki = 0; ki < k; ki++) {
      for (uint64_t l = 0; l < m_w; l++) {
	auto it = std::find_if(std::begin(pl),
			       std::end(pl),
			       [&] (std::pair<uint64_t, uint64_t> ij) {
				 return ((ij.first/m_bsize) == l) &&
				 (p[l] == (ij.second/m_bsize));
			       });
	read_addresses [t][l]    = it->first  % m_bsize;
	// std::cout << "read_address[" << t << "][" << l << "] = " <<
	//   read_addresses[t][l] << "\n";
	write_addresses[t][p[l]] = it->second % m_bsize;
	// std::cout << "write_addresses[" << t << "][" << p[l] << "] = " <<
	//   write_addresses[t][p[l]] << "\n";
	interconnect   [t][l]    = p[l];
	// std::cout << "interconnect[" << t << "][" << l << "] = " <<
	//   interconnect[t][l] << "\n";
	pl.erase(it);
      }
      t++;
    }
  }

  return std::make_tuple(read_addresses,
			 write_addresses,
			 interconnect);
}

void Permuter::control()
{
  while (true) {
    wait();
    if (start.read() == true) {
      counter_i.write(0);
      counter_j.write(1);

      uint64_t p        = permutation.read();
      // std::cout << sc_time_stamp() << ": " << p << "th permutation\n";
      curr_read_addr    = m_read_addresses [p];
      curr_write_addr   = m_write_addresses[p];
      curr_interconnect = m_interconnect   [p];
    } else {
      if (counter_j.read() == m_bsize - 1) {
	counter_j.write(0);
	counter_i.write(counter_i.read()+1);
      } else {
	counter_j.write(counter_j.read()+1);
      }
    }

    for (uint64_t i = 0; i < m_w; i++) {
      y_reg[i].write(bucket_ys[i].read());
      y_zero[i].write(addr_out[i].read() == 0);
    }
  }
}

void Permuter::process()
{
  uint64_t i = (start.read() ? 0 : counter_i.read());
  uint64_t j = (start.read() ? 0 : counter_j.read());
  write_in.write (i == 0);
  write_out.write(i == 1);

  if (i == 0) { // receiving inputs
    for (uint64_t k = 0; k < m_w; k++) {
      addr_in[k].write(j);

      // std::cout << sc_time_stamp() << ": writing into (" << k <<
      // ") " << j << "\n";
    }
  } else if (i == 1){ //permuting data
    for (uint64_t k = 0; k < m_w; k++) {
      addr_in[k]. write(curr_read_addr [j][k]);
      // std::cout << sc_time_stamp() << ": reading from (" << k <<
      //   ") " << curr_read_addr [j][k] << "\n";
      addr_out[k].write(curr_write_addr[j][k]);
      // std::cout << sc_time_stamp() << ": writing into (" << k <<
      //   ") " << curr_write_addr[j][k] << "\n";
      uint64_t target_bucket = curr_interconnect[j][k];
      bucket_ys[target_bucket].write(bucket_xs[k].read());
      // std::cout << sc_time_stamp() << ": connecting " << k <<
      //   " to " << target_bucket << "\n";
    }
  } else { //sending outputs
    for (uint64_t k = 0; k < m_w; k++) {
      addr_out[k].write(j);
      // std::cout << sc_time_stamp() << ": reading from (" << k <<
      //   ") " << j << "\n";
    }
  }

  for (uint64_t k = 0; k < m_w; k++) {
    if (j == 0 && y_zero[k].read())
      y_out[k].write(y_reg[k]. read());
    else
      y_out[k].write(y_out1[k].read());
  }
}

void Permuter::named_in_buckets()
{
  for (uint64_t i = 0; i < m_w; i++) {
    in_buckets_labels.push_back(std::string("in_bucket") + std::to_string(i));
    in_buckets.push_back(new Buffer(in_buckets_labels[i].c_str(), m_n/m_w));
  }
}
  
void Permuter::named_out_buckets()
{
  for (uint64_t i = 0; i < m_w; i++) {
    out_buckets_labels.push_back(std::string("out_bucket") + std::to_string(i));
    out_buckets.push_back(new Buffer(out_buckets_labels[i].c_str(), m_n/m_w));
  }
}

Permuter::Permuter(sc_module_name name,
		   uint64_t n,
		   uint64_t w,
		   std::vector<permutation_t> perms)
  : sc_module(name)
  , m_n(n)
  , m_w(w)
  , m_bsize(n/w)
  , x_in(m_w)
  , y_out(m_w)
  , y_out1(m_w)
  , y_reg(m_w)
  , y_zero(m_w)
  , addr_in(m_w)
  , addr_out(m_w)
  , bucket_xs(m_w)
  , bucket_ys(m_w)
{
  named_in_buckets();
  named_out_buckets();
  label = name;
  
  // std::cout << label << ":\n";
  for (const auto &p : perms) {
    // print_perm(p);
    // std::cout << "\n";
    auto addresses = schedule(p);
    m_read_addresses. push_back(std::get<0>(addresses));
    m_write_addresses.push_back(std::get<1>(addresses));
    m_interconnect.   push_back(std::get<2>(addresses));
  }
  
  SC_THREAD(control);
  sensitive << clk.pos();
  SC_METHOD(process);
  sensitive << start
	    << counter_i
	    << counter_j;
  for (uint64_t i = 0; i < m_w; i++) {
    sensitive << bucket_xs[i];
    sensitive << y_reg[i];
    sensitive << y_out1[i];
    sensitive << y_zero[i];
    sensitive << permutation;
  }
  
  for (uint64_t i = 0; i < m_w; i++) {
    in_buckets[i]->write(write_in);
    in_buckets[i]->clk(clk);
    in_buckets[i]->addr(addr_in[i]);
    in_buckets[i]->data_in(x_in[i]);
    in_buckets[i]->data_out(bucket_xs[i]);
    
    out_buckets[i]->write(write_out);
    out_buckets[i]->clk(clk);
    out_buckets[i]->addr(addr_out[i]);
    out_buckets[i]->data_in(bucket_ys[i]);
    out_buckets[i]->data_out(y_out1[i]);
  }
}

Permuter::~Permuter()
{
  for (auto it : in_buckets)
    delete it;
  for (auto it : out_buckets)
    delete it;
}

#include "Buffer.hpp"

void Buffer::write_process()
{
  while (true) {
    wait();
      
    if (write.read() && addr.read() < mem.size()) {
      uint64_t i = addr.read();
      mem[i] = data_in.read();
      mem_changed.write(!mem_changed.read());

      // std::cout << sc_time_stamp() << " - " << label << ": ";
      // std::cout << "writing " << mem[i] << " to " << i << "\n";
    }
  }
}

void Buffer::read_process()
{
  uint64_t i = addr.read();

  if (i < mem.size())
    data_out.write(mem[i]);
  else
    data_out.write(0);
}

Buffer::Buffer(sc_module_name name, uint64_t n)
  : sc_module(name), mem(n)
{
  label = name;
  SC_THREAD(write_process);
  sensitive << clk.pos();
  SC_METHOD(read_process);
  sensitive << addr << mem_changed;
}

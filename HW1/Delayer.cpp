#include "Delayer.hpp"

void FIFO::control()
{
  while (true) {
    wait();

    if (regs.size() > 0) {
      for (uint64_t i = 1;
	   i < regs.size();
	   i++) {
	regs[i].write(regs[i-1].read());
      }

      regs[0].write(x_in.read());
    }
  }
}

void FIFO::process()
{
  if (start.read()) {
    tap.write(tap_in.read());
    assert(tap_in.read() <= regs.size());
  }

  if (regs.size() > 0 && tap.read() > 0) {
    y_out.write(regs[tap.read()-1].read());
  } else {
    y_out.write(x_in.read());
  }
}

FIFO::FIFO(sc_module_name name, uint64_t depth)
  : sc_module(name), regs(depth)
{
  label = name;
  SC_THREAD(control);
  sensitive << clk.pos();
  SC_METHOD(process);
  sensitive << start;
  for (auto &r : regs)
    sensitive << r;
  sensitive << x_in;
  sensitive << tap;
}

void Delayer::control()
{
  if (start.read()) {
    std::cout << label << ": " << sc_time_stamp() << "\n";
    uint64_t c    = cycle.read();
    m_curr_delays = m_schedule[c];

    for (uint64_t j = 0; j < m_w; j++) {
      std::cout << j << "th delay is " << m_curr_delays[j] << "\n";
      fifos_tap[j].write(m_curr_delays[j]);
    }
    fifos_start.write(true);
  }
}

Delayer::Delayer(sc_module_name name,
		 std::vector<std::vector<uint64_t>> schedule)
  : sc_module(name)
  , m_schedule(schedule)
  , m_w(schedule[0].size())
  , x_in(m_w)
  , y_out(m_w)
  , fifos_name(m_w)
  , fifos(m_w)
  , fifos_tap(m_w)
{
  label = name;
  for (uint64_t j = 0; j < schedule.size(); j++)
    assert(schedule[j].size() == m_w);
    
  for (uint64_t i = 0; i < m_w; i++) {
    fifos_name[i] = std::string("fifo") + std::to_string(i);
      
    uint64_t max_delay_i = 0;
    for (uint64_t j = 0; j < schedule.size(); j++) {
      max_delay_i = std::max(max_delay_i,
			     schedule[j][i]);
    }
    std::cout << i << "th max delay is " << max_delay_i << "\n";

    fifos[i] = new FIFO(fifos_name[i].c_str(),
			max_delay_i);
    fifos[i]->tap_in(fifos_tap[i]);
    fifos[i]->x_in(x_in[i]);
    fifos[i]->y_out(y_out[i]);
    fifos[i]->clk(clk);
    fifos[i]->start(fifos_start);
  }
    
  SC_METHOD(control);
  sensitive << start;
}

Delayer::~Delayer()
{
    for (auto f : fifos)
      delete f;
}

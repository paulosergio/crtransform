#include "PE.hpp"
#include <NTL/ZZ.h>
#include <random>

using namespace sc_core;

struct ButterflyTestbench : public sc_module
{
  const uint64_t numtests      = 100;
  const uint64_t q             = 31;

  std::default_random_engine              gen;
  std::uniform_int_distribution<uint64_t> dist;
  
  std::vector<ButterflyOp> ops;
  token_t                  tokens;

  Butterfly uut;
  sc_signal<bool> uut_start;
  sc_signal<uint64_t> uut_operation, uut_x_in, uut_y_out;
  sc_in<bool> clk;

  std::vector<ButterflyOp> random_ops()
  {
    std::vector<ButterflyOp> ops;
    std::uniform_int_distribution<uint64_t> distT(BO_DFT, BO_TWIDDLE);
    std::uniform_int_distribution<uint64_t> distH(0, 3);
    std::uniform_int_distribution<uint64_t> distM(0, 5);

    for (uint64_t i = 0; i < numtests; i++) {
      auto t = static_cast<ButterflyOpType>(distT(gen));
      uint64_t m = NTL::NextPrime(distM(gen));
      uint64_t h = distH(gen);
      std::vector<uint64_t> factors(m);
      uint64_t w = findElemOfOrder(m, q, factor(m));

      if (t == BO_TWIDDLE) {
	for (uint64_t i = 0; i < m; i++) {
	  factors[i] = dist(gen);
	}
      }

      ops.emplace_back(t, m, h, q, w, factors);
    }

    return ops;
  }

  void process()
  {
    wait();
    for (uint64_t i = 0; i < numtests; i++) {
      uint64_t array_size = ops[i].get_size() *
	ops[i].get_h();
      std::vector<uint64_t> x(array_size);
      std::generate(std::begin(x),
		    std::end(x),
		    [this] () {
		      return this->dist(this->gen);
		    });

      uut_start.write(true);
      uut_operation.write(std::get<0>(tokens)[i]);

      for (uint64_t j = 0; j < x.size(); j++) {
	uut_x_in.write(x[j]);
	wait();
	uut_start.write(false);
      }

      if (ops[i].m_type == BO_TWIDDLE) {
	for (uint64_t j = 0; j < ops[i].get_size() * ops[i].get_h(); j++)
	  wait();
      } else {
	for (uint64_t j = 0; j < ops[i].get_size() * ops[i].get_size() * ops[i].get_h(); j++)
	  wait();
      }

      std::vector<uint64_t> y(array_size);
      for (uint64_t j = 0; j < y.size(); j++) {
	wait(5.0, SC_NS);
	y[j] = uut_y_out.read();
	wait();
      }
      
      std::vector<uint64_t> yexp(array_size);
      for (uint64_t j = 0; j < ops[i].get_h(); j++) {
	std::vector<uint64_t> xslice(std::begin(x) + j * ops[i].get_size(),
				     std::begin(x) + (j+1) * ops[i].get_size());
	std::vector<uint64_t> yslice(xslice.size(), 0);
	auto ws = ops[i].get_ws();
	
	if (ops[i].m_type == BO_TWIDDLE) {
	  for (uint64_t h = 0; h < xslice.size(); h++)
	    yslice[h] = ((uint128_t)xslice[h] * ws[h]) % q;
	} else {
	  for (uint64_t h1 = 0; h1 < xslice.size(); h1++) {
	    for (uint64_t h2 = 0; h2 < xslice.size(); h2++) {	      
	      yslice[h1] = ((uint128_t)xslice[h2] * ws[h1 * xslice.size() + h2] + yslice[h1]) % q;
	    }
	  }
	}
	std::copy(std::begin(yslice),
		  std::end(yslice),
		  std::begin(yexp) + j * ops[i].get_size());
      }

      assert(std::equal(std::begin(yexp),
			std::end(yexp),
			std::begin(y)));
    }

    std::cout << "SUCCESS\n";
    sc_stop();
  }

  ButterflyTestbench(sc_module_name name)
    : sc_module(name)
    , dist(0, q-1)
    , ops(random_ops())
    , tokens(tokenise(ops))
    , uut("uut", q,
	  std::get<1>(tokens),
	  std::get<2>(tokens),
	  std::get<3>(tokens),
	  std::get<4>(tokens))
  {
    uut.start(uut_start);
    uut.clk(clk);
    uut.operation(uut_operation);
    uut.x_in(uut_x_in);
    uut.y_out(uut_y_out);
    SC_THREAD(process);
    sensitive << clk.pos();
  }

  SC_HAS_PROCESS(ButterflyTestbench);
};

int sc_main(int argc, char *argv[])
{
  sc_clock TestClk("TestClock", 10, SC_NS, 0.5);
  ButterflyTestbench t("ButterflyTestbench");
  t.clk(TestClk);
  sc_start();

  return 0;
}

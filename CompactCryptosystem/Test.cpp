#include "CompactCryptosystem.hpp"
#include "DSLSoftware.hpp"
#include "NumTh.hpp"
#include <random>

#define TEST(M,Q)					\
  {							\
    uint64_t m = M;					\
    typename ZZq::value_type q = Q;			\
    typename ZZq::value_type p = 2;			\
    auto *transforms =					\
      new DSLSoftware<ZZq,ZZ>(m, q);			\
    CompactCryptosystem<ZZq,ZZ>				\
      system(m, q, p, transforms);			\
							\
    uint64_t errors = 0;				\
    for (uint64_t i = 0; i < numtests; i++) {		\
      CompactCryptosystem<ZZq,ZZ>::plaintext_t		\
	t(system.m_n);					\
      for (auto &ti : t) ti = dist(gen);		\
							\
      auto c = system.enc(t);				\
      auto t1 = system.dec(c);				\
							\
      for (uint64_t i = 0; i < system.m_n; i++) {	\
	if (t[i] != t1[i]) {				\
	  errors++;					\
	}						\
      }							\
    }							\
							\
    std::cout << "m = " << m << "\n";			\
    std::cout << "q = " << q << "\n";			\
    std::cout << "errors = " << errors << "/"		\
	      << numtests * system.m_n << "\n";		\
							\
  } 


int main(int argc, char *argv[])
{
  using ZZq = ZZq16_t;
  using ZZ = ZZ16_t;
  uint64_t numtests = 1024;
  std::default_random_engine gen;
  std::uniform_int_distribution<typename ZZ::value_type> dist(0,1);

  TEST(256,3329);
  TEST(576,3457);
  TEST(512,7681);
  TEST(800,4001);
  TEST(896,4481);
  TEST(928,5569);
  TEST(1024,12289);
  return 0;
}

#include <vector>
#include <tuple>
#include <cstdint>
#include <random>
#include "DSLInterface.hpp"
#include <memory>
#include <algorithm>
#undef str
#include <iostream>
#include <cmath>
#include "NumTh.hpp"

template<typename ZZq, typename ZZ>
struct CompactCryptosystem
{
  uint64_t m_m, m_q, m_p, m_n;
  std::vector<typename ZZq::value_type> m_a, m_b;
  std::vector<typename ZZ::value_type> m_x;
  std::vector<typename ZZq::value_type> m_crtX;
  std::unique_ptr<DSLInterface<ZZq,ZZ>> m_transforms;
  std::vector<typename ZZq::value_type> m_crtG;
  std::default_random_engine m_gen;
  double m_stddev;
  double m_s;
  std::normal_distribution<double> m_gaussianDist;
  std::uniform_int_distribution<typename ZZq::value_type> m_uniformDist;

  using ciphertext_t = std::pair<
    std::vector<typename ZZq::value_type>,
    std::vector<typename ZZq::value_type>>;

  using plaintext_t = std::vector<typename ZZ::value_type>;

  CompactCryptosystem(uint64_t m,
		      typename ZZq::value_type q,
		      typename ZZq::value_type p,
		      DSLInterface<ZZq,ZZ> *transforms);
  
  ~CompactCryptosystem();

  void gen();
  ciphertext_t enc(const plaintext_t &t);
  plaintext_t dec(const ciphertext_t &c);

private:
  std::vector<typename ZZ::value_type> sampleDiscretisedGaussian
  (const std::vector<typename ZZ::value_type> &coset,
   typename ZZq::value_type scalingFactor = 1);
  
  std::vector<typename ZZ::value_type>
  decodingToPowerful
  (const std::vector<typename ZZ::value_type> &x);
  
  std::vector<typename ZZq::value_type>
  decodingToCrt
  (const std::vector<typename ZZ::value_type> &x);
  
  std::vector<typename ZZ::value_type>
  crtToDecoding
  (const std::vector<typename ZZq::value_type> &x);
  
  std::vector<typename ZZq::value_type>
  powerfulToCrt
  (const std::vector<typename ZZq::value_type> &x);
  
  std::vector<typename ZZq::value_type> crtToPowerful(const std::vector<typename ZZq::value_type> &x);
  
  std::vector<typename ZZ::value_type>
  powerfulToDecoding
  (const std::vector<typename ZZ::value_type> &x);
  
  std::vector<typename ZZq::value_type>
  moduloMult
  (const std::vector<typename ZZq::value_type> &x,
   const std::vector<typename ZZq::value_type> &y);
  
  std::vector<typename ZZq::value_type>
  moduloAdd
  (const std::vector<typename ZZq::value_type> &x,
   const std::vector<typename ZZq::value_type> &y);
  
  std::vector<typename ZZq::value_type>
  moduloSub
  (const std::vector<typename ZZq::value_type> &x,
   const std::vector<typename ZZq::value_type> &y);
  
  std::vector<typename ZZq::value_type>
  mod
  (const std::vector<typename ZZq::value_type> &x);
  
  std::vector<typename ZZq::value_type>
  mod
  (const std::vector<typename ZZ::value_type> &x);
  
  std::vector<typename ZZ::value_type>
  toInt
  (const std::vector<typename ZZq::value_type> &x);
  
  std::vector<typename ZZq::value_type>
  computeCrtG ();
};

template<typename ZZq, typename ZZ>
CompactCryptosystem<ZZq,ZZ>::CompactCryptosystem
(uint64_t m,
 typename ZZq::value_type q,
 typename ZZq::value_type p,
 DSLInterface<ZZq,ZZ> *transforms)
  : m_m(m),
    m_q(q),
    m_p(p),
    m_n(totient(m)),
    m_a(m_n),
    m_b(m_n),
    m_x(m_n),
    m_crtX(m_n),
    m_transforms(transforms),
    m_crtG(computeCrtG()),
    m_stddev(sqrt(log(m_n)/q)),
    m_s(m_stddev * sqrt(m_m / rad(m_m))),
    // m_s(8.0),
    m_gaussianDist(0.0, m_s),
    m_uniformDist(0, m_q-1)
{
  // std::cout << "s = " << m_s << "\n";
  gen();
}

template<typename ZZq, typename ZZ>
void
CompactCryptosystem<ZZq,ZZ>::gen()
{
  std::vector<typename ZZ::value_type> zero(m_n, 0);
  // a <- U(R_q) (c)
  std::generate(m_a.begin(), m_a.end(),
		[this] () {
		  return this->m_uniformDist(this->m_gen);
		});
  // std::cout << "a = " << m_a << "\n";

  // x <- [psi]_R^v (d)
  m_x = sampleDiscretisedGaussian(zero);
  // std::cout << "m_x = " << m_x << "\n";
  // e <- [p . psi]_pR^v (d)
  auto e = sampleDiscretisedGaussian(zero, m_p);
  // std::cout << "e = " << e << "\n";

  m_crtX = decodingToCrt(m_x);
  // std::cout << "m_crtX = " << m_crtX << "\n";
  auto crtE = decodingToCrt(e);
  // std::cout << "crtE = " << crtE << "\n";

  // b = g(a*x + e)
  m_b = moduloMult(m_crtG,
		   moduloAdd(moduloMult(m_a, m_crtX),
			     crtE));
  // std::cout << "m_b = " << m_b << "\n";

}

template<typename ZZq, typename ZZ>
typename CompactCryptosystem<ZZq,ZZ>::ciphertext_t
CompactCryptosystem<ZZq,ZZ>::enc
(const typename CompactCryptosystem<ZZq,ZZ>::plaintext_t &t)
{
  std::vector<typename ZZ::value_type> zero(m_n, 0);

  // std::cout << "t = " << t << "\n";
  auto z     = sampleDiscretisedGaussian(zero);
  // std::cout << "z = " << z << "\n";
  auto e1    = sampleDiscretisedGaussian(zero, m_p);
  // std::cout << "e1 = " << e1 << "\n";
  auto coset = powerfulToDecoding(t);
  // std::cout << "coset = " << coset << "\n";
  auto e2    = sampleDiscretisedGaussian(coset, m_p);
  // std::cout << "e2 = " << e2 << "\n";
  auto crtZ  = decodingToCrt(z);
  // std::cout << "crtZ = " << crtZ << "\n";
  auto crtE1 = decodingToCrt(e1);
  // std::cout << "crtE1 = " << crtE1 << "\n";
  auto u = moduloMult(m_crtG,
		      moduloAdd(moduloMult(m_a, crtZ),
				crtE1));
  // std::cout << "u = " << u << "\n";
  auto crtE2 = decodingToCrt(e2);
  // std::cout << "crtE2 = " << crtE2 << "\n";
  
  auto v = moduloAdd(moduloMult(m_b, crtZ),
		     crtE2);
  // std::cout << "v = " << v << "\n";

  return std::make_pair(u, v);
}

template<typename ZZq, typename ZZ>
typename CompactCryptosystem<ZZq,ZZ>::plaintext_t
CompactCryptosystem<ZZq,ZZ>::dec
(const typename CompactCryptosystem<ZZq,ZZ>::ciphertext_t &c)
{
  std::vector<typename ZZq::value_type> u(m_n), v(m_n);
  std::tie(u, v) = c;
  auto m = moduloSub(v,
		     moduloMult(u, m_crtX));
  // std::cout << "m = " << m << "\n";
  
  auto decodedM = crtToDecoding(m);
  // std::cout << "decodedM = " << decodedM << "\n";
  std::transform(decodedM.begin(),
		 decodedM.end(),
		 decodedM.begin(),
		 [this] (typename ZZ::value_type x) ->
		 typename ZZ::value_type {
		   typename ZZ::value_type xmodq =
		     x % static_cast<int64_t>(this->m_q);
		   if (xmodq >
		       static_cast<typename ZZ::value_type>(this->m_q) / 2) {
		     return xmodq -
		       static_cast<typename ZZ::value_type>(m_q);
		     
		   } else if (xmodq <=
			      -static_cast<typename ZZ::value_type>(this->m_q) / 2) {
		     return xmodq + this->m_q;
		   } else {
		     return xmodq;
		   }
		 });
  // std::cout << "decodedM = " << decodedM << "\n";

  decodedM = decodingToPowerful(decodedM);
  // std::cout << "decodedM = " << decodedM << "\n";

  std::transform(decodedM.begin(),
		 decodedM.end(),
		 decodedM.begin(),
		 [this] (typename ZZ::value_type x) ->
		 typename ZZ::value_type {
		   typename ZZ::value_type xmodp =
		     x % static_cast<typename ZZ::value_type>(this->m_p);
		   if (xmodp >
		       static_cast<typename ZZ::value_type>(this->m_p) / 2) {
		     return xmodp - m_p;
		   } else if (xmodp <=
			      -static_cast<typename ZZ::value_type>(this->m_p) / 2) {
		     return xmodp + this->m_p;
		   } else {
		     return xmodp;
		   }
		 });
  // std::cout << "decodedM = " << decodedM << "\n";


  return decodedM;
}

template<typename ZZq, typename ZZ>
std::vector<typename ZZ::value_type>
CompactCryptosystem<ZZq,ZZ>::sampleDiscretisedGaussian
(const std::vector<typename ZZ::value_type> &coset,
 typename ZZq::value_type scalingFactor)
{
  std::vector<double> sample(m_n);

  // sample continuous Gaussian in K_RR in the decoding basis
  std::generate(sample.begin(), sample.end(),
		[this] () {
		  return this->m_gaussianDist(this->m_gen);
		});
  // std::cout << "sample = " << sample << "\n";
  sample = m_transforms->eval_crt_b(sample);
  // std::cout << "CRT^(-1) B sample = " << sample << "\n";

  // scale if necessary
  if (scalingFactor > 1) {
    std::transform(sample.begin(), sample.end(),
		   sample.begin(),
		   [scalingFactor] (double x) {
		     return x * static_cast<double>(scalingFactor);
		   });
  }
  // std::cout << "scaled CRT^(-1) B sample = " << sample << "\n";

  // discretize to c + pR^dual, where c = cosetRepresentative and p = scalingFactor
  std::transform(sample.cbegin(), sample.cend(), coset.cbegin(),
		 sample.begin(),
		 [scalingFactor, this]
		 (double const& y, int const& c){
		   double intpart;

		   // cut off integer part
		   double p =
		     std::modf((c - y) / scalingFactor, &intpart);

		   // and make sure that p is in [0,1)
		   if (p < 0) {
		     p = p + 1.0;
		   }
		   
		   std::bernoulli_distribution distribution(1.0 - p);
		   if (distribution(this->m_gen)){
		     return round(y + scalingFactor * p);
		     // return this with probability  1 - p
		   } else {
		     return round(y + scalingFactor * (p - 1.0));
		     // otherwise return this
		   }
		 });

  // std::cout << "discretised CRT^(-1) B sample = " << sample << "\n";
  std::vector<typename ZZ::value_type> ret(sample.begin(), sample.end());

  return ret;
}

template<typename ZZq, typename ZZ>
std::vector<typename ZZ::value_type>
CompactCryptosystem<ZZq,ZZ>::decodingToPowerful
(const std::vector<typename ZZ::value_type> &x)
{
  return m_transforms->eval_l(x);
}

template<typename ZZq, typename ZZ>
std::vector<typename ZZq::value_type>
CompactCryptosystem<ZZq,ZZ>::decodingToCrt
(const std::vector<typename ZZ::value_type> &x)
{
  auto powerfulX = decodingToPowerful(x);
  return powerfulToCrt(mod(powerfulX));
}

template<typename ZZq, typename ZZ>
std::vector<typename ZZ::value_type>
CompactCryptosystem<ZZq,ZZ>::crtToDecoding
(const std::vector<typename ZZq::value_type> &x)
{
  auto powerfulX = crtToPowerful(x);
  return powerfulToDecoding(toInt(powerfulX));
}

template<typename ZZq, typename ZZ>
std::vector<typename ZZq::value_type>
CompactCryptosystem<ZZq,ZZ>::crtToPowerful
(const std::vector<typename ZZq::value_type> &x)
{
  // std::cout << "x.size() = " << x.size() << std::endl;
  return m_transforms->eval_icrt(x);
}

template<typename ZZq, typename ZZ>
std::vector<typename ZZq::value_type>
CompactCryptosystem<ZZq,ZZ>::powerfulToCrt
(const std::vector<typename ZZq::value_type> &x)
{
  return m_transforms->eval_crt(x);
}

template<typename ZZq, typename ZZ>
std::vector<typename ZZ::value_type>
CompactCryptosystem<ZZq,ZZ>::powerfulToDecoding
(const std::vector<typename ZZ::value_type> &x)
{
  return m_transforms->eval_il(x);
}

template<typename ZZq, typename ZZ>
std::vector<typename ZZq::value_type>
CompactCryptosystem<ZZq,ZZ>::moduloMult
(const std::vector<typename ZZq::value_type> &x,
 const std::vector<typename ZZq::value_type> &y)
{
  std::vector<typename ZZq::value_type> z(x.size());
  std::transform(x.begin(), x.end(),
		 y.begin(),
		 z.begin(),
		 [this] (typename ZZq::value_type xi,
			 typename ZZq::value_type yi) {
		   return (((typename ZZq::greater_value_type)xi)
			   * yi) % this->m_q;
		 });
  return z;
}

template<typename ZZq, typename ZZ>
std::vector<typename ZZq::value_type>
CompactCryptosystem<ZZq,ZZ>::moduloAdd
(const std::vector<typename ZZq::value_type> &x,
 const std::vector<typename ZZq::value_type> &y)
{
  std::vector<typename ZZq::value_type> z(x.size());
  std::transform(x.begin(), x.end(),
		 y.begin(),
		 z.begin(),
		 [this] (typename ZZq::value_type xi,
			 typename ZZq::value_type yi) {
		   return (((typename ZZq::greater_value_type)xi)
			   + yi) % this->m_q;
		 });
  return z;
}

template<typename ZZq, typename ZZ>
std::vector<typename ZZq::value_type>
CompactCryptosystem<ZZq,ZZ>::moduloSub
(const std::vector<typename ZZq::value_type> &x,
 const std::vector<typename ZZq::value_type> &y)
{
  std::vector<typename ZZq::value_type> z(x.size());
  std::transform(x.begin(), x.end(),
		 y.begin(),
		 z.begin(),
		 [this] (typename ZZq::value_type xi,
			 typename ZZq::value_type yi) {
		   return (((typename ZZq::greater_value_type)xi)
			   + m_q - yi) % this->m_q;
		 });
  return z;
}

template<typename ZZq, typename ZZ>
std::vector<typename ZZq::value_type>
CompactCryptosystem<ZZq,ZZ>::computeCrtG()
{
  std::vector<typename ZZq::value_type> g(m_n, 1), one(m_n, 1);
  // std::cout << "COMPUTING G\n";

  auto fs = factor(m_m);
  for (uint64_t i = 0; i < fs.size(); i++) {
    // std::cout << "f = " << fs[i].first << "^" << fs[i].second << "\n";
    std::vector<typename ZZq::value_type> gp(m_n, 0);
    if (fs[i].first != 2) {
      uint64_t entry = pown(fs[i].first, fs[i].second-1);
      for (uint64_t j = i+1; j < fs.size(); j++) {
	entry *= totient(pown(fs[j].first, fs[j].second));
      }
      gp[entry] = 1;

      gp = powerfulToCrt(gp);
      gp = moduloSub(one, gp);
      g = moduloMult(g, gp);
    }
  }

  // std::cout << "g = " << g << "\n";

  return g;
}

template<typename ZZq, typename ZZ>
CompactCryptosystem<ZZq,ZZ>::~CompactCryptosystem()
{
}


template<typename ZZq, typename ZZ>
std::vector<typename ZZ::value_type>
CompactCryptosystem<ZZq,ZZ>::toInt
(const std::vector<typename ZZq::value_type> &x)
{
  std::vector<typename ZZ::value_type> y(x.begin(), x.end());
  return y;
}

template<typename ZZq, typename ZZ>
std::vector<typename ZZq::value_type>
CompactCryptosystem<ZZq,ZZ>::mod
(const std::vector<typename ZZ::value_type> &xs)
{
  std::vector<typename ZZq::value_type> y(xs.size());
  std::transform(xs.cbegin(),
		 xs.cend(),
		 y.begin(),
		 [this] (typename ZZ::value_type a) ->
		 typename ZZq::value_type {
		   typename ZZ::value_type amodq = a %
		     static_cast<typename ZZ::value_type>(this->m_q);
		   if (amodq < 0) {
		     return static_cast<typename ZZq::value_type>
		       (amodq + static_cast<typename ZZ::value_type>(this->m_q));
		   } else {
		     return static_cast<typename ZZq::value_type>(amodq);
		   }
		 });
  return y;
}

template<typename ZZq, typename ZZ>
std::vector<typename ZZq::value_type>
CompactCryptosystem<ZZq,ZZ>::mod
(const std::vector<typename ZZq::value_type> &x)
{
  std::vector<typename ZZq::value_type> y(x.size());
  std::transform(x.cbegin(),
		 x.cend(),
		 y.begin(),
		 [this] (typename ZZq::value_type a) {
		   return a % this->m_q;
		 });
  return y;
}

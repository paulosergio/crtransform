#include "CRT2.hpp"
#include <iostream>
#include "Glue.hpp"
#include <memory>

int main(int argc, char* argv[])
{
  std::unique_ptr<Trans> crt1(get_crt(9));
  std::cout << *crt1 << "\n";
  Glue glue(4, crt1.get(), 9, 19);
  return 0;
}

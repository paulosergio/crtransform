#pragma once
#include "systemc.h"
#include "Glue1.hpp"
#include "Permuter.hpp"
#include "PE.hpp"
#include "Delayer.hpp"

struct System : public sc_module
{
  const Glue &m_params;
  std::string label;

  sc_in<bool> clk, start;
  std::vector<sc_in<uint64_t>>  x_in;
  std::vector<sc_out<uint64_t>> y_out;

  token_perm_t                     perm_tokens;
  Permuter                        *perm;
  std::vector<sc_signal<uint64_t>> perm_in;
  std::vector<sc_signal<uint64_t>> perm_out;
  sc_signal<bool>                  perm_start;
  sc_signal<uint64_t>              perm_index;

  std::vector<std::string>         proc_name;
  std::vector<token_t>             proc_tokens;
  std::vector<Butterfly*>          proc;
  std::vector<sc_signal<uint64_t>> proc_in;
  std::vector<sc_signal<uint64_t>> proc_out;
  std::vector<sc_signal<uint64_t>> proc_operation;
  sc_signal<bool>                  proc_start;

  Delayer *sync;
  std::vector<sc_signal<uint64_t>> sync_in;
  std::vector<sc_signal<uint64_t>> sync_out;
  sc_signal<uint64_t>              sync_cycle;
  sc_signal<bool>                  sync_start;

  std::vector<uint64_t> weights, min_ti;
  uint64_t total_exec_time;
  
  sc_signal<uint64_t> ctr;
  
  System(sc_module_name name,
	 const Glue &params);
  virtual ~System();

  void control();
  void process();

  SC_HAS_PROCESS(System);
};

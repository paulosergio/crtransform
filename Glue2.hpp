#pragma once
#include "DSL2.hpp"
#include <cstdint>
#include <vector>
#include <tuple>
#include "Permutations.hpp"
#include "BasicTrans.hpp"
#include <cassert>
#include <algorithm>
#include "PE.hpp"

/* As an intermediate representation of transforms, we convert
   the linked list representation to a vector of pairs. Each pair
   has a permutation  and a "singleton" transform that can be
   computed by the PEs. Execution proceeds by sequentially
   permuting data and processing it. */
using intermediate_repr_t =
  std::vector<std::pair<permutation_t,
			Trans*>>; //!= Perm

/* Computes the permutation that results from first permuting
   data according to a and afterwards according to perm */
permutation_t update_perm(permutation_t a,
			  Trans * perm);

/* Converts a transform described with Trans* to one described in
   intermediate_repr_t. To achieve the best results, ops should
   be in the parallelised form */
intermediate_repr_t convert_trans(Trans *ops);

/* Computes how much data each of the w PEs will process for each
   of the operations in repr */
std::vector<std::vector<uint64_t>>
data_processor_allocation(const intermediate_repr_t &repr,
			  uint64_t w);

/* Converts a number in binary representation to an "additive"
   representation. This representation has two digits (j, i)
   representing the value:
   weights[0] + ... + weights[j-1] + i
   Given the mapping of data computed by data_processor_allocation,
   this number representation allows us to determine by which PE
   each element of data will be processed */
std::pair<uint64_t, uint64_t> get_additive
(std::vector<uint64_t> weights,
 uint64_t i);
/* Reverses the above operation */
uint64_t from_additive
(std::vector<uint64_t> weights,
 std::pair<uint64_t, uint64_t> ji);

/* Since the permuter computes permutations for fixed data blocks,
   but the PEs process a variable amount of data, the data size is
   increased such that the fixed data block of the permuter can
   accomodate all PEs' blocks. While the permutations of the original
   data will operate on addresses described with values (i, j) for
   additive representations supported on the weights outputed by
   data_processor_allocation, liftPerm will lift these permutations
   to addresses (i, j) for a constant weight */
permutation_t liftPerm
(std::vector<uint64_t> prev_weights,
 permutation_t perm,
 std::vector<uint64_t> weights,
 uint64_t n1,
 uint64_t kmax);

/* Glue combines the parameters required to map a transform
   into the elements developed for the hardware accelerator */
struct Glue {
  uint64_t m_w; //number of processors
  Trans *m_ops; //transform to be applied
  uint64_t m_kmax, m_n1, m_n; //m_n1 ... extended data length for constant-block permutations
  uint64_t m_m, m_q;          //m_m ... transform index; m_q ... modulus
  std::vector<permutation_t>            m_lifted_perms;  //permutations for the extended data
  std::vector<std::vector<ButterflyOp>> m_op_scheduling; //operation scheduling for each processor
  std::vector<std::vector<uint64_t>>    m_exec_time;     //execution time for the operations
  std::vector<std::vector<uint64_t>>    m_data_allocation;
  
  Glue(uint64_t w, Trans *ops, uint64_t m, uint64_t q);
  virtual ~Glue();
};

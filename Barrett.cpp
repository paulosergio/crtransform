#define CATCH_CONFIG_MAIN
#include "catch.hpp"
#include "NumTh.hpp"
#include <cassert>
#include <random>
#include <NTL/ZZ.h>

struct Reduction {
  uint64_t m_qn, m_q;
  uint64_t m_numbits;

  Reduction(uint64_t q, uint64_t numbits)
    : m_q(q), m_numbits(numbits)
  {
    assert(numbits <= 62);
    assert(q < (static_cast<uint64_t>(1)<<numbits));

    m_qn = (static_cast<uint128_t>(1)<<(2*numbits+1)) / m_q;

    std::cout << "q = " << m_q << "\n";
    std::cout << "qn = " << m_qn << "\n";
  }

  uint128_t lower_bits(uint128_t z, uint64_t numbits)
  {
    return z & ((static_cast<uint128_t>(1)<<numbits)-1);
  }

  uint64_t operator()(uint128_t a)
  {
    uint128_t a1 = lower_bits(a >> (m_numbits-2), m_numbits+2);
    uint128_t t1 = lower_bits(m_qn * a1, 2*m_numbits + 4);
    uint64_t t = lower_bits(t1 >> (m_numbits+3), m_numbits+1);
    uint64_t z = lower_bits(lower_bits(a, m_numbits+1) -
			    lower_bits(m_q * t, m_numbits+1), m_numbits+1);
    if (z >= m_q)
      z -= m_q;
    return z;
  }
};

TEST_CASE("Barrett algorithm", "[reduction]")
{
  const uint64_t numbits = 30;
  const uint64_t m = 3855;
  const uint64_t numtests = 10000;
  uint64_t q = (static_cast<uint64_t>(1)<<(numbits-1)) +
    (m - ((static_cast<uint64_t>(1)<<(numbits-1)) % m)) +
    1;
  while (!NTL::ProbPrime(q))
    q += m;
  Reduction r(q, numbits);

  std::default_random_engine gen;
  std::uniform_int_distribution<uint64_t> dist(0, q-1);

  for (uint64_t i = 0; i < numtests; i++) {
    uint64_t a = dist(gen);
    uint64_t b = dist(gen);
    uint64_t c = dist(gen);

    uint128_t z1 = ((uint128_t)a*b + c);

    uint64_t zred = z1 % q;
    uint64_t zred1 = r(z1);

    REQUIRE(zred == zred1);
  }
}

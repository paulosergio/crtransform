#ifndef __SYSTEMTB_HPP__
#define __SYSTEMTB_HPP__
#include "hls_tb_gen_h.hpp"
#include "SystemFact.hpp"

template<typename ZZq, typename ZZ>
struct SystemTb {
  const SystemFact<ZZq> &m_sf;
  static const uint64_t numTrans = 5;
  uint64_t m_numTests;
  uint64_t m_dataMemSize;
  std::vector<std::vector<typename ZZq::value_type>> m_inputs;
  std::vector<std::vector<typename ZZq::value_type>> m_outputs;
  std::vector<call_parameters_t> m_call_parameters;

  SystemTb(const SystemFact<ZZq> &sf, uint64_t numTestsPerTrans);
  void generateHls(std::string fileName) const;
};

void system_tb_set_debug(bool debug);
bool system_tb_get_debug();

template<typename ZZq, typename ZZ>
SystemTb<ZZq,ZZ>::SystemTb
(const SystemFact<ZZq> &sf, uint64_t numTestsPerTrans)
  : m_sf(sf)
  , m_numTests(numTestsPerTrans * numTrans)
  , m_dataMemSize(sf.s.dataMemSize)
  , m_inputs(m_numTests, std::vector<typename ZZq::value_type>(totient(m_sf.s.m_m)))
  , m_outputs(m_numTests)
{
  std::default_random_engine gen;
  std::uniform_int_distribution<typename ZZq::value_type> dist(0, m_sf.s.m_q-1);
  std::uniform_int_distribution<typename ZZ::value_type> dist_i(-10, 10);
  std::normal_distribution<double> gaussianDist(0.0, 25);
  auto rand_vec = [&] (std::vector<typename ZZq::value_type> &xs) {
    std::generate(xs.begin(),
		  xs.end(),
		  [&] () {
		    return dist(gen);
		  });
  };
  auto rand_vec_i = [&] (std::vector<typename ZZq::value_type> &xs) {
    std::generate(xs.begin(),
		  xs.end(),
		  [&] () {
		    return dist_i(gen);
		  });
  };
  auto rand_vec_g = [&] (std::vector<double> &xs) {
    std::generate(xs.begin(),
		  xs.end(),
		  [&] () {
		    return gaussianDist(gen);
		  });
  };
  auto vec_i = [] (const std::vector<typename ZZq::value_type> &xs)
    -> std::vector<typename ZZ::value_type> {
    std::vector<typename ZZ::value_type> ys(xs.size());
    std::transform(xs.begin(),
		   xs.end(),
		   ys.begin(),
		   [] (typename ZZq::value_type x) {
		     return static_cast<typename ZZ::value_type>(x);
		   });
    return ys;
  };
  auto vec_ui = [] (const std::vector<typename ZZ::value_type> &xs)
    -> std::vector<typename ZZq::value_type> {
    std::vector<typename ZZq::value_type> ys(xs.size());
    std::transform(xs.begin(),
		   xs.end(),
		   ys.begin(),
		   [] (typename ZZ::value_type x) {
		     return static_cast<typename ZZq::value_type>(x);
		   });
    return ys;
  };
  auto vec_fp = [&] (const std::vector<double> &xs)
    -> std::vector<typename ZZq::value_type> {
    std::vector<typename ZZq::value_type> ys(xs.size());
    std::transform(xs.begin(),
		   xs.end(),
		   ys.begin(),
		   [this] (double x) {
		     return this->m_sf.s.fromDouble(x);
		   });
    return ys;
  };

  uint64_t j;
  std::unique_ptr<Trans> crt(get_crt(sf.s.m_m));
  std::unique_ptr<Trans> crt_par(parallelise(crt.get()));
  for (j = 0; j < numTestsPerTrans; j++) {
    rand_vec(m_inputs[j]);
    m_outputs[j] = eval_zzq<ZZq>(crt_par.get(), m_inputs[j],
				 m_sf.s.m_q,
				 m_sf.s.m_m);
    m_inputs[j].resize(m_dataMemSize);
    m_outputs[j].resize(m_dataMemSize);
    m_call_parameters.emplace_back(m_sf.get_crt_params());
  }
  std::unique_ptr<Trans> icrt(get_icrt(sf.s.m_m));
  std::unique_ptr<Trans> icrt_par(parallelise(icrt.get()));
  for (; j < 2*numTestsPerTrans; j++) {
    rand_vec(m_inputs[j]);
    m_outputs[j] = eval_zzq<ZZq>(icrt_par.get(), m_inputs[j],
				 m_sf.s.m_q,
				 m_sf.s.m_m);
    m_inputs[j].resize(m_dataMemSize);
    m_outputs[j].resize(m_dataMemSize);
    m_call_parameters.emplace_back(m_sf.get_icrt_params());
  }
  std::unique_ptr<Trans> l(get_l(sf.s.m_m));
  std::unique_ptr<Trans> l_par(parallelise(l.get()));  
  for (; j < 3*numTestsPerTrans; j++) {
    rand_vec_i(m_inputs[j]);
    m_outputs[j] = vec_ui(eval_zz<ZZ>(l_par.get(),
				      vec_i(m_inputs[j]),
				      m_sf.s.m_m));
    m_inputs[j].resize(m_dataMemSize);
    m_outputs[j].resize(m_dataMemSize);
    m_call_parameters.emplace_back(m_sf.get_l_params());
  }
  std::unique_ptr<Trans> il(get_il(sf.s.m_m));
  std::unique_ptr<Trans> il_par(parallelise(il.get()));
  for (; j < 4*numTestsPerTrans; j++) {
    rand_vec_i(m_inputs[j]);
    m_outputs[j] = vec_ui(eval_zz<ZZ>(il_par.get(),
				      vec_i(m_inputs[j]),
				      m_sf.s.m_m));
    m_inputs[j].resize(m_dataMemSize);
    m_outputs[j].resize(m_dataMemSize);
    m_call_parameters.emplace_back(m_sf.get_il_params());
  }
  std::unique_ptr<Trans> crt_b(get_crt_b(sf.s.m_m));
  std::unique_ptr<Trans> crt_b_par(parallelise(crt_b.get()));
  for (; j < 5*numTestsPerTrans; j++) {
    std::vector<double> input_j(totient(m_sf.s.m_m));
    rand_vec_g(input_j);
    auto output_j = eval_rr(crt_b_par.get(),
			    input_j,
			    m_sf.s.m_m);
    m_inputs[j]  = vec_fp(input_j);
    m_outputs[j] = vec_fp(output_j);
    m_inputs[j]. resize(m_dataMemSize);
    m_outputs[j].resize(m_dataMemSize);
    m_call_parameters.emplace_back(m_sf.get_crt_b_params());
  }
  assert(j == m_numTests);
}

template<typename ZZq, typename ZZ>
void SystemTb<ZZq,ZZ>::generateHls(std::string fileName) const
{
  std::ofstream f(fileName + ".h");
  hls_tb_h_gen_class gen_class;
  gen_class.set_numTests(get_string(m_numTests));
  gen_class.set_inputs(get_string(m_inputs));
  gen_class.set_outputs(get_string(m_outputs));
  gen_class.set_call_parameters(get_string(m_call_parameters));
  gen_class.set_maxError(get_string((double)5.0));
  gen_class.generate_hls_tb_h(f);
  f.close();
}


#endif

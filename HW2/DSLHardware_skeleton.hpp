#include "hls_test.h"
#include <stdint.h>
#include <stddef.h>
#include <stdbool.h>
#include <string.h>
#include <stdlib.h>
#include <vector>
#include <cmath>
#include <cassert>

#include "xtop_module.h"
#include "xaxidma.h"
#include "hls_test.h"
#include "DSLInterface.hpp"

struct call_parameters_t {
  size_t dataLength, blockSize;
  my_addr_t ptr;
  bool is_fixed_point;
};

static struct call_parameters_t entry_parameters[5] =
  @entry_parameters@;

template<typename ZZq, typename ZZ>
struct DSLHardware : DSLInterface<ZZq,ZZ>
{
	DSLHardware();

  virtual std::vector<typename ZZq::value_type>
  eval_crt (const std::vector<typename ZZq::value_type> &x);
  
  virtual std::vector<typename ZZq::value_type>
  eval_icrt (const std::vector<typename ZZq::value_type> &x);
  
  virtual std::vector<typename ZZ::value_type>
  eval_l (const std::vector<typename ZZ::value_type> &x);
  
  virtual std::vector<typename ZZ::value_type>
  eval_il (const std::vector<typename ZZ::value_type> &x);
  
  virtual std::vector<double>
  eval_crt_b (const std::vector<double> &x);

  virtual ~DSLHardware() {};
};

static XTop_module top_module;
static XTop_module_Config *top_module_config;
static XAxiDma axi_dma;
static XAxiDma_Config *axi_dma_config;

int init_peripherals()
{
  int status;

  top_module_config = XTop_module_LookupConfig(XPAR_TOP_MODULE_0_DEVICE_ID);
  if (!top_module_config) return -1;

  status = XTop_module_CfgInitialize(&top_module, top_module_config);
  if (status != XST_SUCCESS) return -1;

  axi_dma_config = XAxiDma_LookupConfig(XPAR_AXIDMA_0_DEVICE_ID);
  if (!axi_dma_config) return -1;

  status = XAxiDma_CfgInitialize(&axi_dma, axi_dma_config);
  if (status != XST_SUCCESS) return -1;

  if (XAxiDma_HasSg(&axi_dma)) {
    print("DMA has SG\n");
    return -1;
  }

  XAxiDma_IntrDisable(&axi_dma, XAXIDMA_IRQ_ALL_MASK, XAXIDMA_DEVICE_TO_DMA);
  XAxiDma_IntrDisable(&axi_dma, XAXIDMA_IRQ_ALL_MASK, XAXIDMA_DMA_TO_DEVICE);

  XAxiDma_Reset(&axi_dma);

  while (!XAxiDma_ResetIsDone(&axi_dma));

  return 0;
}

int
sign (data_t x)
{
  x &= (((greater_data_t)1) << numbits2) - 1;
  
  if (x >= (((data_t)1) << (numbits2-1))) {
    return -1;
  } else if (x == 0) {
    return 0;
  } else {
    return 1;
  }
}

greater_data_t
myabs (data_t x)
{
  x &= (((data_t)1) << numbits2) - 1;
  
  if (sign(x) < 0) {
    greater_data_t x1 =
      (((greater_data_t)1) << numbits2) - x;
    
    return x1;
  } else {
    return x;
  }
}

double to_double(data_t x)
{
  return sign(x) * (double) myabs(x) /
    (((greater_data_t)1) << fixedpoint);
}

void top_module_2(data_t *xs,
		  data_t *ys,
		  size_t blockSize,
		  my_addr_t ptr)
{
  XTop_module_Set_blockSize(&top_module, (u64)blockSize);
  XTop_module_Set_ptr(&top_module, (u64)ptr);

  XTop_module_Start(&top_module);

  Xil_DCacheFlushRange((INTPTR)xs, sizeof(data_t) * dataMemSize);
  Xil_DCacheFlushRange((INTPTR)ys, sizeof(data_t) * dataMemSize);

  XAxiDma_SimpleTransfer(&axi_dma, (UINTPTR)xs, sizeof(data_t) * dataMemSize, XAXIDMA_DMA_TO_DEVICE);
  while(XAxiDma_Busy(&axi_dma, XAXIDMA_DMA_TO_DEVICE));

  XAxiDma_SimpleTransfer(&axi_dma, (UINTPTR)ys, sizeof(data_t) * dataMemSize, XAXIDMA_DEVICE_TO_DMA);
  while(XAxiDma_Busy(&axi_dma, XAXIDMA_DEVICE_TO_DMA));

  Xil_DCacheInvalidateRange((INTPTR)ys, sizeof(data_t) * dataMemSize);

  while (!XTop_module_IsDone(&top_module));
}

template<typename ZZq, typename ZZ>
DSLHardware<ZZq,ZZ>::DSLHardware()
{
	init_peripherals();
}

template<typename ZZq, typename ZZ>
std::vector<typename ZZq::value_type>
DSLHardware<ZZq,ZZ>::eval_crt
(const std::vector<typename ZZq::value_type> &x)
{
  data_t xs[dataMemSize];
  data_t ys[dataMemSize];
  for (uint64_t i = 0; i < x.size(); i++) xs[i] = x[i];
  top_module_2(&xs[0],
	       &ys[0],
	       entry_parameters[0].blockSize,
	       entry_parameters[0].ptr);
  std::vector<data_t> y(&ys[0], &ys[x.size()]);
  return y;
}

template<typename ZZq, typename ZZ>
std::vector<typename ZZq::value_type>
DSLHardware<ZZq,ZZ>::eval_icrt
(const std::vector<typename ZZq::value_type> &x)
{
  data_t xs[dataMemSize];
  data_t ys[dataMemSize];
  for (uint64_t i = 0; i < x.size(); i++) xs[i] = x[i];
  top_module_2(&xs[0],
	       &ys[0],
	       entry_parameters[1].blockSize,
	       entry_parameters[1].ptr);
  std::vector<data_t> y(&ys[0], &ys[x.size()]);
  return y;
}

template<typename ZZq, typename ZZ>
std::vector<typename ZZ::value_type>
DSLHardware<ZZq,ZZ>::eval_l
(const std::vector<typename ZZ::value_type> &x)
{
  data_t xs[dataMemSize];
  data_t ys[dataMemSize];
  for (uint64_t i = 0; i < x.size(); i++)
    xs[i] = static_cast<data_t>(x[i]);
  top_module_2(&xs[0],
	       &ys[0],
	       entry_parameters[2].blockSize,
	       entry_parameters[2].ptr);
  std::vector<typename ZZ::value_type> y(x.size());
  for (uint64_t i = 0;  i < x.size(); i++)
    y[i] = static_cast<typename ZZ::value_type>(ys[i]);
  return y;  
}
  
template<typename ZZq, typename ZZ>
std::vector<typename ZZ::value_type>
DSLHardware<ZZq,ZZ>::eval_il
(const std::vector<typename ZZ::value_type> &x)
{
  data_t xs[dataMemSize];
  data_t ys[dataMemSize];
  for (uint64_t i = 0; i < x.size(); i++)
    xs[i] = static_cast<data_t>(x[i]);
  top_module_2(&xs[0],
	       &ys[0],
	       entry_parameters[3].blockSize,
	       entry_parameters[3].ptr);
  std::vector<typename ZZ::value_type> y(x.size());
  for (uint64_t i = 0;  i < x.size(); i++)
    y[i] = static_cast<typename ZZ::value_type>(ys[i]);
  return y;  
}

template<typename ZZq, typename ZZ>
std::vector<double>
DSLHardware<ZZq,ZZ>::eval_crt_b
(const std::vector<double> &x)
{
  data_t xs[dataMemSize];
  data_t ys[dataMemSize];
  for (uint64_t i = 0; i < x.size(); i++) {
    double xi = x[i] * std::pow(2, fixedpoint);

    if (xi >= 0) {
      assert(xi < std::pow(2, numbits2-1));
      xs[i] = static_cast<data_t>(std::round(xi));
    } else {
      assert(-xi <= std::pow(2, numbits2-1));
      xs[i] = (((greater_data_t)1) << numbits2) -
      static_cast<data_t>(std::round(-xi));
    }
  }

  top_module_2(&xs[0],
	       &ys[0],
	       entry_parameters[4].blockSize,
	       entry_parameters[4].ptr);

  std::vector<double> y(x.size());
  for (uint64_t i = 0;  i < x.size(); i++)
    y[i] = to_double(ys[i]);
  return y;  
}

#include <cstdint>

enum ring_t {
  RING_ZZ,
  RING_ZZq,
  RING_RR
};

enum op_t {
  OP_VECTOR,
  OP_MATRIX,
  OP_ID
};

struct operation_t {
  op_t m_type;

  union op_u {
    struct Id_s {
      uint64_t h, w;
    } m_id;

    struct Vec_s {
      uint64_t h, w;
      ring_t ring;
      std::vector<uint64_t> cnst;
    } m_vec;

    struct Mat_s {
      uint64_t h, w;
      ring_t ring;
      std::vector<std::vector<uint64_t>> cnst;
    } m_mat;
  } m_op;
};

operation_t make_id(uint64_t h, uint64_t w);

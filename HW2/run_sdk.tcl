sdk set_workspace crtransform_sdk
sdk create_hw_project -name test_hw -hwspec test.hdf
sdk create_bsp_project -name test_bsp -hwproject test_hw -proc ps7_cortexa9_0 -os standalone
sdk create_app_project -name test_app -hwproject test_hw -proc ps7_cortexa9_0 -os standalone -app {Empty Application} -lang c++ -bsp test_bsp
sdk import_sources -name test_app -path sdk_files
sdk create_app_project -name cryptosystem -hwproject test_hw -proc ps7_cortexa9_0 -os standalone -app {Empty Application} -lang c++ -bsp test_bsp
sdk import_sources -name cryptosystem -path sdk_cryptosystem


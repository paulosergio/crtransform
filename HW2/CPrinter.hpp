#ifndef __CPRINTER_HPP__
#define __CPRINTER_HPP__

#include <vector>

template<typename T>
std::string get_string(const T &x);

template<typename T>
struct GetString
{
  std::string operator()(const T &x)
  {
    std::stringstream ss;
    ss << x;
    return ss.str();
  }
};

template<>
struct GetString<uint64_t>
{
  std::string operator()(const uint64_t &x)
  {
    std::stringstream ss;
    ss << x << "ULL";
    return ss.str();
  }
};

template<>
struct GetString<double>
{
  std::string operator()(const double &x)
  {
    std::stringstream ss;
    ss << x;
    return ss.str();
  }
};

template<typename T>
struct GetString<std::vector<T>>
{
  std::string operator()(const std::vector<T> &xs)
  {
    std::stringstream ss;
    ss << "{";
    for (uint64_t i = 0; i+1 < xs.size(); i++)
      ss << get_string(xs[i]) << ", ";
    if (xs.size() > 0)
      ss << get_string(xs.back());
    ss << "}";
    return ss.str();
  }
};

template<typename T>
std::string get_string(const T &x)
{
  return GetString<T>{}(x);
}


#endif

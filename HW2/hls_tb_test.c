#include <stdint.h>
#include <stddef.h>
#include <stdbool.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#define TOP_MODULE_PROTOTYPE
#include "hls_tb_test.h"
#undef TOP_MODULE_PROTOTYPE

int main(int argc, char *argv[])
{
  uint64_t i, j;
  for (i = 0; i < numTests; i++) {
    data_t output_i[dataMemSize];
    struct io_data_t io_input_i[dataMemSize];
    struct io_data_t io_output_i[dataMemSize];
    
    for (j = 0; j < dataMemSize; j++) {
      io_input_i[j].data = inputs[i][j];
      io_input_i[j].last = (j+1) == dataMemSize;
    }
    
    top_module(&io_input_i[0],
	       &io_output_i[0],
	       call_parameters[i].blockSize,
	       call_parameters[i].ptr);

    for (j = 0; j < dataMemSize; j++) {
      output_i[j] = io_output_i[j].data;
    }
    
    if (!call_parameters[i].is_fixed_point) {
      if (memcmp(&output_i[0],
		 &outputs[i][0],
		 sizeof(data_t)*call_parameters[i].dataLength) != 0) {
	return 1;
      }
    } else {
      for (j = 0; j < call_parameters[i].dataLength; j++) {
	if (abs(to_double(output_i[j]) - to_double(outputs[i][j])) > maxError) {
	  return 1;
	}
      }
    }
  }
  return 0;
}

#define TOP_MODULE_PROTOTYPE
#include "hls_test.h"
#undef TOP_MODULE_PROTOTYPE

static data_t buffers1[numProcessors][dataMemSize];
static data_t buffers2[numProcessors][dataMemSize];

/* Permuter */
struct permutation_t {
  my_addr_t read_address[numProcessors], write_address[numProcessors];
  unsigned char interconnect[numProcessors];
};

static const struct permutation_t permMem[permMemSize] =
  @permMem@;

void permute(my_addr_t ptr, my_addr_t blockSize)
{
  my_addr_t i;
  size_t j, k;
  for (i = 0; i < blockSize; i++) {
	  data_t permBuffer1[numProcessors], permBuffer2[numProcessors];
	  my_addr_t perm[numProcessors];
#pragma HLS array_partition variable=permBuffer1 complete dim=0
#pragma HLS array_partition variable=permBuffer2 complete dim=0
#pragma HLS array_partition variable=perm complete dim=0

	  for (j = 0; j < numProcessors; j++) {
#pragma HLS unroll
		  permBuffer1[j] = buffers1[j][permMem[ptr+i].read_address[j]];
		  perm[j] = permMem[ptr+i].interconnect[j];
	  }

	  for (j = 0; j < numProcessors; j++) {
#pragma HLS unroll
#pragma HLS dependence variable=permBuffer2 inter false

		  permBuffer2[perm[j]] = permBuffer1[j];
	  }

    for (j = 0; j < numProcessors; j++) {
#pragma HLS unroll
#pragma HLS dependence variable=buffers2 inter false

      buffers2[j][permMem[ptr+i].write_address[j]] =
    		  permBuffer2[j];
    }
  }
}

/* Matrix vector multiplication */
enum ring_t {
  RING_ZZ,
  RING_ZZq,
  RING_RR
};

enum op_t {
  OP_VECTOR,
  OP_MATRIX,
  OP_ID
};

static const data_t cnstMem[numProcessors][cnstMemSize] =
  @cnstMem@;

greater_data_t
lower_bits (greater_data_t z, greater_data_t bits)
{
  return z & ((((greater_data_t)1)<<bits)-1);
}

data_t
barrettRed (greater_data_t a)
{
  greater_data_t a1 = lower_bits(a >> (numbits-2), numbits+2);
  greater_data_t t1 = lower_bits(qn * a1, 2*numbits + 4);
  greater_data_t t = lower_bits(t1 >> (numbits+3), numbits+1);
  greater_data_t z = lower_bits(lower_bits(a, numbits+1) -
				lower_bits(q * t, numbits+1),
				numbits+1);

  if (z >= q)
    z -= q;
  return z;
}

int
sign (data_t x)
{
  x &= (((greater_data_t)1) << numbits2) - 1;
  
  if (x >= (((data_t)1) << (numbits2-1))) {
    return -1;
  } else if (x == 0) {
    return 0;
  } else {
    return 1;
  }
}

greater_data_t
myabs (data_t x)
{
  x &= (((data_t)1) << numbits2) - 1;
  
  if (sign(x) < 0) {
    greater_data_t x1 =
      (((greater_data_t)1) << numbits2) - x;
    
    return x1;
  } else {
    return x;
  }
}

double to_double(data_t x)
{
  return sign(x) * (double) myabs(x) /
    (((greater_data_t)1) << fixedpoint);
}

data_t RRMult(data_t a, data_t b)
{
  int sign_res;
  greater_data_t c, a1, b1;
  data_t c1;

  sign_res = sign(a) * sign(b);
  a1 = myabs(a);
  b1 = myabs(b);

  c = a1 * b1;

  /* decimal part of c >= 0.5 */
  if (c & (((greater_data_t)1) << (fixedpoint-1)))
    c += (((greater_data_t)1) << (fixedpoint-1));

  if (sign_res == -1) {
    c1 = (((greater_data_t)1) << numbits2)
      - (c >> fixedpoint);
  } else if (sign_res == 0) {
    c1 = 0;
  } else {
    c1 = (c >> fixedpoint);
  }

  return c1;
}

void vmmult(data_t buffer1[dataMemSize],
		const data_t buffer2[dataMemSize],
		const data_t cnst[cnstMemSize],
	    unsigned char ring,
	    unsigned char op,
	    my_addr_t h,
	    my_addr_t w,
	    my_addr_t ptr)
{
  my_addr_t k, i, j;
  switch ((enum op_t)op) {
  case OP_VECTOR:
    for (k = 0; k < h; k++) {
      for (i = 0; i < w; i++) {
	switch ((enum ring_t)ring) {
	case RING_ZZ:
	  buffer1[k*w + i] = (signed_data_t)buffer2[k*w + i] * cnst[ptr + i];
	  break;
	case RING_ZZq:
	  buffer1[k*w + i] = barrettRed((greater_data_t)buffer2[k*w + i]*cnst[ptr + i]);
	  break;
	case RING_RR:
	  buffer1[k*w + i] = RRMult(buffer2[k*w + i], cnst[ptr + i]);
	  break;
	}
      }
    }
    break;
  case OP_MATRIX:
    for (k = 0; k < h; k++) {
      for (i = 0; i < w; i++) {
	data_t accum = 0;
	for (j = 0; j < w; j++) {
	  switch ((enum ring_t)ring) {
	  case RING_ZZ:
	    accum = (signed_data_t)buffer2[k*w + j] * cnst[ptr + i*w + j] + accum;
	    break;
	  case RING_ZZq:
	    accum = barrettRed((greater_data_t)buffer2[k*w + j]*cnst[ptr + i*w + j] + accum);
	    break;
	  case RING_RR:
	    accum = RRMult(buffer2[k*w + j], cnst[ptr + i*w + j]) + accum;
	    break;
	  }
	}
	buffer1[k*w + i] = accum;
      }
    }
    break;
  case OP_ID:
    for (k = 0; k < h; k++) {
      for (i = 0; i < w; i++) {
	buffer1[k*w + i] = buffer2[k*w + i];
      }
    }
    break;
  }
}

/* Control */
struct instruction_t {
  my_addr_t perm_ptr, perm_size;
  unsigned char vmmult_ring, vmmult_op;
  my_addr_t vmmult_h[numProcessors], vmmult_w[numProcessors], vmmult_ptr[numProcessors];
  bool end;
};

static const struct instruction_t instMem[instMemSize] =
  @instMem@;

void top_module(const struct io_data_t xs[dataMemSize],
		struct io_data_t ys[dataMemSize],
		size_t blockSize,
		my_addr_t ptr)
{
#pragma HLS INTERFACE axis port=ys
#pragma HLS INTERFACE axis port=xs
#pragma HLS INTERFACE s_axilite port=blockSize bundle=CTRL_BUS
#pragma HLS INTERFACE s_axilite port=ptr bundle=CTRL_BUS
#pragma HLS INTERFACE s_axilite port=return bundle=CTRL_BUS
#pragma HLS array_partition variable=buffers1 complete dim=1
#pragma HLS array_partition variable=buffers2 complete dim=1
#pragma HLS array_partition variable=cnstMem complete dim=1

  my_addr_t j = 0, i, k;
  for (i = 0, k = 0; i < dataMemSize; i++, k++) {
    if (k == blockSize) {
      j++;
      k = 0;
    }
    buffers1[j][k] = xs[i].data;
  }

  struct instruction_t inst;
#pragma HLS array_partition variable=inst.vmmult_h complete dim=0
#pragma HLS array_partition variable=inst.vmmult_w complete dim=0
#pragma HLS array_partition variable=inst.vmmult_ptr complete dim=0

  do {
#pragma HLS array_partition variable=instMem[ptr].vmmult_h complete dim=0
#pragma HLS array_partition variable=instMem[ptr].vmmult_w complete dim=0
#pragma HLS array_partition variable=instMem[ptr].vmmult_ptr complete dim=0
    inst = instMem[ptr];

    permute(inst.perm_ptr, blockSize);

    for (i = 0; i < numProcessors; i++) {
#pragma HLS unroll
#pragma HLS dependence variable=buffers2 inter false
#pragma HLS dependence variable=buffers1 inter false
#pragma HLS dependence variable=cnstMem inter false
#pragma HLS dependence variable=inst inter false
      vmmult(buffers1[i],
    		  buffers2[i],
			  cnstMem[i],
	     inst.vmmult_ring,
	     inst.vmmult_op,
	     inst.vmmult_h[i],
	     inst.vmmult_w[i],
	     inst.vmmult_ptr[i]);
    }

    ptr++;
  } while (!inst.end);

  j = 0;
  for (i = 0, k = 0; i < dataMemSize; i++, k++) {
    if (k == blockSize) {
      j++;
      k = 0;
    }
    ys[i].data = buffers1[j][k];
    ys[i].last = (i+1) == dataMemSize;
  }
}

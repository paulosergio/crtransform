#include "BasicTrans.hpp"
#include "NumTh.hpp"
#include <cassert>

std::ostream& operator<<(std::ostream &os, const BasicTrans &t)
{
  std::string s;

  switch(get_type(t)) {
  case BT_PERM:
    s = print_permutation(t);
    break;
  case BT_DFT:
    s = print_dft(t);
    break;
  case BT_TWIDDLE:
    s = print_twiddle(t);
    break;
  case BT_CRT:
    s = print_crt(t);
    break;
  case BT_TWIDDLE_CRT:
    s = print_twiddle_crt(t);
    break;
  case BT_DFT_INV:
    s = print_dft_inv(t);
    break;
  case BT_TWIDDLE_INV:
    s = print_twiddle_inv(t);
    break;
  case BT_CRT_INV:
    s = print_crt_inv(t);
    break;
  case BT_TWIDDLE_CRT_INV:
    s = print_twiddle_crt_inv(t);
    break;
  case BT_CRT_B:
    s = print_crt_b(t);
    break;
  case BT_L:
    s = print_l(t);
    break;
  case BT_L_INV:
    s = print_l_inv(t);
    break;
  }
  os << s;
  return os;
}

uint64_t get_dim(const BasicTrans &t)
{
  switch(get_type(t)) {
  case BT_PERM:
    return t.m_trans.m_perm.m;
  case BT_DFT:
    return t.m_trans.m_dft.m;
  case BT_TWIDDLE:
    return t.m_trans.m_twiddle.m;
  case BT_CRT:
    return totient(t.m_trans.m_crt.m);
  case BT_TWIDDLE_CRT:
    return totient(t.m_trans.m_twiddle_crt.m);
  case BT_DFT_INV:
    return t.m_trans.m_dft_inv.m;
  case BT_TWIDDLE_INV:
    return t.m_trans.m_twiddle_inv.m;
  case BT_CRT_INV:
    return totient(t.m_trans.m_crt_inv.m);
  case BT_TWIDDLE_CRT_INV:
    return totient(t.m_trans.m_twiddle_crt_inv.m);
  case BT_CRT_B:
    return totient(t.m_trans.m_crt_b.m);
  case BT_L:
    return totient(t.m_trans.m_l.m);
  case BT_L_INV:
    return totient(t.m_trans.m_l_inv.m);
  }

  return 0;
}

uint64_t get_m(const BasicTrans &t)
{
  switch(get_type(t)) {
  case BT_PERM:
    return t.m_trans.m_perm.m;
  case BT_DFT:
    return t.m_trans.m_dft.m;
  case BT_TWIDDLE:
    return t.m_trans.m_twiddle.m;
  case BT_CRT:
    return t.m_trans.m_crt.m;
  case BT_TWIDDLE_CRT:
    return t.m_trans.m_twiddle_crt.m;
  case BT_DFT_INV:
    return t.m_trans.m_dft_inv.m;
  case BT_TWIDDLE_INV:
    return t.m_trans.m_twiddle_inv.m;
  case BT_CRT_INV:
    return t.m_trans.m_crt_inv.m;
  case BT_TWIDDLE_CRT_INV:
    return t.m_trans.m_twiddle_crt_inv.m;
  case BT_CRT_B:
    return t.m_trans.m_crt_b.m;
  case BT_L:
    return t.m_trans.m_l.m;
  case BT_L_INV:
    return t.m_trans.m_l_inv.m;
  }

  return 0;
}


#define CONSTRUCTOR1(TYPE, MEMBER, NAME)	\
  BasicTrans CONCAT(make_,NAME)(uint64_t m)	\
  {						\
    BasicTrans t;				\
    t.m_type = TYPE;				\
    t.m_trans.MEMBER.m = m;			\
    return t;					\
  }

#define CONSTRUCTOR2(TYPE, MEMBER, NAME)	 \
  BasicTrans CONCAT(make_,NAME)(uint64_t m,	 \
				uint64_t n)	 \
  {						 \
    BasicTrans t;				 \
    t.m_type = TYPE;				 \
    t.m_trans.MEMBER.m = m;			 \
    t.m_trans.MEMBER.n = n;			 \
    return t;					 \
  }

CONSTRUCTOR2(BT_PERM, m_perm, permutation)
CONSTRUCTOR1(BT_DFT, m_dft, dft)
CONSTRUCTOR1(BT_TWIDDLE, m_twiddle, twiddle)
CONSTRUCTOR1(BT_CRT, m_crt, crt)
CONSTRUCTOR1(BT_TWIDDLE_CRT, m_twiddle_crt, twiddle_crt)
CONSTRUCTOR1(BT_DFT_INV, m_dft_inv, dft_inv)
CONSTRUCTOR1(BT_TWIDDLE_INV, m_twiddle_inv, twiddle_inv)
CONSTRUCTOR1(BT_CRT_INV, m_crt_inv, crt_inv)
CONSTRUCTOR1(BT_TWIDDLE_CRT_INV, m_twiddle_crt_inv, twiddle_crt_inv)
CONSTRUCTOR1(BT_CRT_B, m_crt_b, crt_b)
CONSTRUCTOR1(BT_L, m_l, l)
CONSTRUCTOR1(BT_L_INV, m_l_inv, l_inv)

#undef CONSTRUCTOR1
#undef CONSTRUCTOR2

#define PRINTER1(TYPE, MEMBER, NAME)			\
  std::string CONCAT(print_,NAME)(const BasicTrans &t)	\
  {							\
    std::stringstream ss;				\
    ss << STRINGIFY(TYPE) << " ";			\
    ss << t.m_trans.MEMBER.m;				\
    return ss.str();					\
  }

#define PRINTER2(TYPE, MEMBER, NAME)			\
  std::string CONCAT(print_,NAME)(const BasicTrans &t)	\
  {							\
    std::stringstream ss;				\
    ss << STRINGIFY(TYPE) << " ";			\
    ss << t.m_trans.MEMBER.m << " ";			\
    ss << t.m_trans.MEMBER.n;				\
    return ss.str();					\
  }

PRINTER2(BT_PERM, m_perm, permutation)
PRINTER1(BT_DFT, m_dft, dft)
PRINTER1(BT_TWIDDLE, m_twiddle, twiddle)
PRINTER1(BT_CRT, m_crt, crt)
PRINTER1(BT_TWIDDLE_CRT, m_twiddle_crt, twiddle_crt)
PRINTER1(BT_DFT_INV, m_dft_inv, dft_inv)
PRINTER1(BT_TWIDDLE_INV, m_twiddle_inv, twiddle_inv)
PRINTER1(BT_CRT_INV, m_crt_inv, crt_inv)
PRINTER1(BT_TWIDDLE_CRT_INV, m_twiddle_crt_inv, twiddle_crt_inv)
PRINTER1(BT_CRT_B, m_crt_b, crt_b)
PRINTER1(BT_L, m_l, l)
PRINTER1(BT_L_INV, m_l_inv, l_inv)

#undef PRINTER1
#undef PRINTER2

BasicTransType get_type(const BasicTrans &t)
{
  return t.m_type;
}

uint64_t get_permutation_n(const BasicTrans &t)
{
  assert(t.m_type == BT_PERM);

  return t.m_trans.m_perm.n;
}

void set_permutation_n(BasicTrans &t, uint64_t n)
{
  assert(t.m_type == BT_PERM);
  
  t.m_trans.m_perm.n = n;
}

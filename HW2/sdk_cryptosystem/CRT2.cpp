#include "DFT2.hpp"
#include "CRT2.hpp"
#include "NumTh.hpp"
#include <memory>

Trans *get_crt_base(uint64_t p, uint64_t k)
{
  using TransP = std::unique_ptr<Trans>;
  
  if (k > 1) {
    uint64_t m1    = pown(p, k-1);
    uint64_t m     = m1 * p;
    uint64_t phi_m = m - m1;
      
    TransP perm    (make_transf(1, 1, make_permutation(phi_m, m1)));
    TransP idp     (make_id(p-1));
    TransP dft_m1  (get_dft(m1));
    TransP tmp1    (Kronecker(idp.get(), dft_m1.get()));
    TransP tmp2    (Dot(perm.get(), tmp1.get()));
    TransP twiddle (make_transf(1, 1, make_twiddle_crt(m)));
    TransP tmp3    (Dot(tmp2.get(), twiddle.get()));
    TransP crt_p   (get_crt_base(p, 1));
    TransP idm1    (make_id(m1));
    TransP tmp4    (Kronecker(crt_p.get(), idm1.get()));
      
    return Dot(tmp3.get(), tmp4.get());
  } else {
    return make_transf(1, 1, make_crt(p));
  }
}

Trans *get_crt(uint64_t m)
{
  return KroneckerProd(get_crt_base, m);
}

Trans *get_icrt_base(uint64_t p, uint64_t k)
{
  using TransP = std::unique_ptr<Trans>;
  
  if (k > 1) {
    uint64_t m1    = pown(p, k-1);
    uint64_t m     = m1 * p;
    uint64_t phi_m = m - m1;
      
    TransP perm    (make_transf(1, 1, make_permutation(phi_m, phi_m/m1)));
    TransP idp     (make_id(p-1));
    TransP dft_m1  (get_idft(m1));
    TransP tmp1    (Kronecker(idp.get(), dft_m1.get()));
    TransP tmp2    (Dot(tmp1.get(), perm.get()));
    TransP twiddle (make_transf(1, 1, make_twiddle_crt_inv(m)));
    TransP tmp3    (Dot(twiddle.get(), tmp2.get()));
    TransP crt_p   (get_icrt_base(p, 1));
    TransP idm1    (make_id(m1));
    TransP tmp4    (Kronecker(crt_p.get(), idm1.get()));
      
    return Dot(tmp4.get(), tmp3.get());
  } else {
    return make_transf(1, 1, make_crt_inv(p));
  }
}

Trans *get_icrt(uint64_t m)
{
  return KroneckerProd(get_icrt_base, m);
}


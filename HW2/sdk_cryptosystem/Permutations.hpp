#pragma once
#include <vector>
#include <cstdint>
#include <iostream>
#include <algorithm>
#include <exception>

using matrix_t =
  std::vector<std::vector<uint64_t>>;

using permutation_t =
  std::vector<uint64_t>;

template<typename T>
void print_perm(std::vector<T> x)
{
  std::cout << "(";

  for (uint64_t i = 0; i < x.size()-1; i++)
    std::cout << x[i] << ", ";

  std::cout << x.back() << ")";
}

/* Decomposes a semimagic matrix m (i.e. a matrix such that the
   entries of each column and line add up to the same value) into a
   sum of permutions. The returned vector contains the multiplicity
   of each permutation and the permutation itself represented as
   a vector */
std::vector<std::pair<uint64_t, permutation_t>> decompose_semimagic
(matrix_t m);

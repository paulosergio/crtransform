#include "DSL2.hpp"
#include <cassert>
#include <memory>
#include "NumTh.hpp"
#include "Permutations.hpp"

static bool s_debug;

void dsl2_set_debug(bool debug)
{
  s_debug = debug;
}

bool dsl2_get_debug()
{
  return s_debug;
}

Trans *Dot(const Trans *t1, const Trans *t2)
{
  assert(get_dim(t1) == get_dim(t2));

  if (get_type(t1) == T_ID) return get_copy(t2);
  if (get_type(t2) == T_ID) return get_copy(t1);

  Trans *t1copy = get_copy(t1);
  Trans *t2copy = get_copy(t2);

  Trans *it = t2copy;
  while (get_type(it->m_trans.m_transf.prev) != T_ID)
    it = it->m_trans.m_transf.prev;

  delete it->m_trans.m_transf.prev;
  it->m_trans.m_transf.prev = t1copy;

  return t2copy;
}

Trans *Kronecker(Trans *t1, Trans *t2)
{
  if (get_type(t1) == T_ID && get_type(t2) == T_ID)
    return make_id(get_dim(t1) * get_dim(t2));

  if (get_type(t1) == T_ID && get_type(t2) == T_TENSORABLE) {
    Trans *prev = Kronecker(t1, t2->m_trans.m_transf.prev);
    return make_transf(get_dim(t1) * t2->m_trans.m_transf.l,
		       t2->m_trans.m_transf.r,
		       t2->m_trans.m_transf.f,
		       prev);
  }

  if (get_type(t1) == T_TENSORABLE && get_type(t2) == T_ID) {
    Trans *prev = Kronecker(t1->m_trans.m_transf.prev, t2);
    return make_transf(t1->m_trans.m_transf.l,
		       t1->m_trans.m_transf.r * get_dim(t2),
		       t1->m_trans.m_transf.f,
		       prev);
  }

  if (get_type(t1) == T_TENSORABLE && get_type(t2) == T_TENSORABLE) {
    std::unique_ptr<Trans> id1(make_id(get_dim(t1)));
    std::unique_ptr<Trans> id2(make_id(get_dim(t2)));
    std::unique_ptr<Trans> pp1(Kronecker(t1, id2.get()));
    std::unique_ptr<Trans> pp2(Kronecker(id1.get(), t2));
    return Dot(pp1.get(), pp2.get());
  }

  return nullptr;
}

std::ostream& operator<<(std::ostream &os, const Trans &t)
{
  switch (get_type(&t)) {
  case T_ID:
    os << "I" << get_dim(&t);
    break;
  case T_TENSORABLE:
    os << *t.m_trans.m_transf.prev;
    os << "*";
    os << "(I" << t.m_trans.m_transf.l << " @ "
       << t.m_trans.m_transf.f << " @ I"
       << t.m_trans.m_transf.r << ")";
    break;
  }

  return os;
}

Trans* make_id(uint64_t m)
{
  Trans* t = new Trans;
  t->m_type = T_ID;
  t->m_trans.m_id.m = m;
  return t;
}

Trans* make_transf(uint64_t l,
		   uint64_t r,
		   BasicTrans f)
{
  Trans *t = new Trans;
  t->m_type = T_TENSORABLE;
  t->m_trans.m_transf.l = l;
  t->m_trans.m_transf.r = r;
  t->m_trans.m_transf.f = f;
  t->m_trans.m_transf.prev = make_id(l*get_dim(f)*r);
  return t;
}

Trans* make_transf(uint64_t l,
		   uint64_t r,
		   BasicTrans f,
		   Trans *pt)
{
  Trans *t = new Trans;
  t->m_type = T_TENSORABLE;
  t->m_trans.m_transf.l = l;
  t->m_trans.m_transf.r = r;
  t->m_trans.m_transf.f = f;
  t->m_trans.m_transf.prev = pt;
  return t;
}

uint64_t get_dim(const Trans *t)
{
  switch(get_type(t)) {
  case T_ID:
    return t->m_trans.m_id.m;
  case T_TENSORABLE:
    return (t->m_trans.m_transf.l *
	    get_dim(t->m_trans.m_transf.f) *
	    t->m_trans.m_transf.r);
  }

  return 0;
}

Trans* get_copy(const Trans *t)
{
  switch(get_type(t)) {
  case T_ID:
    return new Trans(*t);
  case T_TENSORABLE:
    Trans *t1 = make_transf(t->m_trans.m_transf.l,
			    t->m_trans.m_transf.r,
			    t->m_trans.m_transf.f,
			    get_copy(t->m_trans.m_transf.prev));
    return t1;
  }

  return nullptr;
}

Trans::~Trans()
{
  if (get_type(this) == T_TENSORABLE) {
    delete m_trans.m_transf.prev;
  }
}

TransType get_type(const Trans *t)
{
  return t->m_type;
}

Trans *KroneckerProd(std::function<Trans*(uint64_t, uint64_t)> base,
		     uint64_t m)
{
  auto fs = factor(m);
  using TransP = std::unique_ptr<Trans>;
  
  uint64_t p, k;
  std::tie(p, k) = fs[0];
  Trans* prod    = base(p, k);

  for (auto it = std::begin(fs) + 1; it != std::end(fs); it++) {
    std::tie(p, k) = *it;
    TransP tmp     (base(p, k));
    Trans* prod1   = Kronecker(prod, tmp.get());
    std::swap(prod, prod1);
    delete prod1;
  }

  return prod;
}

Trans* parallelise(const Trans *t)
{
  Trans *t1 = get_copy(t);

  auto pure_perm = [] (const Trans *perm) {
    return ((perm != nullptr) &&
	    (get_type(perm) == T_TENSORABLE) &&
	    (get_type(perm->m_trans.m_transf.f) == BT_PERM) &&
	    (perm->m_trans.m_transf.r == 1) &&
	    (perm->m_trans.m_transf.l == 1));
  };

  Trans *after = nullptr;
  Trans *it    = t1;

  while (get_type(it) != T_ID) {
    uint64_t m     = get_dim(it);
    
    if (it->m_trans.m_transf.r != 1 &&
	it->m_trans.m_transf.r != m) {
      
      if (pure_perm(after) &&
	  ((m / get_permutation_n(after->m_trans.m_transf.f)) %
	   it->m_trans.m_transf.r == 0)) {
	uint64_t old_n = get_permutation_n(after->m_trans.m_transf.f);
	assert(get_dim(it) == get_dim(after));
	uint64_t new_n = it->m_trans.m_transf.r * old_n;
	while(new_n % m == 0) new_n /= m;
	if (new_n != 1) {
	  set_permutation_n(after->m_trans.m_transf.f, new_n);
	} else {
	  if (after == t1) {
	    t1 = it;
	    after->m_trans.m_transf.prev = nullptr; //avoiding getting the rest of the list deleted
	    delete after;
	    after = nullptr;
	  } else {
	    Trans *it2 = t1;
	    while (it2->m_trans.m_transf.prev != after)
	      it2 = it2->m_trans.m_transf.prev;
	    it2->m_trans.m_transf.prev = it;
	    after->m_trans.m_transf.prev = nullptr;
	    delete after;
	    after = it2;
	  }
	}
	
	assert(m % it->m_trans.m_transf.r == 0);
	Trans *B = it->m_trans.m_transf.prev;
	it->m_trans.m_transf.prev = make_transf(1, 1,
						make_permutation(m, m/it->m_trans.m_transf.r),
						B);
      } else {
	if (after == nullptr) {
	  t1 = make_transf(1, 1,
			   make_permutation(m, it->m_trans.m_transf.r),
			   it);
	  after = t1;
	} else {
	  after->m_trans.m_transf.prev = make_transf(1, 1,
						     make_permutation(m, it->m_trans.m_transf.r),
						     it);
	}
	
	assert(m % it->m_trans.m_transf.r == 0);
	Trans *B = it->m_trans.m_transf.prev;
	it->m_trans.m_transf.prev = make_transf(1, 1,
						make_permutation(m, m/it->m_trans.m_transf.r),
						B);
	
      }

      it->m_trans.m_transf.l *= it->m_trans.m_transf.r;
      it->m_trans.m_transf.r = 1;

    } else if (it->m_trans.m_transf.r == m) {
      it->m_trans.m_transf.r = 1;
      it->m_trans.m_transf.l = m;
    }

    after = it;
    it    = it->m_trans.m_transf.prev;
  }

  return t1;
}



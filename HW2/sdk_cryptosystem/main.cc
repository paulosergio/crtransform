/*
 * Empty C++ Application
 */

#include "platform.h"
#include "DSLHardware.hpp"
#include "DSLSoftware.hpp"
#include "CompactCryptosystem.hpp"
#include <stdlib.h>
#include <stdio.h>
#include "NumTh.hpp"
#include <chrono>
#include "xtime_l.h"
#include "config.hpp"

int main()
{
  init_platform();
  {
  uint64_t numtests = 1024;

  typename ZZq::value_type p = 2;
  auto *transforms =
		  new DSLHardware<ZZq,ZZ>();
  CompactCryptosystem<ZZq,ZZ> system(mConfig, qConfig, p, transforms);

  uint64_t errors = 0;
  double avg_enc = 0;
  double avg_dec = 0;
  for (uint64_t i = 0; i < numtests; i++) {
	  XTime begin, end;
	  CompactCryptosystem<ZZq,ZZ>::plaintext_t
	  t(system.m_n);
	  for (auto &ti : t) ti = rand() & 1;

	  XTime_GetTime(&begin);
	  auto c = system.enc(t);
	  XTime_GetTime(&end);

	  avg_enc += end-begin;

	  XTime_GetTime(&begin);
	  auto t1 = system.dec(c);
	  XTime_GetTime(&end);

	  avg_dec += end-begin;

	  for (uint64_t i = 0; i < system.m_n; i++) {
		  if (t[i] != t1[i]) {
			  errors++;
		  }
	  }
  }
  avg_enc /= static_cast<double>(numtests);
  avg_dec /= static_cast<double>(numtests);
  std::cout << "## HARDWARE ##\n";
  std::cout << "m = " << mConfig << "\n";
  std::cout << "q = " << qConfig << "\n";
  std::cout << "errors = " << errors << "/"
		  << numtests * system.m_n << "\n";
  std::cout << "avg_enc = " << avg_enc / COUNTS_PER_SECOND << "\n";
  std::cout << "avg_dec = " << avg_dec / COUNTS_PER_SECOND << "\n";
  }
  {
  uint64_t numtests = 1024;

  typename ZZq::value_type p = 2;
  auto *transforms =
		  new DSLSoftware<ZZq,ZZ>(mConfig, qConfig);
  CompactCryptosystem<ZZq,ZZ> system(mConfig, qConfig, p, transforms);

  uint64_t errors = 0;
  double avg_enc = 0;
  double avg_dec = 0;
  for (uint64_t i = 0; i < numtests; i++) {
	  XTime begin, end;
	  CompactCryptosystem<ZZq,ZZ>::plaintext_t
	  t(system.m_n);
	  for (auto &ti : t) ti = rand() & 1;

	  XTime_GetTime(&begin);
	  auto c = system.enc(t);
	  XTime_GetTime(&end);
	  avg_enc += end-begin;

	  XTime_GetTime(&begin);
	  auto t1 = system.dec(c);
	  XTime_GetTime(&end);
	  avg_dec += end-begin;

	  for (uint64_t i = 0; i < system.m_n; i++) {
		  if (t[i] != t1[i]) {
			  errors++;
		  }
	  }
  }
  avg_enc /= static_cast<double>(numtests);
  avg_dec /= static_cast<double>(numtests);
  std::cout << "## SOFTWARE ##\n";
  std::cout << "m = " << mConfig << "\n";
  std::cout << "q = " << qConfig << "\n";
  std::cout << "errors = " << errors << "/"
		  << numtests * system.m_n << "\n";
  std::cout << "avg_enc = " << avg_enc / COUNTS_PER_SECOND << "\n";
  std::cout << "avg_dec = " << avg_dec / COUNTS_PER_SECOND << "\n";

  }
  cleanup_platform();
  return 0;
}

#include "Permutations.hpp"

std::vector<std::pair<uint64_t, permutation_t>> decompose_semimagic
(matrix_t m)
{
  uint64_t n  = m.size();
  uint64_t *s = new uint64_t[n];
  uint64_t j  = n; // size of s
  uint64_t min;
  std::vector<std::pair<uint64_t, permutation_t>> res;

  for (uint64_t i = 0; i < n; i++)
    s[i] = i;

  auto dec_m = [&m, &s, &n, &res] (uint64_t f) -> void {
    res.push_back(std::make_pair(f, std::vector<uint64_t>(s, s+n)));

    for (uint64_t i = 0; i < n; i++)
      m[s[i]][i] -= f;
  };

  auto min_m_s = [&m, &s, &n] () -> uint64_t {
    uint64_t min = m[s[0]][0];
    for (uint64_t i = 1; i < n && min != 0; i++)
      if (min > m[s[i]][i])
	min = m[s[i]][i];
    
    return min;
  };

  if ((min = min_m_s()) != 0) {
    dec_m(min);
  }

  for (;;) {
    for (uint64_t i = 0; i < j; i++) {
      if (m[s[i]][i] == 0) {
	j = i + 1;
	break;
      }
    }

    while (std::all_of(s + j,
		       s + n,
		       [&j,&s] (uint64_t x) {
			 return x <= s[j - 1];
		       })) {
      if (j == 1) goto decompose_semimagic_end;
      j--;
    }

    std::sort(s + j, s + n);

    bool nonzero = false;
    uint64_t i1;
    for (uint64_t i = 0; i < n-j; i++) {
      if (s[i + j] > s[j - 1] &&
	  m[s[i + j]][j - 1] != 0) {
	std::swap(s[j - 1], s[i + j]);
	i1 = i;
	nonzero = true;
	break;
      }
    }

    if (nonzero) {
      while (i1 > 0 &&
	     s[i1 + j] < s[i1 + j - 1]) {
	std::swap(s[i1 + j], s[i1 + j - 1]);
	i1--;
      }

      if ((min = min_m_s()) != 0) {
	dec_m(min);
      }

      j = n-1;
    } else {
      if (j == 1) goto decompose_semimagic_end;
      j--;
    }
  };

 decompose_semimagic_end:
  delete s;
  return res;
}

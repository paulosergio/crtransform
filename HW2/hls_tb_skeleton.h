#ifndef __HLS_TEST_TB_H__
#define __HLS_TEST_TB_H__
#include "hls_test.h"

#define numTests @numTests@

double to_double(data_t x);

static data_t inputs[numTests][dataMemSize] =
  @inputs@;
static data_t outputs[numTests][dataMemSize] =
  @outputs@;
static const double maxError = @maxError@;

struct call_parameters_t {
  size_t dataLength, blockSize;
  my_addr_t ptr;
  bool is_fixed_point;
};

static struct call_parameters_t call_parameters[numTests] =
  @call_parameters@;

#endif

#include "SystemFact.hpp"

call_parameters_t::call_parameters_t(size_t dataLength,
				     size_t blockSize,
				     uint64_t ptr,
				     bool is_fixed_point)
  : m_dataLength(dataLength)
  , m_blockSize(blockSize)
  , m_ptr(ptr)
  , m_is_fixed_point(is_fixed_point)
{
}

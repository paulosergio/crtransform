#pragma once
#include <cstdint>

using ZZq = ZZq16_t;
using ZZ = ZZ16_t;
const uint64_t numProcessorsConfig = 2;
const uint64_t qConfig = 3329;
const uint64_t numbitsConfig = 12;
const uint64_t numbits2Config = 16;
const uint64_t fixedpointConfig = 6;
const uint64_t mConfig = 256;

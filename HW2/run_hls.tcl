open_project -reset crtransform_hls2
set_top top_module
add_files hls_test.c
add_files -tb hls_tb_test.c

open_solution "solution1"
set_part {xc7z100ffg900-2}
create_clock -period 10 -name default

csim_design -clean
csynth_design
cosim_design -rtl vhdl
export_design -format ip_catalog

exit

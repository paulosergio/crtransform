#pragma once
#include "DSL2.hpp"
#include <cstdint>
#include <vector>
#include <tuple>
#include "Permutations.hpp"
#include "BasicTrans.hpp"
#include <cassert>
#include <algorithm>
#include "NumTh.hpp"
#include "hls_gen_c.hpp"
#include "hls_gen_h.hpp"
#include <sstream>
#include "CPrinter.hpp"
#include <fstream>


/* As an intermediate representation of transforms, we convert
   the linked list representation to a vector of pairs. Each pair
   has a permutation  and a "singleton" transform that can be
   computed by the PEs. Execution proceeds by sequentially
   permuting data and processing it. */
using intermediate_repr_t =
  std::vector<std::pair<permutation_t,
			Trans*>>; //!= Perm

/* Computes the permutation that results from first permuting
   data according to a and afterwards according to perm */
permutation_t update_perm(permutation_t a,
			  Trans * perm);

/* Converts a transform described with Trans* to one described in
   intermediate_repr_t. To achieve the best results, ops should
   be in the parallelised form */
intermediate_repr_t convert_trans(Trans *ops);

/* Computes how much data each of the w PEs will process for each
   of the operations in repr */
std::vector<std::vector<uint64_t>>
data_processor_allocation(const intermediate_repr_t &repr,
			  uint64_t w);

/* Converts a number in binary representation to an "additive"
   representation. This representation has two digits (j, i)
   representing the value:
   weights[0] + ... + weights[j-1] + i
   Given the mapping of data computed by data_processor_allocation,
   this number representation allows us to determine by which PE
   each element of data will be processed */
std::pair<uint64_t, uint64_t> get_additive
(std::vector<uint64_t> weights,
 uint64_t i);
/* Reverses the above operation */
uint64_t from_additive
(std::vector<uint64_t> weights,
 std::pair<uint64_t, uint64_t> ji);

/* Since the permuter computes permutations for fixed data blocks,
   but the PEs process a variable amount of data, the data size is
   increased such that the fixed data block of the permuter can
   accomodate all PEs' blocks. While the permutations of the original
   data will operate on addresses described with values (i, j) for
   additive representations supported on the weights outputed by
   data_processor_allocation, liftPerm will lift these permutations
   to addresses (i, j) for a constant weight */
permutation_t liftPerm
(std::vector<uint64_t> prev_weights,
 permutation_t perm,
 std::vector<uint64_t> weights,
 uint64_t n1,
 uint64_t kmax);

std::tuple<
  std::vector<std::vector<uint64_t>>, //read_addr
  std::vector<std::vector<uint64_t>>, //write_addr
  std::vector<std::vector<uint64_t>>  //interconnect
  >
schedule(permutation_t p, uint64_t numProcessors, uint64_t blockSize);

void update_data_processor_allocation(const intermediate_repr_t &repr,
				      std::vector<std::vector<uint64_t>> &map,
				      uint64_t kmax,
				      uint64_t w);

struct permutation_instr_t {
  std::vector<uint64_t> m_read_address;
  std::vector<uint64_t> m_write_address;
  std::vector<uint64_t> m_interconnect;
  permutation_instr_t(std::vector<uint64_t> read_address,
		      std::vector<uint64_t> write_address,
		      std::vector<uint64_t> interconnect);
};

struct permutation_block_t {
  uint64_t m_idx;
  permutation_t m_reference;
  permutation_block_t(uint64_t idx,
		      permutation_t reference);
};

enum ring_t {
  RING_ZZ,
  RING_ZZq,
  RING_RR
};

enum op_t {
  OP_VECTOR,
  OP_MATRIX,
  OP_ID
};

template<typename ZZq>
struct data_block_t {
  uint64_t m_idx;
  std::vector<typename ZZq::value_type> m_reference;
  data_block_t(uint64_t idx, std::vector<typename ZZq::value_type> reference);
};

struct instruction_t {
  uint64_t perm_ptr, perm_size;
  uint64_t vmmult_ring, vmmult_op;
  std::vector<uint64_t> vmmult_h, vmmult_w, vmmult_ptr;
  bool end;
};

template<typename ZZq>
struct vmmult_instr_t {
  std::vector<std::vector<typename ZZq::value_type>> m_data_blocks;
  std::vector<uint64_t> m_w, m_h;
  op_t m_op;
  ring_t m_ring;
  vmmult_instr_t
  (std::vector<std::vector<typename ZZq::value_type>> data_blocks,
   std::vector<uint64_t> w,
   std::vector<uint64_t> h,
   op_t op,
   ring_t ring);
};

struct entry_point_t {
  uint64_t m_ptr, m_kmax;
  entry_point_t(uint64_t ptr, uint64_t kmax);
};

template<typename ZZq>
struct System {
  uint64_t m_numProcessors, m_q, m_qn, m_numbits, m_numbits2, m_fixedpoint;
  uint64_t m_m, m_n;
  
  std::vector<permutation_instr_t> permMem;
  std::vector<permutation_block_t> permBlocks;

  std::vector<std::vector<typename ZZq::value_type>> cnstMem;
  std::vector<std::vector<data_block_t<ZZq>>> dataBlock;

  std::vector<instruction_t> instMem;
  std::vector<entry_point_t> entry_points;

  uint64_t dataMemSize;

  System(uint64_t numProcessors,
	 typename ZZq::value_type q,
	 uint64_t numbits,
	 uint64_t numbits2,
	 uint64_t fixedpoint,
	 uint64_t m);
  
  void updateSystem(Trans *ops);

  void generateHls(std::string fileName) const;  

  typename ZZq::value_type fromDouble(double x) const;
  
private:
  vmmult_instr_t<ZZq>
  get_vmmult_instr
  (Trans *it,
   typename ZZq::value_type w0,
   std::vector<uint64_t> map);
};

void system_set_debug(bool debug);
bool system_get_debug();

template<typename ZZq>
vmmult_instr_t<ZZq>::vmmult_instr_t
(std::vector<std::vector<typename ZZq::value_type>> data_blocks,
 std::vector<uint64_t> w,
 std::vector<uint64_t> h,
 op_t op,
 ring_t ring)
  : m_data_blocks(data_blocks),
    m_w(w),
    m_h(h),
    m_op(op),
    m_ring(ring)
{
}

template<typename ZZq>
data_block_t<ZZq>::data_block_t(uint64_t idx,
			   std::vector<typename ZZq::value_type> reference)
  : m_idx(idx),
    m_reference(reference)
{
}

template<typename ZZq>
System<ZZq>::System
(uint64_t numProcessors,
 typename ZZq::value_type q,
 uint64_t numbits,
 uint64_t numbits2,
 uint64_t fixedpoint,
 uint64_t m)
  : m_numProcessors(numProcessors),
    m_q(q),
    m_qn((static_cast<typename ZZq::greater_value_type>(1)<<(2*numbits+1)) / m_q),
    m_numbits(numbits),
    m_numbits2(numbits2),
    m_fixedpoint(fixedpoint),
    m_m(m),
    m_n(totient(m)),
    cnstMem(numProcessors),
    dataBlock(numProcessors),
    dataMemSize(0)
{
}

template<typename ZZq>
void
System<ZZq>::updateSystem(Trans *ops)
{
  Trans *parOps = parallelise(ops);

  auto repr = convert_trans(parOps);

  std::vector<instruction_t> instructions(repr.size());
  
  auto map = data_processor_allocation(repr, m_numProcessors);
  uint64_t kmax = 0;

  for (auto op_i : map) {
    for (auto map_j : op_i) {
      kmax = std::max(kmax, map_j);
    }
  }

  uint64_t n1 = kmax * m_numProcessors;

  update_data_processor_allocation(repr, map, kmax, m_numProcessors);

  std::vector<permutation_t> lifted_perms(repr.size());
  std::vector<uint64_t> def_weights(m_numProcessors, kmax);

  for (uint64_t i = 0; i < repr.size(); i++) {
    if (system_get_debug()) {
      std::cout << "t = " << i << "\n";
    }
    lifted_perms[i] = liftPerm((i == 0 ? def_weights : map[i-1]),
			       repr[i].first,
			       map[i],
			       n1,
			       kmax);
  }

  for (uint64_t i = 0; i < lifted_perms.size(); i++) {
    auto permIt = std::find_if(permBlocks.begin(),
			       permBlocks.end(),
			       [&] (const permutation_block_t &block) {
				 return ((lifted_perms[i].size() == block.m_reference.size()) &&
					 std::equal(lifted_perms[i].begin(),
						    lifted_perms[i].end(),
						    block.m_reference.begin()));
			       });

    if (permIt != permBlocks.end()) {
      instructions[i].perm_ptr  = permIt->m_idx;
      instructions[i].perm_size = kmax;
    } else {
      instructions[i].perm_ptr  = permMem.size();
      instructions[i].perm_size = kmax;
      permBlocks.emplace_back(permMem.size(), lifted_perms[i]);

      std::vector<std::vector<uint64_t>> read_addr;
      std::vector<std::vector<uint64_t>> write_addr;
      std::vector<std::vector<uint64_t>> interconnect;
      std::tie(read_addr, write_addr, interconnect) =
	schedule(lifted_perms[i], m_numProcessors, kmax);
      for (uint64_t i = 0; i < kmax; i++) {
	permMem.emplace_back(read_addr[i],
			     write_addr[i],
			     interconnect[i]);
      }
    }
  }

  typename ZZq::value_type w0 =
    findElemOfOrder<ZZq>(m_m, m_q, factor(m_m));
  
  for (uint64_t i = 0; i < repr.size(); i++) {
    if (get_type(repr[i].second) == T_ID) {
      instructions[i].end           = true;
      instructions[i].vmmult_h      = std::vector<uint64_t>(m_numProcessors, 1);
      instructions[i].vmmult_w      = std::vector<uint64_t>(m_numProcessors, kmax);
      instructions[i].vmmult_ring   = 0;
      instructions[i].vmmult_op     = OP_ID;
      instructions[i].vmmult_ptr    = std::vector<uint64_t>(m_numProcessors, 0);
    } else {
      auto vmmult_instr = get_vmmult_instr(repr[i].second,
					   w0,
					   map[i]);
      instructions[i].vmmult_ring = vmmult_instr.m_ring;
      instructions[i].vmmult_op   = vmmult_instr.m_op;
      instructions[i].vmmult_h    = vmmult_instr.m_h;
      instructions[i].vmmult_w    = vmmult_instr.m_w;
      instructions[i].vmmult_ptr  = std::vector<uint64_t>(m_numProcessors, 0);

      for (uint64_t j = 0; j < m_numProcessors; j++) {
	auto cnstIt = find_if(dataBlock[j].begin(),
			      dataBlock[j].end(),
			      [&] (const data_block_t<ZZq> &b) {
				return (vmmult_instr.m_data_blocks[j].size() == b.m_reference.size()) &&
				std::equal(b.m_reference.begin(),
					   b.m_reference.end(),
					   vmmult_instr.m_data_blocks[j].begin());
			      });

	if (cnstIt != dataBlock[j].end()) {
	  instructions[i].vmmult_ptr[j] = cnstIt->m_idx;
	} else {
	  instructions[i].vmmult_ptr[j] = cnstMem[j].size();
	  dataBlock[j].emplace_back(cnstMem[j].size(),
				    vmmult_instr.m_data_blocks[j]);
	  cnstMem[j].insert(cnstMem[j].end(),
			    vmmult_instr.m_data_blocks[j].begin(),
			    vmmult_instr.m_data_blocks[j].end());
			    
	}
      }
    }
  }

  entry_points.emplace_back(instMem.size(), kmax);
  instMem.insert(instMem.end(),
		 instructions.begin(),
		 instructions.end());

  dataMemSize = std::max(dataMemSize, n1);
  
  delete parOps;
}


template<>
struct GetString<permutation_instr_t>
{
  std::string operator()(const permutation_instr_t &p)
  {
    std::stringstream ss;
    ss << "{\n";
    ss << get_string(p.m_read_address) << ",\n";
    ss << get_string(p.m_write_address) << ",\n";
    ss << get_string(p.m_interconnect) << "\n";
    ss << "}";
    return ss.str();
  }
};

template<>
struct GetString<instruction_t>
{
  std::string operator()(const instruction_t &inst)
  {
    std::stringstream ss;
    ss << "{\n";
    ss << get_string(inst.perm_ptr) << ",\n";
    ss << get_string(inst.perm_size) << ",\n";
    ss << get_string(inst.vmmult_ring) << ",\n";
    ss << get_string(inst.vmmult_op) << ",\n";
    ss << get_string(inst.vmmult_h) << ",\n";
    ss << get_string(inst.vmmult_w) << ",\n";
    ss << get_string(inst.vmmult_ptr) << ",\n";
    ss << (inst.end ? "true" : "false") << "\n";
    ss << "}\n";
    return ss.str();
  }
};


template<typename ZZq>
vmmult_instr_t<ZZq>
System<ZZq>::get_vmmult_instr
(Trans *it,
 typename ZZq::value_type w0,
 std::vector<uint64_t> map)
{
  assert(get_type(it) != T_ID);

  switch(get_type(it->m_trans.m_transf.f)) {
  case BT_PERM:
    abort();
	  
  case BT_TWIDDLE:
    {
      uint64_t l = it->m_trans.m_transf.l;
      uint64_t block_size = m_n/l;
      std::vector<typename ZZq::value_type> factors(m_n);
      typename ZZq::value_type w = pown<ZZq>(w0, m_m/block_size, m_q);
      auto fs = factor(block_size);
      assert(fs.size() == 1);
      uint64_t bs1 = pown(fs[0].first, fs[0].second-1);

      for (uint64_t k = 0; k < block_size; k++) {
	uint64_t i0, i1;
	i1 = k % bs1;
	i0 = k / bs1;
	//k = i0 * bs1 + i1
	factors[k] = pown<ZZq>(w, i1*i0, m_q);
      }

      for (uint64_t k = 1; k < l; k++) {
	std::copy(std::begin(factors),
		  std::begin(factors) + block_size,
		  std::begin(factors) + k*block_size);
      }

      uint64_t consumed = 0;
      std::vector<std::vector<typename ZZq::value_type>> data_blocks(m_numProcessors);
      std::vector<uint64_t> ws(m_numProcessors), hs(m_numProcessors);
      for (uint64_t j = 0; j < m_numProcessors; j++) {
	data_blocks[j] = std::vector<typename ZZq::value_type>
	  (std::begin(factors) + consumed,
	   std::begin(factors) + consumed + map[j]);
	consumed += map[j];
	hs[j] = 1;
	ws[j] = map[j];
      }

      return vmmult_instr_t<ZZq>(data_blocks,
				 ws,
				 hs,
				 OP_VECTOR,
				 RING_ZZq);
    }
    break;
	  
  case BT_TWIDDLE_INV:
    {
      uint64_t l = it->m_trans.m_transf.l;
      uint64_t block_size = m_n/l;
      std::vector<typename ZZq::value_type> factors(m_n);
      typename ZZq::value_type w = modinv<ZZq>(pown<ZZq>(w0, m_m/block_size, m_q), m_q);
      auto fs = factor(block_size);
      assert(fs.size() == 1);
      uint64_t bs1 = pown(fs[0].first, fs[0].second-1);

      for (uint64_t k = 0; k < block_size; k++) {
	uint64_t i0, i1;
	i1 = k % bs1;
	i0 = k / bs1;
	//k = i0 * bs1 + i1
	factors[k] = pown<ZZq>(w, i1*i0, m_q);
      }

      for (uint64_t k = 1; k < l; k++) {
	std::copy(std::begin(factors),
		  std::begin(factors) + block_size,
		  std::begin(factors) + k*block_size);
      }

      uint64_t consumed = 0;
      std::vector<std::vector<typename ZZq::value_type>> data_blocks(m_numProcessors);
      std::vector<uint64_t> ws(m_numProcessors), hs(m_numProcessors);
      for (uint64_t j = 0; j < m_numProcessors; j++) {
	data_blocks[j] = std::vector<typename ZZq::value_type>
	  (std::begin(factors) + consumed,
	   std::begin(factors) + consumed + map[j]);
	consumed += map[j];
	hs[j] = 1;
	ws[j] = map[j];
      }

      return vmmult_instr_t<ZZq>(data_blocks,
				 ws,
				 hs,
				 OP_VECTOR,
				 RING_ZZq);
    }
    break;
	  
  case BT_TWIDDLE_CRT:
    {
      uint64_t l = it->m_trans.m_transf.l;
      uint64_t block_size = m_n/l;
      uint64_t crt_index = (it->m_trans.m_transf.f)
	.m_trans.m_twiddle_crt.m;
      std::vector<typename ZZq::value_type> factors(m_n);
      typename ZZq::value_type w = pown<ZZq>(w0, m_m/crt_index, m_q);
      auto fs = factor(crt_index);
      assert(fs.size() == 1);
      uint64_t bs1 = pown(fs[0].first, fs[0].second-1);

      for (uint64_t i = 1; i < fs[0].first; i++) {
	for (uint64_t j = 0; j < bs1; j++) {
	  factors[(i-1)*bs1 + j] = pown<ZZq>(w, i*j, m_q);
	  if (system_get_debug()) {
	    std::cout << "factors[" << (i-1)*bs1 + j << "] = "
		      << factors[(i-1)*bs1 + j] << "\n";
	  }
	}
      }
	    
      for (uint64_t k = 1; k < l; k++) {
	std::copy(std::begin(factors),
		  std::begin(factors) + block_size,
		  std::begin(factors) + k*block_size);
      }

      uint64_t consumed = 0;
      std::vector<std::vector<typename ZZq::value_type>> data_blocks(m_numProcessors);
      std::vector<uint64_t> ws(m_numProcessors), hs(m_numProcessors);
      for (uint64_t j = 0; j < m_numProcessors; j++) {
	data_blocks[j] = std::vector<typename ZZq::value_type>
	  (std::begin(factors) + consumed,
	   std::begin(factors) + consumed + map[j]);
	consumed += map[j];
	hs[j] = 1;
	ws[j] = map[j];
      }

      return vmmult_instr_t<ZZq>(data_blocks,
				 ws,
				 hs,
				 OP_VECTOR,
				 RING_ZZq);
      
    }
    break;
	  
  case BT_TWIDDLE_CRT_INV:
    {
      uint64_t l = it->m_trans.m_transf.l;
      uint64_t block_size = m_n/l;
      uint64_t crt_index = (it->m_trans.m_transf.f)
	.m_trans.m_twiddle_crt_inv.m;
      std::vector<typename ZZq::value_type> factors(m_n);
      typename ZZq::value_type w = modinv<ZZq>(pown<ZZq>(w0, m_m/crt_index, m_q), m_q);
      auto fs = factor(crt_index);
      assert(fs.size() == 1);
      uint64_t bs1 = pown(fs[0].first, fs[0].second-1);

      for (uint64_t i = 1; i < fs[0].first; i++) {
	for (uint64_t j = 0; j < bs1; j++) {
	  factors[(i-1)*bs1 + j] = pown<ZZq>(w, i*j, m_q);
	}
      }

      for (uint64_t k = 1; k < l; k++) {
	std::copy(std::begin(factors),
		  std::begin(factors) + block_size,
		  std::begin(factors) + k*block_size);
      }
      uint64_t consumed = 0;
      std::vector<std::vector<typename ZZq::value_type>> data_blocks(m_numProcessors);
      std::vector<uint64_t> ws(m_numProcessors), hs(m_numProcessors);
      for (uint64_t j = 0; j < m_numProcessors; j++) {
	data_blocks[j] = std::vector<typename ZZq::value_type>
	  (std::begin(factors) + consumed,
	   std::begin(factors) + consumed + map[j]);
	consumed += map[j];
	hs[j] = 1;
	ws[j] = map[j];
      }

      return vmmult_instr_t<ZZq>(data_blocks,
				 ws,
				 hs,
				 OP_VECTOR,
				 RING_ZZq);

    }
    break;
	  
  case BT_DFT:
    {
      uint64_t l = it->m_trans.m_transf.l;
      uint64_t block_size = m_n/l;
      std::vector<typename ZZq::value_type> mat(block_size * block_size);
      typename ZZq::value_type w = pown<ZZq>(w0, m_m/block_size, m_q);

      for (uint64_t i = 0; i < block_size; i++) {
	for (uint64_t j = 0; j < block_size; j++) {
	  uint64_t ij = i*block_size + j;
	  mat[ij] = pown<ZZq>(w, i*j, m_q);
	}
      }

      std::vector<std::vector<typename ZZq::value_type>> data_blocks(m_numProcessors);
      std::vector<uint64_t> ws(m_numProcessors), hs(m_numProcessors);
      for (uint64_t j = 0; j < m_numProcessors; j++) {
	data_blocks[j] = mat;
	ws[j] = block_size;
	hs[j] = map[j]/block_size;
      }
      
      return vmmult_instr_t<ZZq>(data_blocks,
				 ws,
				 hs,
				 OP_MATRIX,
				 RING_ZZq);
    }
    break;

  case BT_CRT:
    {
      uint64_t l = it->m_trans.m_transf.l;
      uint64_t crt_index = (it->m_trans.m_transf.f)
	.m_trans.m_crt.m;
      uint64_t block_size = m_n/l;
      std::vector<typename ZZq::value_type> mat(block_size * block_size);
      typename ZZq::value_type w = pown<ZZq>(w0, m_m/crt_index, m_q);

      assert(crt_index - 1 == block_size);
      for (uint64_t i = 1; i < crt_index; i++) {
	for (uint64_t j = 0; j < block_size; j++) {
	  mat[(i-1)*block_size + j] = pown<ZZq>(w, i*j, m_q);
	}
      }

      std::vector<std::vector<typename ZZq::value_type>> data_blocks(m_numProcessors);
      std::vector<uint64_t> ws(m_numProcessors), hs(m_numProcessors);
      for (uint64_t j = 0; j < m_numProcessors; j++) {
	data_blocks[j] = mat;
	ws[j] = block_size;
	hs[j] = map[j]/block_size;
      }
      
      return vmmult_instr_t<ZZq>(data_blocks,
				 ws,
				 hs,
				 OP_MATRIX,
				 RING_ZZq);
	  
    }
    break;
	  
  case BT_DFT_INV:
    {
      uint64_t l = it->m_trans.m_transf.l;
      uint64_t block_size = m_n/l;
      std::vector<typename ZZq::value_type> mat(block_size * block_size);
      typename ZZq::value_type w = modinv<ZZq>(pown<ZZq>(w0, m_m/block_size, m_q), m_q);
      typename ZZq::value_type minv = modinv<ZZq>(block_size, m_q);
      
      for (uint64_t i = 0; i < block_size; i++) {
	for (uint64_t j = 0; j < block_size; j++) {
	  uint64_t ij = i*block_size + j;
	  mat[ij] = ((typename ZZq::greater_value_type)pown<ZZq>(w, i*j, m_q) * minv) % m_q;
	}
      }
      
      std::vector<std::vector<typename ZZq::value_type>> data_blocks(m_numProcessors);
      std::vector<uint64_t> ws(m_numProcessors), hs(m_numProcessors);
      for (uint64_t j = 0; j < m_numProcessors; j++) {
	data_blocks[j] = mat;
	ws[j] = block_size;
	hs[j] = map[j]/block_size;
      }
      
      return vmmult_instr_t<ZZq>(data_blocks,
				 ws,
				 hs,
				 OP_MATRIX,
				 RING_ZZq);

    }
    break;
	
  case BT_CRT_INV:
    {
      uint64_t l = it->m_trans.m_transf.l;
      uint64_t crt_index = (it->m_trans.m_transf.f)
	.m_trans.m_crt_inv.m;
      uint64_t block_size = m_n/l;
      assert(block_size + 1 == crt_index);
      typename ZZq::value_type minv = modinv<ZZq>(crt_index, m_q);

      std::vector<typename ZZq::value_type> mat_tmp(block_size * block_size);
      std::vector<typename ZZq::value_type> mat(block_size * block_size);
      typename ZZq::value_type w = modinv<ZZq>(pown<ZZq>(w0, m_m/crt_index, m_q), m_q);

      for (uint64_t i = 0; i < block_size; i++) {
	for (uint64_t j = 1; j < crt_index; j++) {
	  mat_tmp[i*block_size + j-1] = ((typename ZZq::greater_value_type)pown<ZZq>(w, i*j, m_q) * minv) % m_q;
	}
      }

      for (uint64_t i = 0; i < block_size; i++) {
	for (uint64_t j = 0; j < block_size; j++) {
	  mat[i*block_size + j] = 0;
	  for (uint64_t k = 0; k < block_size; k++) {
	    typename ZZq::value_type a = (k == i ? 2 : 1);
	    mat[i*block_size + j] = ((typename ZZq::greater_value_type)mat_tmp[k*block_size + j]*a
				     + mat[i*block_size + j]) % m_q;
	  }
	}
      }

      std::vector<std::vector<typename ZZq::value_type>> data_blocks(m_numProcessors);
      std::vector<uint64_t> ws(m_numProcessors), hs(m_numProcessors);
      for (uint64_t j = 0; j < m_numProcessors; j++) {
	data_blocks[j] = mat;
	ws[j] = block_size;
	hs[j] = map[j]/block_size;
      }
      
      return vmmult_instr_t<ZZq>(data_blocks,
				 ws,
				 hs,
				 OP_MATRIX,
				 RING_ZZq);
	  
    }
    break;

  case BT_CRT_B:
    {
      uint64_t l = it->m_trans.m_transf.l;
      uint64_t crt_index = (it->m_trans.m_transf.f)
	.m_trans.m_crt_inv.m;
      uint64_t block_size = m_n/l;
      std::complex<double> w = findElemOfOrderCC(crt_index);
      std::complex<double> w1(1.0, 0.0), w2;
      std::vector<typename ZZq::value_type> mat(block_size * block_size);
      double sqrt_2 = std::sqrt(2.0);

      if (block_size == 1) {
	mat[0] = fromDouble(1.0);
      } else {
	for (uint64_t i = 0; i < block_size; i++) {
	  w2 = w1;
	  for (uint64_t j = 0; j < block_size/2; j++) {
	    double mat_ij = sqrt_2 * w2.real();
	    mat[i*block_size + j] = fromDouble(mat_ij);

	    double mat_ij1 = sqrt_2 * w2.imag();
	    mat[i*block_size + block_size - (j+1)] =
	      fromDouble(mat_ij1);
	    
	    w2 *= w1;
	  }
	  w1 *= w;
	}
      }

      std::vector<std::vector<typename ZZq::value_type>> data_blocks(m_numProcessors);
      std::vector<uint64_t> ws(m_numProcessors), hs(m_numProcessors);
      for (uint64_t j = 0; j < m_numProcessors; j++) {
	data_blocks[j] = mat;
	ws[j] = block_size;
	hs[j] = map[j]/block_size;
      }
      
      return vmmult_instr_t<ZZq>(data_blocks,
				 ws,
				 hs,
				 OP_MATRIX,
				 RING_RR);
    }
    break;
  case BT_L:
    {
      uint64_t l = it->m_trans.m_transf.l;
      uint64_t block_size = m_n/l;
      std::vector<typename ZZq::value_type> mat(block_size * block_size, 0);
      for (uint64_t i = 0; i < block_size; i++) {
	for (uint64_t j = 0; j <= i; j++) {
	  mat[i*block_size + j] = 1;
	}
      }
      
      std::vector<std::vector<typename ZZq::value_type>> data_blocks(m_numProcessors);
      std::vector<uint64_t> ws(m_numProcessors), hs(m_numProcessors);
      for (uint64_t j = 0; j < m_numProcessors; j++) {
	data_blocks[j] = mat;
	ws[j] = block_size;
	hs[j] = map[j]/block_size;
      }
      
      return vmmult_instr_t<ZZq>(data_blocks,
				 ws,
				 hs,
				 OP_MATRIX,
				 RING_ZZ);
    }
    break;
  case BT_L_INV:
    {
      uint64_t l = it->m_trans.m_transf.l;
      uint64_t block_size = m_n/l;
      std::vector<typename ZZq::value_type> mat(block_size * block_size, 0);
      mat[0] = 1;
      for (uint64_t i = 1; i < block_size; i++) {
	mat[i*block_size + i-1] = -1;
	mat[i*block_size + i] = 1;
      }
      
      std::vector<std::vector<typename ZZq::value_type>> data_blocks(m_numProcessors);
      std::vector<uint64_t> ws(m_numProcessors), hs(m_numProcessors);
      for (uint64_t j = 0; j < m_numProcessors; j++) {
	data_blocks[j] = mat;
	ws[j] = block_size;
	hs[j] = map[j]/block_size;
      }
      
      return vmmult_instr_t<ZZq>(data_blocks,
				 ws,
				 hs,
				 OP_MATRIX,
				 RING_ZZ);
    }
    break;
  }
  abort();
}

static std::string get_udatatype(uint64_t numbytes)
{
  if (numbytes <= 1) return "uint8_t";
  if (numbytes <= 2) return "uint16_t";
  if (numbytes <= 4) return "uint32_t";
  if (numbytes <= 8) return "uint64_t";
  if (numbytes <= 16) return "unsigned __int128";
  abort();
}

static std::string get_sdatatype(uint64_t numbytes)
{
  if (numbytes <= 1) return "int8_t";
  if (numbytes <= 2) return "int16_t";
  if (numbytes <= 4) return "int32_t";
  if (numbytes <= 8) return "int64_t";
  if (numbytes <= 16) return "__int128";
  abort();
}

template<typename ZZq>
void
System<ZZq>::generateHls
(std::string fileName) const
{
  std::ofstream f_h(fileName + ".h");
  std::ofstream f_c(fileName + ".c");
  hls_h_gen_class gen_h_class;
  hls_c_gen_class gen_c_class;
  gen_h_class.set_numProcessors(get_string(m_numProcessors));
  gen_h_class.set_permMemSize(get_string(permMem.size()));
  gen_h_class.set_dataMemSize(get_string(dataMemSize));
  uint64_t cnstMemSize = 0;
  for (uint64_t j = 0; j < m_numProcessors; j++) {
    cnstMemSize = std::max(cnstMemSize, cnstMem[j].size());
  }
  gen_h_class.set_cnstMemSize(get_string(cnstMemSize));
  gen_h_class.set_instMemSize(get_string(instMem.size()));
  gen_h_class.set_addr_t("uint32_t");
  gen_h_class.set_data_t(get_udatatype(sizeof(typename ZZq::value_type)));
  gen_h_class.set_signed_data_t(get_sdatatype(sizeof(typename ZZq::value_type)));
  gen_h_class.set_greater_data_t(get_udatatype(sizeof(typename ZZq::greater_value_type)));
  gen_h_class.set_numbits(get_string(m_numbits));
  gen_h_class.set_numbits2(get_string(m_numbits2));
  gen_h_class.set_fixedpoint(get_string(m_fixedpoint));
  gen_h_class.set_q(get_string(m_q));
  gen_h_class.set_qn(get_string(m_qn));
  gen_c_class.set_permMem(get_string(permMem));
  gen_c_class.set_cnstMem(get_string(cnstMem));
  gen_c_class.set_instMem(get_string(instMem));
  gen_h_class.generate_hls_h(f_h);
  gen_c_class.generate_hls_c(f_c);
  f_h.close();
  f_c.close();
}

template<typename ZZq>
typename ZZq::value_type
System<ZZq>::fromDouble(double x) const
{
  double x1 = x * std::pow(2, m_fixedpoint);

  if (x >= 0) {
    assert(x1 < std::pow(2, m_numbits2-1));
    return static_cast<typename ZZq::value_type>
      (std::round(x1));
  } else {
    assert(-x1 <= std::pow(2, m_numbits2-1));
    return (((typename ZZq::greater_value_type)1) << m_numbits2) - 
      static_cast<typename ZZq::value_type>(std::round(-x1));
  }
}

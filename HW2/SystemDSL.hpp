#ifndef __SYSTEMDSL_HPP__
#define __SYSTEMDSL_HPP__
#include "DSLHardware_gen.hpp"
#include "SystemFact.hpp"

template<typename ZZq, typename ZZ>
struct SystemDSL {
  const SystemFact<ZZq> &m_sf;
  static const uint64_t numTrans = 5;
  uint64_t m_numTests;
  std::vector<call_parameters_t> m_entry_parameters;

  SystemDSL(const SystemFact<ZZq> &sf);
  void generateHls(std::string fileName) const;
};

template<typename ZZq, typename ZZ>
SystemDSL<ZZq,ZZ>::SystemDSL
(const SystemFact<ZZq> &sf)
  : m_sf(sf)
{
  m_entry_parameters.emplace_back(m_sf.get_crt_params());
  m_entry_parameters.emplace_back(m_sf.get_icrt_params());
  m_entry_parameters.emplace_back(m_sf.get_l_params());
  m_entry_parameters.emplace_back(m_sf.get_il_params());
  m_entry_parameters.emplace_back(m_sf.get_crt_b_params());
}

template<typename ZZq, typename ZZ>
void SystemDSL<ZZq,ZZ>::generateHls(std::string fileName) const
{
  std::ofstream f(fileName + ".hpp");
  dsl_hw_gen_class gen_class;
  gen_class.set_entry_parameters(get_string(m_entry_parameters));
  gen_class.generate_dsl_hw(f);
  f.close();
}


#endif

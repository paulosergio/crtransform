#ifndef __HLS_TEST_H__
#define __HLS_TEST_H__

#include <stdint.h>
#include <stddef.h>
#include <stdbool.h>

#define numProcessors @numProcessors@
#define permMemSize @permMemSize@
#define dataMemSize @dataMemSize@
#define cnstMemSize @cnstMemSize@
#define instMemSize @instMemSize@
typedef @addr_t@ my_addr_t;
typedef @data_t@ data_t;
typedef @signed_data_t@ signed_data_t;
typedef @greater_data_t@ greater_data_t;
static const size_t numbits = @numbits@;
static const size_t numbits2 = @numbits2@;
static const size_t fixedpoint = @fixedpoint@;
static const @data_t@ q = @q@;
static const @greater_data_t@ qn = @qn@;

#ifdef TOP_MODULE_PROTOTYPE
struct io_data_t {
  data_t data;
  bool last;
};
void top_module(const struct io_data_t xs[dataMemSize],
		struct io_data_t ys[dataMemSize], size_t blockSize,
		my_addr_t ptr);
#endif

#endif

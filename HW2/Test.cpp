#include "System.hpp"
#include "SystemFact.hpp"
#include "SystemTb.hpp"
#include "SystemDSL.hpp"
#include "NumTh.hpp"
#include "config.hpp"

int main(int argc, char *argv[])
{
  dsl2_set_debug(false);
  system_set_debug(false);
  system_tb_set_debug(false);
  SystemFact<ZZq> s(numProcessorsConfig, qConfig, numbitsConfig, numbits2Config, fixedpointConfig, mConfig);
  s.generateHls("hls_test");
  SystemTb<ZZq,ZZ> tb(s, 5);
  tb.generateHls("hls_tb_test");
  SystemDSL<ZZq,ZZ> dsl(s);
  dsl.generateHls("DSLHardware");
  return 0;
}



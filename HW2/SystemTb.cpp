#include "SystemTb.hpp"
#include <memory>
#include "CRT2.hpp"
#include "DSL2.hpp"
#include "L.hpp"
#include <random>
#include <fstream>
#include "NumTh.hpp"
#include "CRT_B.hpp"

static bool s_debug;

void system_tb_set_debug(bool debug)
{
  s_debug = debug;
}

bool system_tb_get_debug()
{
  return s_debug;
}


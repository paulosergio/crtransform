#include "System.hpp"
#include <algorithm>
#include "Permutations.hpp"
#include <fstream>
#include <list>

static bool s_debug;

void system_set_debug(bool debug)
{
  s_debug = debug;
}

bool system_get_debug()
{
  return s_debug;
}

permutation_t update_perm(permutation_t a,
			  Trans * perm)
{
  //sanity checks
  assert(a.size() == get_dim(perm));
  assert(perm->m_trans.m_transf.r == 1);
  
  uint64_t dim = get_dim(perm);
  uint64_t l   = perm->m_trans.m_transf.l;
  uint64_t n   = get_permutation_n(perm->m_trans.m_transf.f);
  uint64_t m   = dim / l;

  if (s_debug) {
    std::cout << "#### UPDATE_PERM ####\n";
    print_perm(a); std::cout << "\n";
    std::cout << *perm << "\n";
    std::cout << "l = " << l << "\n";
    std::cout << "m = " << m << "\n";
    std::cout << "n = " << n << "\n";
  }
  for (uint64_t i = 0; i < dim; i++) {
    uint64_t t = a[i];

    uint64_t t0 = t / m;
    //t = t0 * m + t1 * (m/n) + t2

    // a[i] = t0 * m + t2 * n + t1;
    uint64_t t1 = (t / n) % (m / n);
    uint64_t t2 = t % n;
    a[i] = t0 * m + t2 * (m/n) + t1;
  }

  if (s_debug) {
    print_perm(a); std::cout << "\n";
  }

  return a;
}

intermediate_repr_t convert_trans(Trans *ops)
{
  Trans *it = ops;
  intermediate_repr_t repr;
  uint64_t dim = get_dim(it);
  permutation_t p(dim);
  bool hasId = false;

  if (s_debug) {
    std::cout << "########\n";
  }
  
  while (get_type(it) != T_ID) {
    for (uint64_t i = 0; i < dim; i++)
      p[i] = i;

    while (get_type(it) != T_ID &&
	   get_type(it->m_trans.m_transf.f) == BT_PERM) {
      p = update_perm(p, it);
      it = it->m_trans.m_transf.prev;
    }

    if (s_debug) {
      print_perm(p); std::cout << "\n";
    }

    repr.push_back(std::make_pair(p, it));

    if (s_debug) {
      std::cout << *it << "\n";
    }

    if (get_type(it) != T_ID) {
      it = it->m_trans.m_transf.prev;
    } else {
      hasId = true;
    }
  }

  if (!hasId) {
    for (uint64_t i = 0; i < dim; i++)
      p[i] = i;
    repr.push_back(std::make_pair(p, it));
  }

  if (s_debug) {
    std::cout << "#instr = " << repr.size() << "\n";
    std::cout << "###############\n";
  }
  
  return repr;
}

std::vector<std::vector<uint64_t>>
data_processor_allocation(const intermediate_repr_t &repr,
			  uint64_t w)
{
  std::vector<std::vector<uint64_t>> res(repr.size(), std::vector<uint64_t>(w));
  uint64_t dim = get_dim(repr[0].second);

  for (uint64_t i = 0; i < repr.size(); i++) {

    if (get_type(repr[i].second) == T_ID) {
      for (uint64_t j = 0; j < w; j++)
	res[i][j] = 0; //defer this to later
    } else {
      uint64_t block_size;
      
      if (get_type(repr[i].second->m_trans.m_transf.f) == BT_TWIDDLE ||
	  get_type(repr[i].second->m_trans.m_transf.f) == BT_TWIDDLE_CRT ||
	  get_type(repr[i].second->m_trans.m_transf.f) == BT_TWIDDLE_INV ||
	  get_type(repr[i].second->m_trans.m_transf.f) == BT_TWIDDLE_CRT_INV) {
	block_size = 1;
      } else {
	block_size = get_dim(repr[i].second->m_trans.m_transf.f);
      }

      if (s_debug) {
	std::cout << "block_size = " << block_size << "\n";
      }
      for (uint64_t j = 0; j < w; j++) {
	res[i][j] = block_size*(((j+1) * (dim/block_size))/w)-
	  block_size*((j * (dim/block_size))/w);
	if (s_debug) {
	  std::cout << "allocation[" << i << "][" << j << "] = "
		    << res[i][j] << "\n";
	}
      }
    }
  }

  return res;
}

void update_data_processor_allocation(const intermediate_repr_t &repr,
				      std::vector<std::vector<uint64_t>> &map,
				      uint64_t kmax,
				      uint64_t w)
{
  uint64_t dim = get_dim(repr[0].second);
  
  for (uint64_t i = 0; i < repr.size(); i++) {
    if (get_type(repr[i].second) == T_ID) {
      uint64_t allocated = 0;
      for (uint64_t j = 0; j < w; j++) {
	if (allocated + kmax <= dim) {
	  map[i][j] = kmax;
	  allocated += kmax;
	} else {
	  map[i][j] = dim - allocated;
	  allocated = dim;
	}
      }
      break;
    }
  }
}

std::pair<uint64_t, uint64_t> get_additive
(std::vector<uint64_t> weights,
 uint64_t i)
{
  uint64_t j = 0;

  while (i >= weights[j]) {
    i -= weights[j];
    j++;
  }

  return std::make_pair(j, i);
}

uint64_t from_additive
(std::vector<uint64_t> weights,
 std::pair<uint64_t, uint64_t> ji)
{
  uint64_t j, i;
  std::tie(j, i) = ji;

  while (j != 0) {
    i += weights[j-1];
    j--;
  }

  return i;
}

permutation_t liftPerm
(std::vector<uint64_t> prev_weights,
 permutation_t perm,
 std::vector<uint64_t> weights,
 uint64_t n1,
 uint64_t kmax)
{
  //sanity check
  assert(prev_weights.size() == weights.size());
  
  permutation_t lifted(n1);
  uint64_t n = perm.size();
  if (s_debug) {
    std::cout << "n = " << n << "\n";
  }
  for (uint64_t i = 0; i < n1; i++)
    lifted[i] = i;

  for (uint64_t i = 0; i < n; i++) {
    auto i_add = get_additive(prev_weights, i);
    auto j_add = get_additive(weights, perm[i]);

    uint64_t h = 0;
    while (lifted[h] != j_add.first * kmax + j_add.second) h++;
    std::swap(lifted[i_add.first * kmax + i_add.second],
	      lifted[h]);
    if (s_debug) {
      std::cout << "p[" << i << "] = " << perm[i] << " -> ";
      std::cout << "lifted-p[" << i_add.first * kmax + i_add.second <<
	"] = " << lifted[i_add.first * kmax + i_add.second] << "\n";
    }
  }

  if (s_debug) {
    print_perm(lifted); std::cout << "\n";
  }

  return lifted;
}

permutation_instr_t::permutation_instr_t(std::vector<uint64_t> read_address,
					 std::vector<uint64_t> write_address,
					 std::vector<uint64_t> interconnect)
  : m_read_address(read_address),
    m_write_address(write_address),
    m_interconnect(interconnect)
{
}

permutation_block_t::permutation_block_t(uint64_t idx,
					 permutation_t reference)
  : m_idx(idx),
    m_reference(reference)
{
}

std::tuple<
  std::vector<std::vector<uint64_t>>, //read_addr
  std::vector<std::vector<uint64_t>>, //write_addr
  std::vector<std::vector<uint64_t>>  //interconnect
  >
schedule(permutation_t p, uint64_t numProcessors, uint64_t blockSize)
{
  std::vector<std::vector<uint64_t>> read_addresses(blockSize, std::vector<uint64_t>(numProcessors));
  std::vector<std::vector<uint64_t>> write_addresses(blockSize, std::vector<uint64_t>(numProcessors));
  std::vector<std::vector<uint64_t>> interconnect(blockSize, std::vector<uint64_t>(numProcessors));
  /* c[k][j] counts how many values will need to be written
     to out_buckets[k] coming from in_buckets[j] */
  matrix_t c(numProcessors, std::vector<uint64_t>(numProcessors, 0));

  // std::cout << "##### schedule #####\n";
  // print_perm(p); std::cout << std::endl;
  for (uint64_t k = 0; k < numProcessors; k++) {
    for (uint64_t l = 0; l < numProcessors; l++) {
	
      for (uint64_t i = l*blockSize; i < (l+1)*blockSize; i++) {
	if (p[i] >= k*blockSize &&
	    p[i] < (k+1)*blockSize) {
	  c[k][l]++;
	}
      }

      // std::cout << c[k][l];
    }
    // std::cout << std::endl;
  }

  /* c is decomposed into a sum of permutation matrices.
     Permutations are represented as an array of values such that
     P = (e_(p[0]), e_(p[1]), ..., e_(p[w-1]))
     where e_i is the ith canonical basis vector */
  auto ps = decompose_semimagic(c);

  // for (const auto &kp : ps) {
  //   uint64_t k;
  //   permutation_t p;
  //   std::tie(k, p) = kp;
  //   std::cout << "k = " << k << std::endl;
  //   std::cout << "p = ";
  //   print_perm(p);
  //   std::cout << std::endl;
  // }   

  std::list<std::pair<uint64_t, uint64_t>> pl;
  for (uint64_t i = 0; i < p.size(); i++)
    pl.push_back(std::make_pair(i, p[i]));

  uint64_t t = 0; //clock cycle
  /* For each of the above permutations of size w we associate values
     from the larger permutation of size n */
  for (const auto &kp : ps) {
    uint64_t k;
    permutation_t p;
    std::tie(k, p) = kp;

    for (uint64_t ki = 0; ki < k; ki++) {
      for (uint64_t l = 0; l < numProcessors; l++) {
	auto it = std::find_if(std::begin(pl),
			       std::end(pl),
			       [&] (std::pair<uint64_t, uint64_t> ij) {
				 return ((ij.first/blockSize) == l) &&
				 (p[l] == (ij.second/blockSize));
			       });
	read_addresses [t][l]    = it->first  % blockSize;
	// std::cout << "read_address[" << t << "][" << l << "] = " <<
	//   read_addresses[t][l] << "\n";
	write_addresses[t][p[l]] = it->second % blockSize;
	// std::cout << "write_addresses[" << t << "][" << p[l] << "] = " <<
	//   write_addresses[t][p[l]] << "\n";
	interconnect   [t][l]    = p[l];
	// std::cout << "interconnect[" << t << "][" << l << "] = " <<
	//   interconnect[t][l] << "\n";
	pl.erase(it);
      }
      t++;
    }
  }

  return std::make_tuple(read_addresses,
			 write_addresses,
			 interconnect);
}

entry_point_t::entry_point_t(uint64_t ptr, uint64_t kmax)
  : m_ptr(ptr)
  , m_kmax(kmax)
{
}


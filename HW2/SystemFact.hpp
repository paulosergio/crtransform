#ifndef __SYSTEMFACT_HPP__
#define __SYSTEMFACT_HPP__
#include "System.hpp"
#include "CPrinter.hpp"
#include <memory>
#include "CRT2.hpp"
#include "L.hpp"
#include "CRT_B.hpp"
#include "NumTh.hpp"


struct call_parameters_t {
  size_t m_dataLength, m_blockSize;
  uint64_t m_ptr;
  bool m_is_fixed_point;
  call_parameters_t(size_t dataLength,
		    size_t blockSize,
		    uint64_t ptr,
		    bool is_fixed_point);
};

template<typename ZZq>
struct SystemFact {
  System<ZZq> s;
  std::vector<call_parameters_t> params;
  
  SystemFact(uint64_t numProcessors,
	     typename ZZq::value_type q,
	     uint64_t numbits,
	     uint64_t numbits2,
	     uint64_t fixedpoint,
	     uint64_t m);

  call_parameters_t get_crt_params() const;
  call_parameters_t get_icrt_params() const;
  call_parameters_t get_l_params() const;
  call_parameters_t get_il_params() const;
  call_parameters_t get_crt_b_params() const;

  void generateHls(std::string fileName) const;
};

template<typename ZZq>
SystemFact<ZZq>::SystemFact
(uint64_t numProcessors,
 typename ZZq::value_type q,
 uint64_t numbits,
 uint64_t numbits2,
 uint64_t fixedpoint,
 uint64_t m)
  : s(numProcessors, q, numbits, numbits2, fixedpoint, m)
{
  uint64_t dataLength = totient(m);
  std::unique_ptr<Trans> crt(get_crt(m));
  s.updateSystem(crt.get());
  params.emplace_back(dataLength,
		      s.entry_points.back().m_kmax,
		      s.entry_points.back().m_ptr,
		      false);
  std::unique_ptr<Trans> icrt(get_icrt(m));
  s.updateSystem(icrt.get());
  params.emplace_back(dataLength,
		      s.entry_points.back().m_kmax,
		      s.entry_points.back().m_ptr,
		      false);
  std::unique_ptr<Trans> l(get_l(m));
  s.updateSystem(l.get());
  params.emplace_back(dataLength,
		      s.entry_points.back().m_kmax,
		      s.entry_points.back().m_ptr,
		      false);
  std::unique_ptr<Trans> il(get_il(m));
  s.updateSystem(il.get());
  params.emplace_back(dataLength,
		      s.entry_points.back().m_kmax,
		      s.entry_points.back().m_ptr,
		      false);
  std::unique_ptr<Trans> crt_b(get_crt_b(m));
  s.updateSystem(crt_b.get());
  params.emplace_back(dataLength,
		      s.entry_points.back().m_kmax,
		      s.entry_points.back().m_ptr,
		      true);
}

template<typename ZZq>
call_parameters_t SystemFact<ZZq>::get_crt_params() const
{
  return params[0];
}

template<typename ZZq>
call_parameters_t SystemFact<ZZq>::get_icrt_params() const
{
  return params[1];
}

template<typename ZZq>
call_parameters_t SystemFact<ZZq>::get_l_params() const
{
  return params[2];
}

template<typename ZZq>
call_parameters_t SystemFact<ZZq>::get_il_params() const
{
  return params[3];
}

template<typename ZZq>
call_parameters_t SystemFact<ZZq>::get_crt_b_params() const
{
  return params[4];
}

template<typename ZZq>
void SystemFact<ZZq>::generateHls(std::string fileName) const
{
  s.generateHls(fileName);
}

template<>
struct GetString<call_parameters_t>
{
  std::string operator()(const call_parameters_t &param)
  {
    std::stringstream ss;
    ss << "{";
    ss << get_string(param.m_dataLength) << ", ";
    ss << get_string(param.m_blockSize) << ", ";
    ss << get_string(param.m_ptr) << ", ";
    ss << (param.m_is_fixed_point ? "true" : "false");
    ss << "}\n";
    return ss.str();
  }
};

#endif

#include "System1.hpp"

System::System(sc_module_name name,
	       const Glue &params)
  : sc_module(name)
  , m_params(params)
  , x_in(m_params.m_w)
  , y_out(m_params.m_w)
  , perm_in(m_params.m_w)
  , perm_out(m_params.m_w)
  , proc_name(m_params.m_w)
  , proc_tokens(m_params.m_w)
  , proc(m_params.m_w)
  , proc_in(m_params.m_w)
  , proc_out(m_params.m_w)
  , proc_operation(m_params.m_w)
  , sync_in(m_params.m_w)
  , sync_out(m_params.m_w)
{
  label = name;
    
  /* Permuter */
  perm_tokens = tokenise_perms(m_params.m_lifted_perms);
  perm = new Permuter("system_permuter",
		      m_params.m_n1,
		      m_params.m_w,
		      std::get<1>(perm_tokens));
  for (uint64_t i = 0; i < m_params.m_w; i++) {
    perm->x_in[i](perm_in[i]);
    perm->y_out[i](perm_out[i]);
  }
  perm->start(perm_start);
  perm->permutation(perm_index);
  perm->clk(clk);

  /* Processors */
  for (uint64_t i = 0; i < m_params.m_w; i++) {
    proc_tokens[i] = tokenise(m_params.m_op_scheduling[i]);
    proc_name[i] = std::string("system_proc") +
      std::to_string(i);
    proc[i] = new Butterfly(proc_name[i].c_str(),
			    m_params.m_q,
			    std::get<1>(proc_tokens[i]),
			    std::get<2>(proc_tokens[i]),
			    std::get<3>(proc_tokens[i]),
			    std::get<4>(proc_tokens[i]));
      
    proc[i]->x_in(proc_in[i]);
    proc[i]->y_out(proc_out[i]);
    proc[i]->operation(proc_operation[i]);
    proc[i]->start(proc_start);
    proc[i]->clk(clk);
  }

  /* Delayer & System */
  std::vector<std::vector<uint64_t>> schedule
    (m_params.m_exec_time.size(),
     std::vector<uint64_t>(m_params.m_w));
  total_exec_time = 0;
  for (uint64_t i = 0; i < m_params.m_exec_time.size(); i++) {
    uint64_t max_exec_time_i = 0;
    uint64_t min_exec_time_i = m_params.m_exec_time[i][0];

    for (auto exec_time : m_params.m_exec_time[i]) {
      max_exec_time_i = std::max(max_exec_time_i,
				 exec_time);
      min_exec_time_i = std::min(min_exec_time_i,
				 exec_time);
    }

    for (uint64_t j = 0; j < m_params.m_w; j++)
      schedule[i][j] = max_exec_time_i - m_params.m_exec_time[i][j];

    weights.push_back(2*m_params.m_n1/m_params.m_w + max_exec_time_i);
    std::cout << label << ": weights[" << i << "] = " << weights.back() << "\n";
    total_exec_time += weights.back();
    min_ti.push_back(min_exec_time_i);
  }
  sync = new Delayer("system_sync", schedule);
  for (uint64_t i = 0; i < m_params.m_w; i++) {
    sync->x_in[i](sync_in[i]);
    sync->y_out[i](sync_out[i]);
  }
  sync->cycle(sync_cycle);
  sync->start(sync_start);
  sync->clk(clk);

  /** Senstive list **/
  SC_THREAD(control);
  sensitive << clk.pos();
  
  SC_METHOD(process);
  sensitive << ctr
	    << start;
  for (uint64_t j = 0; j < m_params.m_w; j++) {
    sensitive << x_in[j];
    sensitive << sync_out[j];
    sensitive << proc_out[j];
    sensitive << perm_out[j];
  }
}

void System::control()
{
  while (true) {
    wait();

    if (start.read()) {
      ctr.write(1);
    } else {
      ctr.write(ctr.read() + 1);
    }
  }
}

void System::process()
{
  perm_start.write(false); //defaults
  proc_start.write(false);
  sync_start.write(false);

  if ((ctr.read() < total_exec_time)
      || start.read()) {
    uint64_t ctr0, ctr1;
    std::tie(ctr0, ctr1) = get_additive(weights,
					(start.read() ?
					 0 : ctr.read()));
    // std::cout << label << " - " << sc_time_stamp() << ":\n";
    // std::cout << "ctr = " << ctr << "\n";
    // std::cout << "ctr1 = " << ctr1 << "\n";
    // std::cout << "ctr0 = " << ctr0 << "\n";
    // std::cout << "min_ti = " << min_ti[ctr0] << "\n";
    // std::cout << "weight = " << weights[ctr0] << "\n";

    if (ctr1 < m_params.m_n1/m_params.m_w) {
      // if (ctr0 == 0) std::cout << "reading from x_in\n";
      // else std::cout << "reading from sync_out\n";
      for (uint64_t j = 0; j < m_params.m_w; j++) {
	perm_in[j].write(ctr0 == 0 ? x_in[j] :
			 sync_out[j]);
      }
    }

    if (ctr1 == 0) {
      perm_start.write(true);
      perm_index.write(std::get<0>(perm_tokens)[ctr0]);
    }

    if (ctr1 == 2*m_params.m_n1/m_params.m_w) {
      proc_start.write(true);

      for (uint64_t j = 0; j < m_params.m_w; j++) {
	proc_operation[j].write(std::get<0>(proc_tokens[j])[ctr0]);
      }
    }

    if (ctr1 == 2*m_params.m_n1/m_params.m_w + min_ti[ctr0]) {
      sync_start.write(true);
      sync_cycle.write(ctr0);
    } else if ((ctr0 > 0) &&
	       (2*m_params.m_n1/m_params.m_w + min_ti[ctr0-1] == weights[ctr0-1]) &&
	       (ctr1 == 0)) {
      sync_start.write(true);
      sync_cycle.write(ctr0-1);
    }

  } else {
    if ((2*m_params.m_n1/m_params.m_w + min_ti.back() == weights.back()) &&
	(ctr.read() == total_exec_time)) {
      sync_start.write(true);
      sync_cycle.write(weights.size() - 1);
    }
    // std::cout << sc_time_stamp() << ": Finished computation\n";
  }

  for (uint64_t j = 0; j < m_params.m_w; j++) {
    proc_in[j].write(perm_out[j].read());
    sync_in[j].write(proc_out[j].read());
    y_out[j].write(sync_out[j].read());
  }
}

System::~System()
{
  delete perm;
  delete sync;
  for (auto p : proc)
    delete p;
}

#define CATCH_CONFIG_MAIN
#include "catch.hpp"
#include "Permutations.hpp"
#include <list>
#include <random>

permutation_t random_permutation(uint64_t w)
{
  permutation_t res(w);
  for (uint64_t i = 0; i < w; i++) res[i] = i;
  std::random_shuffle(std::begin(res),
		      std::end(res));
  return res;
}

TEST_CASE("Decomposition of semimagic matrix", "[semimagic]")
{
  const uint64_t numtests = 100;
  const uint64_t k        = 10;
  const uint64_t w        = 5;

  for (uint64_t i = 0; i < numtests; i++) {
    matrix_t semimagic(w,
		       std::vector<uint64_t>(w, 0));
    for (uint64_t j = 0; j < k; j++) {
      auto p = random_permutation(w);
      for (uint64_t h = 0; h < w; h++) {
	semimagic[p[h]][h]++;
      }
    }

    auto perms = decompose_semimagic(semimagic);
    matrix_t semimagic2(w,
			std::vector<uint64_t>(w, 0));
    for (const auto &p1 : perms) {
      permutation_t p;
      uint64_t a;
      std::tie(a, p) = p1;

      for (uint64_t h = 0; h < w; h++) {
	semimagic2[p[h]][h] += a;
      }
    }

    REQUIRE(std::equal(std::begin(semimagic),
		       std::end(semimagic),
		       std::begin(semimagic2),
		       [] (const std::vector<uint64_t> &a,
			   const std::vector<uint64_t> &b) {
			 return std::equal(std::begin(a),
					   std::end(a),
					   std::begin(b));
		       }));
  }
}

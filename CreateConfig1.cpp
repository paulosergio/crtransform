#include <iostream>
#include "System1.hpp"
#include "CRT2.hpp"
#include "NumTh.hpp"
#include <NTL/ZZ.h>
#include <memory>
#include <string>
#include <sstream>
#include <random>
#include <algorithm>
#include <fstream>
#include <cassert>

template<typename T>
std::ostream &operator<<(std::ostream &os, const std::vector<T> v)
{
  os << "(";
  for (uint64_t i = 1; i < v.size(); i++) {
    os << v[i-1] << ", ";
  }
  if (v.size() > 0) {
    if (v.size() == 1) os << "0 => ";
    os << v.back();
  }
  os << ")";

  return os;
}

std::string create_definition(const std::string &label,
			      const std::string &def)
{
  return std::string("define(`") + label +
    std::string("', `") + def +
    std::string("')\n");
}

std::string create_definition(const std::string &label,
			      const uint64_t &def)
{
  return create_definition(label, std::to_string(def));
}

template<typename T>
std::string create_definition(const std::string &label,
			      const std::vector<T> &def)
{
  std::stringstream ss;
  ss << def;
  return create_definition(label, ss.str());
}

std::string create_config(System *sys, uint64_t numbits)
{
  std::string res("");
  const uint64_t numtests = 5;
  uint64_t w = sys->m_params.m_w;
  uint64_t n = sys->m_params.m_n1;
  uint64_t norig = sys->m_params.m_n;
  uint64_t q = sys->m_params.m_q;
  uint64_t qn = (static_cast<uint128_t>(1)<<(2*numbits+1)) / q;
  std::default_random_engine gen;

  /* generic stuff */
  res += create_definition("w", w);
  res += create_definition("n", n);
  res += create_definition("q", q);
  res += create_definition("qn", qn);
  res += create_definition("qdata_width", numbits);
  res += create_definition("norig", norig);

  /* permuter */
  res += create_definition("numperms",
			   std::to_string(sys->perm->m_read_addresses.size()));
  res += create_definition("perm_precomputed_read_addresses",
			   sys->perm->m_read_addresses);
  res += create_definition("perm_precomputed_write_addresses",
			   sys->perm->m_write_addresses);
  res += create_definition("perm_precomputed_interconnections",
			   sys->perm->m_interconnect);

  {
    res += create_definition("perm_numtests", numtests);
    std::uniform_int_distribution<uint64_t> dist(0, q-1);
    std::uniform_int_distribution<uint64_t> distP(0, sys->m_params.m_lifted_perms.size()-1);
    
    std::vector<std::vector<std::vector<uint64_t>>> xs(numtests, std::vector<std::vector<uint64_t>>(n/w, std::vector<uint64_t>(w)));
    std::vector<std::vector<std::vector<uint64_t>>> ys(numtests, std::vector<std::vector<uint64_t>>(n/w, std::vector<uint64_t>(w)));
    std::vector<uint64_t> ps(numtests);

    for (uint64_t i = 0; i < numtests; i++) {
      uint64_t pindex = distP(gen);
      ps[i] = std::get<0>(sys->perm_tokens)[pindex];
      std::vector<uint64_t> x(n), y(n);
      for (uint64_t j = 0; j < n; j++) {
	x[j] = dist(gen);
	y[sys->m_params.m_lifted_perms[pindex][j]] = x[j];
      }
      for (uint64_t j = 0; j < n/w; j++) {
	for (uint64_t k = 0; k < w; k++) {
	  xs[i][j][k] = x[k*n/w + j];
	  ys[i][j][k] = y[k*n/w + j];
	}
      }
    }

    res += create_definition("perm_precomputed_x",
			     xs);
    res += create_definition("perm_precomputed_y",
			     ys);
    res += create_definition("perm_precomputed_p",
			     ps);
  }

  /* processing elements */
  for (uint64_t i = 0; i < w; i++) {
    std::cout << "hs = " << sys->proc[i]->m_hs << "\n";
    res += create_definition("pe_" + std::to_string(i+1) + "_numops",
			     sys->proc[i]->m_wss.size());
    uint64_t max_m = *std::max_element(std::begin(sys->proc[i]->m_ms),
				       std::end(sys->proc[i]->m_ms));
    uint64_t max_h = *std::max_element(std::begin(sys->proc[i]->m_hs),
				       std::end(sys->proc[i]->m_hs));
    uint64_t max_mh = sys->proc[i]->m_max_mh;
    res += create_definition("pe_" + std::to_string(i+1) + "_max_m",
			     max_m);
    res += create_definition("pe_" + std::to_string(i+1) + "_max_h",
			     max_h);
    res += create_definition("pe_" + std::to_string(i+1) + "_max_hm",
			     max_mh);
    res += create_definition("pe_" + std::to_string(i+1) + "_precomputed_hs",
			     sys->proc[i]->m_hs);
    res += create_definition("pe_" + std::to_string(i+1) + "_precomputed_ms",
			     sys->proc[i]->m_ms);
    res += create_definition("pe_" + std::to_string(i+1) + "_precomputed_linears",
			     sys->proc[i]->m_linears);

    for (uint64_t j = 0; j < sys->proc[i]->m_wss.size(); j++) {
      res += create_definition("pe_" + std::to_string(i+1) + "_precomputed_w_" + std::to_string(j+1) + "_size", sys->proc[i]->m_wss[j].size());
      res += create_definition("pe_" + std::to_string(i+1) + "_precomputed_w_" + std::to_string(j+1), sys->proc[i]->m_wss[j]);
    }
  }

  {
    res += create_definition("pe_numtests", numtests);
    std::vector<uint64_t> ops(numtests);
    std::uniform_int_distribution<uint64_t> distOp(0, sys->proc[0]->m_wss.size()-1);
    std::uniform_int_distribution<uint64_t> dist(0, q-1);

    for (uint64_t i = 0; i < numtests; i++) {
      do {
	ops[i] = distOp(gen);
      } while (sys->proc[0]->m_ms[ops[i]] == 0 ||
	       sys->proc[0]->m_hs[ops[i]] == 0);
      uint64_t m = sys->proc[0]->m_ms[ops[i]];
      uint64_t h = sys->proc[0]->m_hs[ops[i]];
      bool linear = sys->proc[0]->m_linears[ops[i]];
      auto ws = sys->proc[0]->m_wss[ops[i]];
      uint64_t xsize = h*m;
      uint64_t exectime = (linear ? h*m : h*m*m);
      res += create_definition("pe_precomputed_" +
			       std::to_string(i+1) +
			       "_size",
			       xsize);
      res += create_definition("pe_precomputed_" +
			       std::to_string(i+1) +
			       "_exectime",
			       exectime);

      std::vector<uint64_t> x(xsize), y(xsize, 0);
      for (uint64_t j = 0; j < xsize; j++) {
	x[j] = dist(gen);
      }
      res += create_definition("pe_precomputed_x_" +
			       std::to_string(i+1),
			       x);
      for (uint64_t j = 0; j < h; j++) {
	std::vector<uint64_t> xslice(std::begin(x) + j*m,
				     std::begin(x) + (j+1)*m);
	std::vector<uint64_t> yslice(m, 0);

	if (linear) {
	  for (uint64_t h = 0; h < xslice.size(); h++) {
	    yslice[h] = ((uint128_t)xslice[h] * ws[h]) % q;
	  }
	} else {
	  for (uint64_t h1 = 0; h1 < xslice.size(); h1++) {
	    for (uint64_t h2 = 0; h2 < xslice.size(); h2++) {
	      yslice[h1] = ((uint128_t)xslice[h2] * ws[h1 * m + h2] + yslice[h1]) % q;
	    }
	  }
	}

	std::copy(std::begin(yslice),
		  std::end(yslice),
		  std::begin(y) + j*m);
      }

      res += create_definition("pe_precomputed_y_" +
			       std::to_string(i+1),
			       y);
    }

    res += create_definition("pe_precomputed_ops", ops);
  }

  /* delayer */
  uint64_t max_delay = 0;
  for (uint64_t i = 0; i < sys->sync->m_schedule.size(); i++) {
    for (uint64_t j = 0; j < sys->sync->m_schedule[i].size(); j++) {
      max_delay = std::max(max_delay, sys->sync->m_schedule[i][j]);
    }
  }
  res += create_definition("delay_max_delay", max_delay);
  for (uint64_t i = 0; i < w; i++) {
    res += create_definition("delay_depth_" + std::to_string(i+1),
			     sys->sync->fifos[i]->regs.size());
  }
  res += create_definition("delay_cycles", sys->sync->m_schedule.size());
  res += create_definition("delay_precomputed_schedule",
			   sys->sync->m_schedule);

  res += create_definition("system_total_exec_time", sys->total_exec_time);
  res += create_definition("system_weights_size", sys->weights.size());
  res += create_definition("system_weights", sys->weights);
  res += create_definition("system_permutation_tokens", std::get<0>(sys->perm_tokens));
  for (uint64_t i = 0; i < w; i++) {
    res += create_definition("system_pe_" + std::to_string(i+1) +
			     "_tokens",
			     std::get<0>(sys->proc_tokens[i]));
  }
  res += create_definition("system_min_ti", sys->min_ti);

  /* system */
  {
    res += create_definition("system_numtests", numtests);
    std::vector<std::vector<uint64_t>> xs(numtests, std::vector<uint64_t>(norig, 0));
    std::vector<std::vector<uint64_t>> ys(numtests, std::vector<uint64_t>(norig, 0));
    std::uniform_int_distribution<uint64_t> dist(0, q-1);
    
    for (uint64_t i = 0; i < numtests; i++) {
      for (uint64_t j = 0; j < norig; j++) {
	xs[i][j] = dist(gen);
      }
      ys[i] = eval(sys->m_params.m_ops, xs[i], q, sys->m_params.m_m);
    }

    res += create_definition("system_precomputed_xs", xs);
    res += create_definition("system_precomputed_ys", ys);
    res += create_definition("system_final_weights", sys->m_params.m_data_allocation.back());
  }

  /* barrett */
  {
    const uint64_t barrett_numtests = 10000;
    std::uniform_int_distribution<uint64_t> dist(0, q-1);
    res += create_definition("barrett_numtests", barrett_numtests);
    
    std::vector<uint64_t> as(barrett_numtests);
    std::vector<uint64_t> bs(barrett_numtests);
    std::vector<uint64_t> cs(barrett_numtests);
    std::vector<uint64_t> zs(barrett_numtests);

    for (uint64_t i = 0; i < barrett_numtests; i++) {
      as[i] = dist(gen);
      bs[i] = dist(gen);
      cs[i] = dist(gen);
      zs[i] = (((uint128_t)as[i]) * bs[i] + cs[i]) % q;
    }

    res += create_definition("barrett_precomputed_as", as);
    res += create_definition("barrett_precomputed_bs", bs);
    res += create_definition("barrett_precomputed_cs", cs);
    res += create_definition("barrett_precomputed_zs", zs);
  }

  return res;
}

int main(int argc, char *argv[])
{
  if (argc != 4) {
    std::cout << "usage: "
	      << argv[0]
	      << " [m] [w] [numbits]\n";
    return 1;
  }

  uint64_t m = std::stoi(argv[1]);
  uint64_t numbits = std::stoi(argv[3]);
  assert(numbits <= 29);
  uint64_t q = (static_cast<uint64_t>(1)<<(numbits-1)) +
    (m - ((static_cast<uint64_t>(1)<<(numbits-1)) % m)) +
    1;
  while (!NTL::ProbPrime(q))
    q += m;

  uint64_t w = std::stoi(argv[2]);

  std::unique_ptr<Trans> ops(get_crt(m));

  Glue params(w, ops.get(), m, q);
  System sys("system", params);

  auto s = create_config(&sys, numbits);

  std::ofstream config;
  config.open("config.m4");
  config << s;
  config.close();

  return 0;
}

#include "System1.hpp"
#include <random>
#include <iomanip>
#include "CRT2.hpp"

struct SystemTestbench : sc_module
{
  const uint64_t w = 4;
  const uint64_t m = 9;
  const uint64_t n = 6;
  const uint64_t q = 19;
  
  Trans *t;
  Glue params;
  System uut;
  sc_in<bool> clk;
  std::vector<sc_signal<uint64_t>> uut_x_in;
  std::vector<sc_signal<uint64_t>> uut_y_out;
  sc_signal<bool> uut_start;

  std::default_random_engine gen;
  std::uniform_int_distribution<uint64_t> dist;

  void process()
  {
    wait();
    uut_start.write(true);

    std::cout << std::setw(10) << "Time ";
    std::cout << std::setw(5) << "X ";
    std::cout << std::endl;

    for (uint64_t i = 0; i < uut.perm->m_bsize; i++) {
      std::cout << std::setw(10) << sc_time_stamp();
      for (uint64_t j = 0; j < w; j++) {
	uint64_t xij = dist(gen);
	std::cout << std::setw(5) << xij;
	uut_x_in[j].write(xij);
      }
      wait();
      uut_start.write(false);
      std::cout << std::endl;
    }

    std::cout << "total_exec_time = " <<
      uut.total_exec_time << "\n";
    for (uint64_t i = 0; i < uut.total_exec_time - uut.perm->m_bsize; i++)
      wait();

    std::cout << std::setw(10) << "Time ";
    std::cout << std::setw(5) << "Y ";
    std::cout << std::endl;
    
    for (uint64_t i = 0; i < uut.perm->m_bsize; i++) {
      wait();
      std::cout << std::setw(10) << sc_time_stamp();
      for (uint64_t j = 0; j < w; j++) {
	std::cout << std::setw(5) << uut_y_out[j].read();
      }
      std::cout << std::endl;
    }

    sc_stop();
  }

  SystemTestbench(sc_module_name name)
    : sc_module(name)
    , t(get_crt(m))
    , params(w, t, m, q)
    , uut("uut", params)
    , uut_x_in(w)
    , uut_y_out(w)
    , dist(0, q-1)
  {
    uut.clk(clk);
    uut.start(uut_start);
    for (uint64_t j = 0; j < w; j++) {
      uut.x_in[j](uut_x_in[j]);
      uut.y_out[j](uut_y_out[j]);
    }

    SC_THREAD(process);
    sensitive << clk.pos();
  }

  SC_HAS_PROCESS(SystemTestbench);
};

int sc_main(int argc, char *argv[])
{
  sc_clock TestClk("TestClock", 10, SC_NS, 0.5);
  SystemTestbench t("PermuterTestbench");
  t.clk(TestClk);

  sc_trace_file *Tf;
  Tf = sc_create_vcd_trace_file("traces");
  Tf->set_time_unit(1, sc_time_unit::SC_NS);

  std::vector<std::string> perm_in_names(t.w);
  for (uint64_t i = 0; i < t.w; i++) {
    perm_in_names[i] = std::string("perm_in_") + std::to_string(i);
    sc_trace(Tf, t.uut.perm_in[i], perm_in_names[i].c_str());
  }
  sc_trace(Tf, t.uut.perm_start, "perm_start");
  sc_trace(Tf, t.uut.proc_start, "proc_start");
  sc_trace(Tf, t.uut.sync_start, "sync_start");
  std::vector<std::string> perm_out_names(t.w);
  for (uint64_t i = 0; i < t.w; i++) {
    perm_out_names[i] = std::string("perm_out_") + std::to_string(i);
    sc_trace(Tf, t.uut.perm_out[i], perm_out_names[i].c_str());
  }
  sc_trace(Tf, t.uut.perm->bucket_ys[3], "bucket_ys_3_in");
  sc_trace(Tf, t.uut.perm->write_out, "write_out_buckets");
  sc_trace(Tf, t.uut.perm->addr_out[3], "bucket_3_addr");
  std::vector<std::string> proc_out_names(t.w);
  for (uint64_t i = 0; i < t.w; i++) {
    proc_out_names[i] = std::string("proc_out_") + std::to_string(i);
    sc_trace(Tf, t.uut.proc_out[i], proc_out_names[i].c_str());
  }
  
  sc_start();
  sc_close_vcd_trace_file(Tf);
  return 0;
}

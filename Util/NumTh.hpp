#pragma once

#include <vector>
#include <utility>
#include <tuple>
#include <cstdint>
#undef str
#include <iostream>
#undef str
#include <complex>
#include <cassert>

#ifndef M_PI
#define M_PI           3.14159265358979323846  /* pi */
#endif

#ifdef __SIZEOF_INT128__
typedef unsigned __int128 uint128_t;
typedef __int128 int128_t;
#endif

uint64_t pown(uint64_t base, uint64_t exp);
template<typename ZZq>
typename ZZq::value_type
pown (typename ZZq::value_type base,
      typename ZZq::value_type exp,
      typename ZZq::value_type mod);
template<typename ZZq>
typename ZZq::value_type
modinv (typename ZZq::value_type a,
	typename ZZq::value_type mod); // for mod prime
uint64_t gcd(uint64_t a, uint64_t b);
uint64_t totient(uint64_t m);
uint64_t rad(uint64_t m);
bool witness(uint64_t n, uint64_t s, uint64_t d, uint64_t a);
bool is_prime_mr(uint64_t n);
std::vector<std::pair<uint64_t, uint64_t>> factor(uint64_t m);
template<typename ZZq>
typename ZZq::value_type
findElemOfOrder (uint64_t m,
		 typename ZZq::value_type q,
		 std::vector<std::pair<uint64_t, uint64_t>> m_factors);
std::complex<double> findElemOfOrderCC(uint64_t m);

#ifdef __SIZEOF_INT128__
std::ostream &operator<<(std::ostream &os, uint128_t i);
#endif

struct ZZq16_t {
  using value_type = uint16_t;
  using greater_value_type = uint32_t;
};

struct ZZq32_t {
  using value_type = uint32_t;
  using greater_value_type = uint64_t;
};

#ifdef __SIZEOF_INT128__
struct ZZq64_t {
  using value_type = uint64_t;
  using greater_value_type = uint128_t;
};
#endif

struct ZZ16_t {
  using value_type = int16_t;
  using greater_value_type = int32_t;
};

struct ZZ32_t {
  using value_type = int32_t;
  using greater_value_type = int64_t;
};

#ifdef __SIZEOF_INT128__
struct ZZ64_t {
  using value_type = int64_t;
  using greater_value_type = int128_t;
};
#endif

template<typename ZZq>
typename ZZq::value_type
findElemOfOrder (uint64_t m,
		 typename ZZq::value_type q,
		 std::vector<std::pair<uint64_t, uint64_t>> m_factors)
{
  assert(q % m == 1);
  //mth root of unit
  bool isRoot = false;
  typename ZZq::value_type root = 1;
  
  do {
    root++;
    if (pown<ZZq>(root, m, q) == 1) {
      isRoot = true;
      for (const auto &f : m_factors) {
	typename ZZq::value_type exp  = m / f.first;
	typename ZZq::value_type test = pown<ZZq>(root, exp, q);
	
	if (test == 1) {
	  isRoot = false;
	  break;
	}
      }
    }
  } while (!isRoot);

  return root;
}

template<typename ZZq>
typename ZZq::value_type
modinv (typename ZZq::value_type a,
	typename ZZq::value_type mod) // for mod prime
{
  return pown<ZZq>(a, mod-2, mod);
}

template<typename ZZq>
typename ZZq::value_type
pown (typename ZZq::value_type base,
      typename ZZq::value_type exp,
      typename ZZq::value_type mod)
{
  typename ZZq::value_type r = 1, x = base;

  while(exp != 0) {
    if ((exp & 1) == 1)
      r = ((typename ZZq::greater_value_type)r*x) % mod;
    x = ((typename ZZq::greater_value_type)x*x) % mod;

    exp >>= 1;
  }

  return r;
}


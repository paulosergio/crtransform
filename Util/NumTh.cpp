#include "NumTh.hpp"
#include <map>
#include <cassert>
#undef str
#include <iomanip>

// n-1 = 2^s * d with d odd by factoring powers of 2 from n-1
bool witness(uint64_t n, uint64_t s, uint64_t d, uint64_t a)
{
  uint64_t x = pown<ZZq32_t>(a, d, n);
  uint64_t y;
 
  while (s) {
    y = (x * x) % n;
    if (y == 1 && x != 1 && x != n-1)
      return false;
    x = y;
    --s;
  }
  if (y != 1)
    return false;
  return true;
}
 
/*
 * if n < 1,373,653, it is enough to test a = 2 and 3;
 * if n < 9,080,191, it is enough to test a = 31 and 73;
 * if n < 4,759,123,141, it is enough to test a = 2, 7, and 61;
 * if n < 1,122,004,669,633, it is enough to test a = 2, 13, 23, and 1662803;
 * if n < 2,152,302,898,747, it is enough to test a = 2, 3, 5, 7, and 11;
 * if n < 3,474,749,660,383, it is enough to test a = 2, 3, 5, 7, 11, and 13;
 * if n < 341,550,071,728,321, it is enough to test a = 2, 3, 5, 7, 11, 13, and 17.
 */
 
bool is_prime_mr(uint64_t n)
{
  if (((!(n & 1)) && n != 2 ) || (n < 2) || (n % 3 == 0 && n != 3))
    return false;
  if (n <= 3)
    return true;
 
  uint64_t d = n / 2;
  uint64_t s = 1;
  while (!(d & 1)) {
    d /= 2;
    ++s;
  }
 
  if (n < 1373653)
    return witness(n, s, d, 2) && witness(n, s, d, 3);
  if (n < 9080191)
    return witness(n, s, d, 31) && witness(n, s, d, 73);
  if (n < 4759123141)
    return witness(n, s, d, 2) && witness(n, s, d, 7) && witness(n, s, d, 61);
  if (n < 1122004669633)
    return witness(n, s, d, 2) && witness(n, s, d, 13) && witness(n, s, d, 23) && witness(n, s, d, 1662803);
  if (n < 2152302898747)
    return witness(n, s, d, 2) && witness(n, s, d, 3) && witness(n, s, d, 5) && witness(n, s, d, 7) && witness(n, s, d, 11);
  if (n < 3474749660383)
    return witness(n, s, d, 2) && witness(n, s, d, 3) && witness(n, s, d, 5) && witness(n, s, d, 7) && witness(n, s, d, 11) && witness(n, s, d, 13);
  return witness(n, s, d, 2) && witness(n, s, d, 3) && witness(n, s, d, 5) && witness(n, s, d, 7) && witness(n, s, d, 11) && witness(n, s, d, 13) && witness(n, s, d, 17);
}

std::vector<std::pair<uint64_t, uint64_t>> factor(uint64_t m)
{
  std::vector<std::pair<uint64_t, uint64_t>> fs;
  uint64_t curr_prime = 2;

  while (!is_prime_mr(m) && m != 1)
    {
      uint64_t k = 0;
      
      while (m % curr_prime == 0)
	{
	  m /= curr_prime;
	  k++;
	}

      if (k != 0)
	fs.push_back(std::make_pair(curr_prime, k));

      if (curr_prime == 2) curr_prime = 3;
      else curr_prime += 2;
      
      while (!is_prime_mr(curr_prime))
	curr_prime += 2;
    }

  if (m != 1)
    fs.push_back(std::make_pair(m, 1));

  return fs;
}

uint64_t gcd(uint64_t a, uint64_t b)
{
  if (b == 0) return a;
  return gcd(b, a % b);
}

uint64_t totient(uint64_t m)
{
  if (m < 2)
    return 0;

  if (m == 2 || is_prime_mr(m))
    return m-1;

  if ((m & 1) == 0) {
    m >>= 1;
    return !(m & 1) ? totient(m) << 1 : totient(m);
  }

  uint64_t p = 3;
  while (m % p) {
	p += 2;

    while (!is_prime_mr(p))
    	p += 2;
  }

  uint64_t o = m / p;
  uint64_t d = gcd(p, o);
  return d == 1 ?
	totient(p)*totient(o) :
	totient(p)*totient(o)*d / totient(d);
}

uint64_t pown(uint64_t base, uint64_t exp)
{
  uint64_t r = 1, x = base;

  while(exp != 0) {
    if ((exp & 1) == 1)
      r *= x;
    x *= x;

    exp >>= 1;
  }

  return r;
}

#ifdef __SIZEOF_INT128__
std::ostream &operator<<(std::ostream &os, uint128_t i)
{
  std::vector<uint64_t> is;
  const uint64_t radix = pown(10, 18);
  if (i == 0) {
    os << 0;
    return os;
  }
  
  while (i != 0) {
    is.push_back(i % radix);
    i /= radix;
  }
  os << is.back();
  for (auto ii = is.rbegin()+1;
       ii != is.rend();
       ii++) {
    os << std::setfill('0') << std::setw(18) << *ii;
  }
  
  return os;
}
#endif

std::complex<double> findElemOfOrderCC(uint64_t m)
{
  std::complex<double> im(0.0, 1.0);
  return std::exp(2 * M_PI * im / static_cast<double>(m));
}

uint64_t rad(uint64_t m)
{
  auto fs = factor(m);
  uint64_t ret = 1;

  for (auto f : fs)
    ret *= f.first;

  return ret;
}

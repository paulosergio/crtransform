#include <iostream>
#include <vector>

template<typename T>
std::ostream &operator<<(std::ostream &os,
			 const std::vector<T> &v)
{
  os << "(";

  for (uint64_t i = 0; i + 1 < v.size(); i++) {
    os << v[i] << ", ";
  }

  if (v.size() != 0) {
    os << v.back();
  }

  os << ")";

  return os;
}

#include "Glue2.hpp"

permutation_t update_perm(permutation_t a,
			  Trans * perm)
{
  //sanity checks
  assert(a.size() == get_dim(perm));
  assert(perm->m_trans.m_transf.r == 1);
  
  uint64_t dim = get_dim(perm);
  uint64_t l   = perm->m_trans.m_transf.l;
  uint64_t n   = get_permutation_n(perm->m_trans.m_transf.f);
  uint64_t m   = dim / l;

  std::cout << "#### UPDATE_PERM ####\n";
  print_perm(a); std::cout << "\n";
  std::cout << *perm << "\n";
  std::cout << "l = " << l << "\n";
  std::cout << "m = " << m << "\n";
  std::cout << "n = " << n << "\n";
  for (uint64_t i = 0; i < dim; i++) {
    uint64_t t = a[i];

    uint64_t t0 = t / m;
    // uint64_t t1 = (t / (m/n)) % n;
    // uint64_t t2 = t % (m/n);
    //t = t0 * m + t1 * (m/n) + t2

    // a[i] = t0 * m + t2 * n + t1;
    uint64_t t1 = (t / n) % (m / n);
    uint64_t t2 = t % n;
    a[i] = t0 * m + t2 * (m/n) + t1;
  }
  print_perm(a); std::cout << "\n";  

  return a;
}

intermediate_repr_t convert_trans(Trans *ops)
{
  Trans *it = ops;
  intermediate_repr_t repr;
  uint64_t dim = get_dim(it);
  permutation_t p(dim);

  std::cout << "########\n";
  
  while (get_type(it) != T_ID) {
    for (uint64_t i = 0; i < dim; i++)
      p[i] = i;

    while (get_type(it) != T_ID &&
	   get_type(it->m_trans.m_transf.f) == BT_PERM) {
      p = update_perm(p, it);
      it = it->m_trans.m_transf.prev;
    }

    print_perm(p); std::cout << "\n";

    repr.push_back(std::make_pair(p, it));

    std::cout << *it << "\n";

    if (get_type(it) != T_ID)
      it = it->m_trans.m_transf.prev;      
  }

  std::cout << "#instr = " << repr.size() << "\n";
  std::cout << "###############\n";
  
  return repr;
}

std::vector<std::vector<uint64_t>>
data_processor_allocation(const intermediate_repr_t &repr,
			  uint64_t w)
{
  std::vector<std::vector<uint64_t>> res(repr.size(), std::vector<uint64_t>(w));
  uint64_t dim = get_dim(repr[0].second);

  for (uint64_t i = 0; i < repr.size(); i++) {
    uint64_t block_size;

    if (get_type(repr[i].second) == T_ID ||
	get_type(repr[i].second->m_trans.m_transf.f) == BT_TWIDDLE ||
	get_type(repr[i].second->m_trans.m_transf.f) == BT_TWIDDLE_CRT ||
	get_type(repr[i].second->m_trans.m_transf.f) == BT_TWIDDLE_INV ||
	get_type(repr[i].second->m_trans.m_transf.f) == BT_TWIDDLE_CRT_INV) {
      block_size = 1;
    } else {
      block_size = get_dim(repr[i].second->m_trans.m_transf.f);
    }

    std::cout << "block_size = " << block_size << "\n";
    for (uint64_t j = 0; j < w; j++) {
      res[i][j] = block_size*(((j+1) * (dim/block_size))/w)-
	block_size*((j * (dim/block_size))/w);
      std::cout << "allocation[" << i << "][" << j << "] = "
		<< res[i][j] << "\n";
    }
  }

  return res;
}

std::pair<uint64_t, uint64_t> get_additive
(std::vector<uint64_t> weights,
 uint64_t i)
{
  uint64_t j = 0;

  while (i >= weights[j]) {
    i -= weights[j];
    j++;
  }

  return std::make_pair(j, i);
}

uint64_t from_additive
(std::vector<uint64_t> weights,
 std::pair<uint64_t, uint64_t> ji)
{
  uint64_t j, i;
  std::tie(j, i) = ji;

  while (j != 0) {
    i += weights[j-1];
    j--;
  }

  return i;
}

permutation_t liftPerm
(std::vector<uint64_t> prev_weights,
 permutation_t perm,
 std::vector<uint64_t> weights,
 uint64_t n1,
 uint64_t kmax)
{
  //sanity check
  assert(prev_weights.size() == weights.size());
  
  permutation_t lifted(n1);
  uint64_t n = perm.size();
  std::cout << "n = " << n << "\n";
  for (uint64_t i = 0; i < n1; i++)
    lifted[i] = i;

  for (uint64_t i = 0; i < n; i++) {
    auto i_add = get_additive(prev_weights, i);
    auto j_add = get_additive(weights, perm[i]);

    uint64_t h = 0;
    while (lifted[h] != j_add.first * kmax + j_add.second) h++;
    std::swap(lifted[i_add.first * kmax + i_add.second],
	      lifted[h]);
    std::cout << "p[" << i << "] = " << perm[i] << " -> ";
    std::cout << "lifted-p[" << i_add.first * kmax + i_add.second <<
      "] = " << lifted[i_add.first * kmax + i_add.second] << "\n";
  }

  print_perm(lifted); std::cout << "\n";

  return lifted;
}

Glue::Glue(uint64_t w, Trans *ops, uint64_t m, uint64_t q)
  : m_w(w)
  , m_ops(parallelise(ops))
  , m_kmax(0)
  , m_n(totient(m))
  , m_m(m)
  , m_q(q)
{
  std::cout << *m_ops << "\n";
  auto repr = convert_trans(m_ops);
  auto map  = data_processor_allocation(repr, m_w);
    
  for (auto op_i : map) {
    for (auto map_j : op_i) {
      m_kmax = std::max(m_kmax, map_j);
    }
  }

  m_n1 = m_kmax * m_w;

  std::cout << "n1 = " << m_n1 << "\n";
  std::cout << "kmax = " << m_kmax << "\n";

  std::vector<permutation_t> lifted_perms(repr.size());
  std::vector<uint64_t> def_weights(m_w, m_kmax);

  for (uint64_t i = 0; i < repr.size(); i++) {
    std::cout << "t = " << i << "\n";
    lifted_perms[i] = liftPerm((i == 0 ? def_weights : map[i-1]),
			       repr[i].first,
			       map[i],
			       m_n1,
			       m_kmax);
  }

  std::vector<std::vector<ButterflyOp>> op_scheduling(m_w);
  uint64_t w0 = findElemOfOrder(m_m, m_q, factor(m_m));
  std::cout << "w0 = " << w0 << "\n";
  std::vector<std::vector<uint64_t>> exec_time
    (repr.size(), std::vector<uint64_t>(m_w));

  for (uint64_t i = 0; i < repr.size(); i++) {
    std::cout << "########## cycle " << i << "############\n";
    if (get_type(repr[i].second) == T_ID) {
      for (uint64_t j = 0; j < m_w; j++) {
	std::vector<uint64_t> factors(map[i][j], 1);
	op_scheduling[j].push_back(ButterflyOp(BO_TWIDDLE,
					       map[i][j],
					       1,
					       m_q,
					       0,
					       factors));
	exec_time[i][j] = map[i][j];

	std::cout << "j = " << j << " exec_time = " << exec_time[i][j]
		  << " 1 * identity of size " << map[i][j] << "\n";
      }
    } else {
      switch(get_type(repr[i].second->m_trans.m_transf.f)) {
      case BT_PERM:
	abort();
	  
      case BT_TWIDDLE:
	{
	  uint64_t l = repr[i].second->m_trans.m_transf.l;
	  uint64_t block_size = m_n/l;
	  std::vector<uint64_t> factors(m_n);
	  uint64_t w = pown(w0, m_m/block_size, m_q);
	  auto fs = factor(block_size);
	  assert(fs.size() == 1);
	  uint64_t bs1 = pown(fs[0].first, fs[0].second-1);

	  for (uint64_t k = 0; k < block_size; k++) {
	    uint64_t i0, i1;
	    i1 = k % bs1;
	    i0 = k / bs1;
	    //k = i0 * bs1 + i1
	    factors[k] = pown(w, i1*i0, m_q);
	  }

	  for (uint64_t k = 1; k < l; k++) {
	    std::copy(std::begin(factors),
		      std::begin(factors) + block_size,
		      std::begin(factors) + k*block_size);
	  }

	  uint64_t consumed = 0;
	  for (uint64_t j = 0; j < m_w; j++) {
	    op_scheduling[j].push_back(ButterflyOp(BO_TWIDDLE,
						   map[i][j],
						   1,
						   m_q,
						   0,
						   std::vector<uint64_t>
						   (std::begin(factors) + consumed,
						    std::begin(factors) + consumed + map[i][j])));
	    consumed += map[i][j];
	      
	    exec_time[i][j] = map[i][j];
	    std::cout << "j = " << j << " exec_time = " << exec_time[i][j]
		      << " 1 * twiddle of size " << map[i][j] << "\n";
	  }
	}
	break;
	  
      case BT_TWIDDLE_INV:
	{
	  uint64_t l = repr[i].second->m_trans.m_transf.l;
	  uint64_t block_size = m_n/l;
	  std::vector<uint64_t> factors(m_n);
	  uint64_t w = pown(w0, m_m/block_size, m_q);
	  w = modinv(w, m_q);
	  auto fs = factor(block_size);
	  assert(fs.size() == 1);
	  uint64_t bs1 = pown(fs[0].first, fs[0].second-1);

	  for (uint64_t k = 0; k < block_size; k++) {
	    uint64_t i0, i1;
	    i1 = k % bs1;
	    i0 = k / bs1;
	    //k = i0 * bs1 + i1
	    factors[k] = pown(w, i1*i0, m_q);
	  }

	  for (uint64_t k = 1; k < l; k++) {
	    std::copy(std::begin(factors),
		      std::begin(factors) + block_size,
		      std::begin(factors) + k*block_size);
	  }

	  uint64_t consumed = 0;
	  for (uint64_t j = 0; j < m_w; j++) {
	    op_scheduling[j].push_back(ButterflyOp(BO_TWIDDLE,
						   map[i][j],
						   1,
						   m_q,
						   0,
						   std::vector<uint64_t>
						   (std::begin(factors) + consumed,
						    std::begin(factors) + consumed + map[i][j])));
	    consumed += map[i][j];

	    exec_time[i][j] = map[i][j];
	    std::cout << "j = " << j << " exec_time = " << exec_time[i][j]
		      << " 1 * twiddle_inv of size " << map[i][j] << "\n";
	  }
	}
	break;
	  
      case BT_TWIDDLE_CRT:
	{
	  uint64_t l = repr[i].second->m_trans.m_transf.l;
	  uint64_t block_size = m_n/l;
	  uint64_t crt_index = (repr[i].second->m_trans.m_transf.f)
	    .m_trans.m_twiddle_crt.m;
	  std::vector<uint64_t> factors(m_n);
	  uint64_t w = pown(w0, m_m/crt_index, m_q);
	  auto fs = factor(crt_index);
	  assert(fs.size() == 1);
	  uint64_t bs1 = pown(fs[0].first, fs[0].second-1);

	  for (uint64_t i = 1; i < fs[0].first; i++) {
	    for (uint64_t j = 0; j < bs1; j++) {
	      factors[(i-1)*bs1 + j] = pown(w, i*j, m_q);
	      std::cout << "factors[" << (i-1)*bs1 + j << "] = "
			<< factors[(i-1)*bs1 + j] << "\n";
	    }
	  }
	    
	  for (uint64_t k = 1; k < l; k++) {
	    std::copy(std::begin(factors),
		      std::begin(factors) + block_size,
		      std::begin(factors) + k*block_size);
	  }

	  uint64_t consumed = 0;
	  for (uint64_t j = 0; j < m_w; j++) {
	    op_scheduling[j].push_back(ButterflyOp(BO_TWIDDLE,
						   map[i][j],
						   1,
						   m_q,
						   0,
						   std::vector<uint64_t>
						   (std::begin(factors) + consumed,
						    std::begin(factors) + consumed + map[i][j])));
	    consumed += map[i][j];

	    exec_time[i][j] = map[i][j];
	    std::cout << "j = " << j << " exec_time = " << exec_time[i][j]
		      << " 1 * twiddle_crt of size " << map[i][j] << "\n";
	  }
	}
	break;
	  
      case BT_TWIDDLE_CRT_INV:
	{
	  uint64_t l = repr[i].second->m_trans.m_transf.l;
	  uint64_t block_size = m_n/l;
	  uint64_t crt_index = (repr[i].second->m_trans.m_transf.f)
	    .m_trans.m_twiddle_crt_inv.m;
	  std::vector<uint64_t> factors(m_n);
	  uint64_t w = pown(w0, m_m/crt_index, m_q);
	  w = modinv(w, m_q);
	  auto fs = factor(crt_index);
	  assert(fs.size() == 1);
	  uint64_t bs1 = pown(fs[0].first, fs[0].second-1);

	  for (uint64_t i = 1; i < fs[0].first; i++) {
	    for (uint64_t j = 0; j < bs1; j++) {
	      factors[(i-1)*bs1 + j] = pown(w, i*j, m_q);
	    }
	  }

	  for (uint64_t k = 1; k < l; k++) {
	    std::copy(std::begin(factors),
		      std::begin(factors) + block_size,
		      std::begin(factors) + k*block_size);
	  }

	  uint64_t consumed = 0;
	  for (uint64_t j = 0; j < m_w; j++) {
	    op_scheduling[j].push_back(ButterflyOp(BO_TWIDDLE,
						   map[i][j],
						   1,
						   m_q,
						   0,
						   std::vector<uint64_t>
						   (std::begin(factors) + consumed,
						    std::begin(factors) + consumed + map[i][j])));
	    consumed += map[i][j];

	    exec_time[i][j] = map[i][j];
	    std::cout << "j = " << j << " exec_time = " << exec_time[i][j]
		      << " 1 * twiddle_crt_inv of size " << map[i][j] << "\n";
	  }
	}
	break;
	  
      case BT_DFT:
	{
	  uint64_t l = repr[i].second->m_trans.m_transf.l;
	  uint64_t block_size = m_n/l;
	  for (uint64_t j = 0; j < m_w; j++) {
	    op_scheduling[j].push_back(ButterflyOp(BO_DFT,
						   block_size,
						   map[i][j]/block_size,
						   m_q,
						   pown(w0, m_m/block_size, m_q)));

	    exec_time[i][j] = (map[i][j]/block_size) * block_size * block_size;
	    std::cout << "j = " << j << " exec_time = " << exec_time[i][j]
		      << " " << map[i][j]/block_size << " * dft of size " << block_size << "\n";

	  }
	}
	break;

      case BT_CRT:
	{
	  uint64_t l = repr[i].second->m_trans.m_transf.l;
	  uint64_t crt_index = (repr[i].second->m_trans.m_transf.f)
	    .m_trans.m_crt.m;
	  uint64_t block_size = m_n/l;
	  
	  for (uint64_t j = 0; j < m_w; j++) {
	    op_scheduling[j].push_back(ButterflyOp(BO_CRT,
						   crt_index,
						   map[i][j]/block_size,
						   m_q,
						   pown(w0, m_m/crt_index, m_q)));
	      
	    exec_time[i][j] = (map[i][j]/block_size) * block_size * block_size;
	    std::cout << "j = " << j << " exec_time = " << exec_time[i][j]
		      << " " << map[i][j]/block_size << " * crt of m " << crt_index << "\n";
	  }
	}
	break;
	  
      case BT_DFT_INV:
	{
	  uint64_t l = repr[i].second->m_trans.m_transf.l;
	  uint64_t block_size = m_n/l;
	  for (uint64_t j = 0; j < m_w; j++) {
	    op_scheduling[j].push_back(ButterflyOp(BO_DFT_INV,
						   block_size,
						   map[i][j]/block_size,
						   m_q,
						   pown(w0, m_m/block_size, m_q)));

	    exec_time[i][j] = (map[i][j]/block_size) * block_size * block_size;
	    std::cout << "j = " << j << " exec_time = " << exec_time[i][j]
		      << " " << map[i][j]/block_size << " * dft_inv of size " << block_size << "\n";
	  }
	}
	break;
	
      case BT_CRT_INV:
	{
	  uint64_t l = repr[i].second->m_trans.m_transf.l;
	  uint64_t crt_index = (repr[i].second->m_trans.m_transf.f)
	    .m_trans.m_crt_inv.m;
	  uint64_t block_size = m_n/l;
	  
	  for (uint64_t j = 0; j < m_w; j++) {
	    op_scheduling[j].push_back(ButterflyOp(BO_CRT_INV,
						   crt_index,
						   map[i][j]/block_size,
						   m_q,
						   pown(w0, m_m/crt_index, m_q)));

	    exec_time[i][j] = (map[i][j]/block_size) * block_size * block_size;
	    std::cout << "j = " << j << " exec_time = " << exec_time[i][j]
		      << " " << map[i][j]/block_size << " * crt_inv of m " << crt_index << "\n";
	  }
	}
	break;
      }
    }
  }

  /* The transformed signal is derived from the permutation output */
  permutation_t perm_id(m_n1);
  for (uint64_t i = 0; i < m_n1; i++) perm_id[i] = i;
  lifted_perms.push_back(perm_id);

  m_lifted_perms    = lifted_perms;
  m_op_scheduling   = op_scheduling;
  m_exec_time       = exec_time;
  m_data_allocation = map;
}

Glue::~Glue()
{
  delete m_ops;
}

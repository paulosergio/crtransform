CXX=g++ -std=c++11
HEADERS=Permutations.hpp Glue1.hpp NumTh.hpp System1.hpp DSL2/DFT2.hpp DSL2/CRT2.hpp DSL2/DSL2.hpp DSL2/BasicTrans.hpp HW1/Delayer.hpp HW1/Permuter.hpp HW1/Buffer.hpp
CFLAGS=-g -O0 -Wall -pedantic -Wno-long-long -Wextra
LIBS=-lm -lntl -lgmp -lrt -pthread -L/home/paulo/systemc-2.3.2/lib-cygwin64 -Wl,-rpath=/home/paulo/systemc-2.3.2/lib-cygwin64 -lsystemc
INCLUDE=-I/home/paulo/systemc-2.3.2/include -I./HW1 -I./DSL2 -I.

.PHONY: all
all: Test SystemTestbench1 CreateConfig1 Barrett

Barrett: Barrett.o NumTh.o
	$(CXX) $(CFLAGS) $(INCLUDE) -o $@ $^ $(LIBS)

Test: Test.o Permutations.o
	$(CXX) $(CFLAGS) $(INCLUDE) -o $@ $^ $(LIBS)

SystemTestbench1: HW1/Delayer.o Permutations.o HW1/Permuter.o HW1/Buffer.o HW1/PE.o DSL2/CRT2.o DSL2/DFT2.o DSL2/DSL2.o DSL2/BasicTrans.o NumTh.o Glue1.o System1.o SystemTest1.o
	$(CXX) $(CFLAGS) $(INCLUDE) -o $@ $^ $(LIBS)

CreateConfig1: HW1/Delayer.o Permutations.o HW1/Permuter.o HW1/Buffer.o HW1/PE.o DSL2/CRT2.o DSL2/DFT2.o DSL2/DSL2.o DSL2/BasicTrans.o NumTh.o Glue1.o System1.o CreateConfig1.o
	$(CXX) $(CFLAGS) $(INCLUDE) -o $@ $^ $(LIBS)

%.o: %.cpp $(HEADERS)
	$(CXX) $(CFLAGS) $(INCLUDE) -c $< -o $@ 

